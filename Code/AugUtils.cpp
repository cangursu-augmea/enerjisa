// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 12:08 28.01.2019, Ali Mehmet Altundag
// & Alper Sekerci

#include "StdAfx.h"
#include "AugUtils.h"

void AugUtils::IgnoreCollisionBetween(const char* sEntA, const char* sEntB)
{
	IEntity* pEntA = gEnv->pEntitySystem->FindEntityByName(sEntA);
	if (!pEntA) return;

	IEntity* pEntB = gEnv->pEntitySystem->FindEntityByName(sEntB);
	if (!pEntB) return;

	IPhysicalEntity* pEntPhyA = pEntA->GetPhysicalEntity();
	IPhysicalEntity* pEntPhyB = pEntB->GetPhysicalEntity();

	if (!pEntPhyA || !pEntPhyB) return;

	pe_params_collision_class gccParamsType;
	gccParamsType.collisionClassOR.type = collision_class_game;
	gccParamsType.collisionClassAND.type = collision_class_game;
	pEntPhyA->SetParams(&gccParamsType);

	pe_params_collision_class gccParams;
	gccParams.collisionClassOR.ignore = collision_class_game;
	gccParams.collisionClassAND.ignore = collision_class_game;
	pEntPhyB->SetParams(&gccParams);
}