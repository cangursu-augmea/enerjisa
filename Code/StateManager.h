// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 08:05 16.01.2019, Ali Mehmet Altundag
// & Modified by Alper �ekerci

// An experimental aproach to the state registery
// needs and state query.
// ORIGIN: apollo 11 guidance computer
// STATUS: Experimental!!!
// TODO: better design is needed

#pragma once

#include <string>
#include <vector>
#include <chrono>
#include <ctime>

namespace StateManager
{

enum ECommand : uint32_t
{
	/* returns first matching event result found in the inserted events */
	IS_DONE,

	PLUG_SYSTEM_CHECK,

	CABLE_SYSTEM_CHECK,

	SCENARIO_CHECK
};

typedef uint32_t TAction;
const TAction ALL = -1;
typedef std::string TStateObject;
typedef std::vector<TStateObject> TStateObjects;

union EventResult
{
	EventResult()
	{
		std::memset(this, 0, sizeof(this));
	}

	float		fValue;
	uint32_t	nValue;
	bool		bValue;
	char		sValue[64];
};

typedef std::vector<EventResult> TEventResults;

class CStateManager;
struct Event
{
	// name of the interactgion that causes the event (e.g. "TURN COMPONENT" enum)
	TAction action;

	// vector of affected int
	TStateObjects objects;
	
	// value that we got after event (e.g. 1 after "carry interaction")
	EventResult result;

protected:
	// time point, filled by state manager do not touch!
	std::time_t time;

	inline bool Equals(const TAction _action, TStateObjects& _objects)
	{
		if (action != _action) return false;

		if (objects.size() != _objects.size()) return false;

		// TODO: is really needed? performance?
		std::sort(_objects.begin(), _objects.end());

		// NOTE: objects must be sorted (means not order depended)
		for (int i = 0; i < objects.size(); ++i)
			if (objects[i] != _objects[i]) return false;

		return true;
	}

	inline bool Equals(const TAction _action)
	{
		if (action != _action) return false;

		return true;
	}

	inline TStateObject IsIn(const TAction _action, const TStateObject& _object)
	{
		TStateObject otherObj = "";

		if (_action != action) return otherObj;

		auto it = std::find(objects.begin(), objects.end(), _object);
		bool found = it != objects.end();		

		if (found)
		{
			for (auto name : objects)
			{
				if (name.compare(_object) != 0) // Find the other object that the parameter object is connected to.
				{
					otherObj = name;
					break;
				}
			}
		}

		return otherObj;
	}

	friend class CStateManager;
};

class CStateManager
{
public:

	/* inserts event into the global state manager */
	void Insert(Event& event);

	/* removes all logged events */
	void Reset();

	/*
	statement ex.: COMMAND, ACTION, {OBJECT, ..}
	             : IS_DONE, TURN_COMPONENT, {KURMA_KOLU}
				 : IS_DONE, WEAR_COMPONENT, {YUKSEK_GERILIM_ELDIVENI}
				 : IS_DONE, CONNECT_COMPONENT, {FAZ_KABLOSU_1, FAZ_KLAMENS_1}
				 : IS_DONE_IN_ORDER, 
	NOTE: order of objects IS NOT important
	returns InteractionEventResult(0) if no entry found

	TODO: performance update needed. O^2 in some cases !!!

	*/
	TEventResults Query(ECommand command, TAction action, TStateObjects& objects);

	TEventResults Query(ECommand command, TAction action);

	TStateObject WhichObjImConnected(TStateObject me);

private:
	std::vector<Event> m_events;

	std::map<TAction, std::map<std::string, std::map<std::string, EventResult>>> m_lastStates{};

	TEventResults TO_BE_DELETED_Query(ECommand command, TAction action, TStateObjects& objects);
};

}