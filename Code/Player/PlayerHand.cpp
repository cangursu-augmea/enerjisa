#include "stdafx.h"
#include "PlayerHand.h"
#include <DefaultComponents/Physics/RigidBodyComponent.h>
#include <DefaultComponents/Audio/ListenerComponent.h>
#include <IViewSystem.h>
#include <CryGame/IGameFramework.h>
#include <CrySensorSystem/Interface/ICrySensorSystemPlugin.h>
#include "Interaction/InteractionManagerComponent.h"
#include "StdDefs.h"

CPlayerHand::CPlayerHand(const bool bIsLeft)
	: m_bIsLeft(bIsLeft)
	, m_pMeshComponent(nullptr)
{
	SEntitySpawnParams spawnParams;
	spawnParams.id = 0;
	spawnParams.sName = bIsLeft ? AUG_ENTITY_NAME_LEFT_HAND.c_str() : AUG_ENTITY_NAME_RIGHT_HAND.c_str();
	spawnParams.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();

	m_pHandEntity = gEnv->pEntitySystem->SpawnEntity(spawnParams);
	CRY_ASSERT_MESSAGE(m_pHandEntity != nullptr, "CPlayerHand::CPlayerHand: cannot spawn entity.");
	gEnv->pEntitySystem->AddEntityEventListener(m_pHandEntity->GetId(), ENTITY_EVENT_DONE, this);
}

CPlayerHand::~CPlayerHand()
{
	if (m_pHandEntity != nullptr) {
		gEnv->pEntitySystem->RemoveEntityEventListener(m_pHandEntity->GetId(), ENTITY_EVENT_DONE, this);
	}
}

void CPlayerHand::setModel(const char * sModelFile)
{
	if (m_pHandEntity == nullptr)
		return;
	if (m_pMeshComponent = m_pHandEntity->GetOrCreateComponent<Cry::DefaultComponents::CStaticMeshComponent>()) {
		m_pMeshComponent->GetComponentFlags().Add(EEntityComponentFlags::UserAdded);
		m_pMeshComponent->SetFilePath(sModelFile);
		m_pMeshComponent->LoadFromDisk();
		m_pMeshComponent->ResetObject();
		AABB bounds;
		m_pHandEntity->GetLocalBounds(bounds);
	}

	if (m_pRigidBodyComponent = m_pHandEntity->GetOrCreateComponent<Cry::DefaultComponents::CRigidBodyComponent>()) {
		m_pRigidBodyComponent->m_type = Cry::DefaultComponents::CRigidBodyComponent::EPhysicalType::Rigid;
		m_pRigidBodyComponent->GetComponentFlags().Add(EEntityComponentFlags::UserAdded);
		m_pRigidBodyComponent->Enable(true);
		m_pRigidBodyComponent->SendEvent(SEntityEvent(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED));

		//pe_params_flags pf;
		//pf.flagsOR = pef_ignore_areas;
		//m_pHandEntity->GetPhysicalEntity()->SetParams(&pf);
		//
		//pe_simulation_params pp;
		//m_pHandEntity->GetPhysicalEntity()->GetParams(&pp);
		//pp.gravity = Vec3(ZERO);
		//pp.gravityFreefall = Vec3(ZERO);
		//m_pHandEntity->GetPhysicalEntity()->SetParams(&pp);

		// dont hit eachother...
		//pe_params_collision_class ppcc;
		//ppcc.collisionClassOR.type = 0x01010101;
		//ppcc.collisionClassOR.ignore = 0x01010101;
		//m_pHandEntity->GetPhysicalEntity()->SetParams(&ppcc);
	}
}

void CPlayerHand::updateFromPlayerState(const CTrackerState & state)
{
	if (m_pHandEntity == nullptr)
		return;

	if (m_pRigidBodyComponent == nullptr)
		return;

	pe_params_pos setpos;	setpos.bRecalcBounds = 16 | 32 | 64;
	setpos.pos = state.GetPosition();
	setpos.q = state.GetRotation();
	setpos.iSimClass = SC_ACTIVE_RIGID;
	m_pHandEntity->GetPhysicalEntity()->SetParams(&setpos);

	pe_action_set_velocity setvel;
	setvel.v = state.GetVelocity();
	setvel.w = state.GetAngularVelocity();
	m_pHandEntity->GetPhysicalEntity()->Action(&setvel);

	auto pNode = m_pHandEntity->GetRenderNode(m_pMeshComponent->GetEntitySlotId());
	if (pNode)
	{
		pNode->Hide(IsHidden());
	}


	UpdateTrackingState(state);
}

void CPlayerHand::OnEntityEvent(IEntity * pEntity, const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_DONE:
		m_pHandEntity = nullptr;
		break;
	}
}