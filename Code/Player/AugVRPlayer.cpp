#include "stdafx.h"
#include "AugVRPlayer.h"
#include "Input\HMDIInput.h"

void CAugVRPlayer::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CAugVRPlayer));
		{
		}
	}
}
void CAugVRPlayer::Initialize()
{
	m_playerState.setOrigin(GetEntity()->GetWorldPos());
	processComponentChangeEvent();
	m_head.setPlayerEntity(m_pEntity);

	m_leftHand.SetRadius(m_handRadius);
	m_leftHand.SetRelativePos(Vec3{ -m_handOffset.x, m_handOffset.y, m_handOffset.z });
	m_rightHand.SetRadius(m_handRadius);
	m_rightHand.SetRelativePos(m_handOffset);
}

void CAugVRPlayer::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
		processUpdateEvent();
	break;
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
		processComponentChangeEvent();
	break;
	};
}
void CAugVRPlayer::jumpToPosition(const Vec3 & pos)
{
	m_playerState.jumpToPosition(pos);
}

void CAugVRPlayer::processUpdateEvent()
{
	if (m_playerState.updateFromController()) {
		GetEntity()->SetPos(m_playerState.getPosition());
		GetEntity()->SetRotation(m_playerState.getRotation());
		m_head.updateFromPlayerState(m_playerState);

		m_leftHand.updateFromPlayerState(m_playerState.getLeftHandState());
		m_rightHand.updateFromPlayerState(m_playerState.getRightHandState());
	}
}

void CAugVRPlayer::processComponentChangeEvent()
{
	if (!m_sHeadModelFile.value.IsEmpty()) {
		m_head.setModel(m_sHeadModelFile.value.c_str());
	}
	if (!m_sLeftHandModelFile.value.IsEmpty()) {
		m_leftHand.setModel(m_sLeftHandModelFile.value.c_str());

		m_playerState.getLeftHandState().SetHandId(m_leftHand.getEntity()->GetId());
	}
	if (!m_sRightHandModelFile.value.IsEmpty()) {
		m_rightHand.setModel(m_sRightHandModelFile.value.c_str());
		m_playerState.getRightHandState().SetHandId(m_rightHand.getEntity()->GetId());
	}

}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&CAugVRPlayer::Register)