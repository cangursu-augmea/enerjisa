#pragma once
#include "StdAfx.h"
#include <CryEntitySystem/IEntitySystem.h>
#include <CrySensorSystem/Interface/ISensorMap.h>
#include "PlayerState.h"
#include "Interaction/Interaction.h"
#include "Interaction/TrackerState.h"
#include "Interaction/Interactor.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>

namespace Cry {
	namespace DefaultComponents {
		class CRigidBodyComponent;
	}
}

class CPlayerHand 
	: public IEntityEventListener 
	, public CInteractor
{
public:
	CPlayerHand(const bool bIsLeft);
	~CPlayerHand();
	void setModel(const char *sModelFile);
	void updateFromPlayerState(const CTrackerState &state);
	IEntity *getEntity() const { return m_pHandEntity; };

private:
	virtual void OnEntityEvent(IEntity* pEntity, const SEntityEvent& event) override;

	IEntity * m_pHandEntity{ nullptr };
	const bool m_bIsLeft;
	Cry::DefaultComponents::CRigidBodyComponent* m_pRigidBodyComponent{ nullptr };
	Cry::DefaultComponents::CStaticMeshComponent* m_pMeshComponent;

	IEntity *m_pHeldEntity{ nullptr };
};