#pragma once

#include <CryMath\Cry_Math.h>
#include "Input\HMDIInput.h"
#include "PlayerHand.h"
#include "Interaction/TrackerState.h"
#include <CryEntitySystem/IEntitySystem.h>

class CPlayerState {
public:
	void setOrigin(const Vec3 &pos);
	bool updateFromController();
	void jumpToPosition(const Vec3 &pos);

	Vec3 getPosition() const { return m_playerTransform.GetTranslation(); };
	Quat getRotation() const { return m_playerTransform.GetRotation().ToQuat(); };
	Vec3 getHeadPosition() const { return m_headTransform.GetTranslation(); };
	Quat getHeadRotation() const { return m_headTransform.GetRotation().ToQuat(); };
	CTrackerState &getLeftHandState() { return m_leftHandState; };
	CTrackerState &getRightHandState() { return m_rightHandState; };

private:
	CryTransform::CTransform m_playerTransform;
	CryTransform::CTransform m_headTransform;
	CTrackerState m_leftHandState;
	CTrackerState m_rightHandState;

	Vec3 m_origin{ type_zero::ZERO };
};