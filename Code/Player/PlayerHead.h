#pragma once
#include "StdAfx.h"
#include <CryEntitySystem/IEntitySystem.h>
#include <DefaultComponents/Geometry/StaticMeshComponent.h>
#include <DefaultComponents/Audio/ListenerComponent.h>
#include <DefaultComponents/Cameras/ICameraManager.h>
#include <DefaultComponents/Cameras/VirtualReality/RoomScaleCamera.h>

class CPlayerHead : public IEntityEventListener {
public:
	CPlayerHead(); 
	~CPlayerHead();
	void setModel(const char *sModelFile);
	void updateFromPlayerState(const CPlayerState &state);
	IEntity *getEntity() const { return m_pHeadEntity; };
	void setPlayerEntity(IEntity *pPlayerEntity);
private:
	virtual void OnEntityEvent(IEntity* pEntity, const SEntityEvent& event) override;
	void createView();

	IEntity * m_pHeadEntity{ nullptr };
	IEntity *m_pPlayerEntity{ nullptr };

	CCamera m_camera;

	Cry::DefaultComponents::VirtualReality::CRoomscaleCameraComponent* m_pRoomscaleCameraComponent;
};