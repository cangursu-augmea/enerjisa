#include "stdafx.h"
#include "PlayerState.h"

void CPlayerState::setOrigin(const Vec3 &pos)
{
	m_origin = pos;
}

bool CPlayerState::updateFromController()
{
	auto *pControllerDevice = CHMDIInput::getInstance().getHmdDevice();
	if (pControllerDevice == nullptr)
		return false;

	Vec3 hmdOffset(type_zero::ZERO);
	Quat hmdOrient(type_identity::IDENTITY);

	hmdOffset.x = pControllerDevice->GetLocalTrackingState().pose.position.x;
	hmdOffset.y = pControllerDevice->GetLocalTrackingState().pose.position.y;
	hmdOrient.SetRotationZ(pControllerDevice->GetLocalTrackingState().pose.orientation.GetRotZ());

	m_playerTransform.SetTranslation({ m_origin.x + hmdOffset.x, m_origin.y + hmdOffset.y, m_origin.z });
	m_playerTransform.SetRotation( CryTransform::CRotation(hmdOrient.Normalize()));

	m_headTransform.SetTranslation(m_playerTransform.GetTranslation() + Vec3(0, 0, pControllerDevice->GetLocalTrackingState().pose.position.z));
	m_headTransform.SetRotation(CryTransform::CRotation(pControllerDevice->GetLocalTrackingState().pose.orientation));


//	Ang3 headAng3(pControllerDevice->GetLocalTrackingState().pose.orientation);
//	CryLogAlways("Head Angle: %.1f  %.1f  %.1f", RAD2DEG(headAng3.x), RAD2DEG(headAng3.y), RAD2DEG(headAng3.z));


	m_leftHandState.UpdateFromController(static_cast<EHmdController>(0), m_origin);
	m_rightHandState.UpdateFromController(static_cast<EHmdController>(1), m_origin);
	return true;
}

void CPlayerState::jumpToPosition(const Vec3 & pos)
{
	Vec2 hmdOffset{ type_zero::ZERO };

	if (CHMDIInput::getInstance().getHmdDevice() != nullptr) {
		hmdOffset.x = CHMDIInput::getInstance().getHmdDevice()->GetLocalTrackingState().pose.position.x;
		hmdOffset.y = CHMDIInput::getInstance().getHmdDevice()->GetLocalTrackingState().pose.position.y;
	}

	m_origin = { pos.x - hmdOffset.x, pos.y - hmdOffset.y, pos.z };
}