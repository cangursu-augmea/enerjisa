#pragma once
#include "StdAfx.h"
#include "Player/PlayerState.h"
#include "Player/PlayerHead.h"
#include "Player/PlayerHand.h"
#include <CryEntitySystem/IEntityComponent.h>

class CAugVRPlayer : public IEntityComponent {
private:
	float m_handRadius;
	Vec3 m_handOffset;

public:

	static constexpr uint32 headMemberId = 'hmdl';
	static constexpr uint32 leftHandMemberId = 'lhmd';
	static constexpr uint32 rightHandMemberId = 'rhmd';

	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CAugVRPlayer>& desc)
	{
		desc.SetGUID("{4097A179-0F19-4C58-A24D-0540847B4B79}"_cry_guid);
		desc.SetLabel("AugVRPlayer");
		desc.SetEditorCategory("AugVRPlayer");
		desc.SetDescription("Augmea VR Player component");

		desc.AddMember(&CAugVRPlayer::m_sHeadModelFile, headMemberId, "HeadModelFile", "HeadModelFile", "Determines the model file of the head.", "");
		desc.AddMember(&CAugVRPlayer::m_sLeftHandModelFile, leftHandMemberId, "LeftHandModelFile", "LeftHandModelFile", "Determines the model file of the left hand.", "");
		desc.AddMember(&CAugVRPlayer::m_sRightHandModelFile, rightHandMemberId, "RightHandModelFile", "RightHandModelFile", "Determines the model file of the right hand.", "");

		desc.AddMember(&CAugVRPlayer::m_handRadius, 'rad', "HandRadius", "Hand Radius", 0, 0.05f);
		desc.AddMember(&CAugVRPlayer::m_handOffset, 'off', "HandOffset", "Hand Offset", "Positional offset of the RIGHT hand, left is automatically adjusted.", Vec3{ 0,0.13f,0 });
	}
	// IEntityComponent
	virtual void Initialize() override;
	virtual	void ProcessEvent(const SEntityEvent &event) override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override { return ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE) | ENTITY_EVENT_BIT(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED); };
	// ~IEntityComponent
	
	void jumpToPosition(const Vec3 &pos);
	bool activateOnNetwork();

private:
	void processUpdateEvent();
	void processComponentChangeEvent();

private:
	Schematyc::GeomFileName m_sHeadModelFile{ "" };
	Schematyc::GeomFileName m_sLeftHandModelFile{ "" };
	Schematyc::GeomFileName m_sRightHandModelFile{ "" };

	CPlayerState m_playerState;

	CPlayerHead m_head;
	CPlayerHand m_leftHand{ true }; // bIsLeft = true
	CPlayerHand m_rightHand{ false }; // bIsLeft = false
};