#include "stdafx.h"
#include "PlayerHead.h"
#include <IViewSystem.h>
#include <CryGame/IGameFramework.h>
#include "Input/HMDIInput.h"
#include "StdDefs.h"

using namespace Cry::DefaultComponents::VirtualReality;

CPlayerHead::CPlayerHead()
	: m_pRoomscaleCameraComponent(nullptr)
{
	SEntitySpawnParams spawnParams;
	spawnParams.id = 0; 
	spawnParams.sName = AUG_ENTITY_NAME_PLAYER_HEAD.c_str();
	spawnParams.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();

	m_pHeadEntity = gEnv->pEntitySystem->SpawnEntity(spawnParams);
	CRY_ASSERT_MESSAGE(m_pHeadEntity != nullptr, "CPlayerHead::CPlayerHead: cannot spawn entity.");
	gEnv->pEntitySystem->AddEntityEventListener(m_pHeadEntity->GetId(), ENTITY_EVENT_DONE, this);

	if (Cry::Audio::DefaultComponents::CListenerComponent* pAudioListenerComponent = m_pHeadEntity->GetOrCreateComponent<Cry::Audio::DefaultComponents::CListenerComponent>())
	{
		pAudioListenerComponent->GetComponentFlags().Add(EEntityComponentFlags::UserAdded);
	}

//	createView();

}

CPlayerHead::~CPlayerHead()
{
	if (m_pHeadEntity != nullptr)
		gEnv->pEntitySystem->RemoveEntityEventListener(m_pHeadEntity->GetId(), ENTITY_EVENT_DONE, this);

}

void CPlayerHead::setModel(const char * sModelFile)
{
	if (m_pHeadEntity == nullptr)
		return;
	if (Cry::DefaultComponents::CStaticMeshComponent* pMeshComponent = m_pHeadEntity->GetOrCreateComponent<Cry::DefaultComponents::CStaticMeshComponent>()) {
		pMeshComponent->GetComponentFlags().Add(EEntityComponentFlags::UserAdded);
		pMeshComponent->SetFilePath(sModelFile);
		pMeshComponent->LoadFromDisk();
		pMeshComponent->ResetObject();
	}
}

void CPlayerHead::updateFromPlayerState(const CPlayerState & state)
{
	if (m_pHeadEntity == nullptr)
		return;

	//Matrix34 CamTM;
	//CamTM.SetTranslation(state.getHeadPosition());
	//CamTM.SetRotation33(Matrix33(state.getHeadRotation()));
	//m_pRoomscaleCameraComponent->SetTransformMatrix(CamTM);

//	m_pHeadEntity->SetPos(state.getHeadPosition());
//	m_pHeadEntity->SetRotation(state.getHeadRotation());

/*	auto *pDevice = CHMDIInput::getInstance().getHmdDevice();
	if (pDevice == nullptr)
		return;

	const auto& worldTranform = m_pHeadEntity->GetWorldTM();
	pDevice->EnableLateCameraInjectionForCurrentFrame(std::make_pair(Quat(worldTranform), worldTranform.GetTranslation()));*/

	
}

void CPlayerHead::setPlayerEntity(IEntity * pPlayerEntity)
{
	m_pPlayerEntity = pPlayerEntity;
	m_pHeadEntity->SetPos(pPlayerEntity->GetPos());
	createView();
}

void CPlayerHead::OnEntityEvent(IEntity * pEntity, const SEntityEvent & event)
{
	if (ENTITY_EVENT_DONE == event.event)
		m_pHeadEntity = nullptr;
}

void CPlayerHead::createView()
{
/*	IView *pActiveView = gEnv->pGameFramework->GetIViewSystem()->GetActiveView();
	if (pActiveView == nullptr) {
		pActiveView = gEnv->pGameFramework->GetIViewSystem()->CreateView();
		gEnv->pGameFramework->GetIViewSystem()->SetActiveView(pActiveView);
	}
		pActiveView->LinkTo(m_pHeadEntity);*/


	m_pRoomscaleCameraComponent = m_pHeadEntity->GetOrCreateComponent<CRoomscaleCameraComponent>();
	if (auto pCvar = gEnv->pConsole->GetCVar(AUG_CVAR_NEAR_PLANE.c_str()))
	{
		m_pRoomscaleCameraComponent->SetNearPlane(pCvar->GetFVal());
	}

	m_pRoomscaleCameraComponent->Activate();

	if (IRenderNode* pRenderNode = m_pHeadEntity->GetRenderNode())
		pRenderNode->SetRndFlags(pRenderNode->GetRndFlags() | ERF_HIDDEN);
}
