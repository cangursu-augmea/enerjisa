// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 08:05 16.01.2019, Ali Mehmet Altundag
// & Modified by Alper �ekerci

#include "StdAfx.h"
#include "StateManager.h"
#include "Components\PlugSystem\PlugFemaleTerminal.h"
#include "Components\PlugSystem\PlugMaleTerminal.h"

using namespace StateManager;

void CStateManager::Insert(Event& event)
{
#ifndef _RELEASE
	CryLog("[SIMULATION STATE CHANGED] Action: %.2d, nValue:%d, Num of involved objects: %d", event.action, event.result.nValue, event.objects.size());
#endif

	event.time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());	
	m_events.push_back(event);

	std::vector<std::string> objs{ "", "" };
	for (int i = 0; i < event.objects.size(); ++i)
	{
		objs[i] = event.objects[i];
	}

	m_lastStates[event.action][objs[0]][objs[1]] = event.result;
}

void CStateManager::Reset()
{
	m_events.clear();
	m_lastStates = {};
}

TEventResults CStateManager::TO_BE_DELETED_Query(ECommand command, StateManager::TAction action, TStateObjects& objects)
{
	TEventResults results;
	
	switch (command)
	{
	case ECommand::IS_DONE:
	{		
		// start from back to catch the latest event!
		for (int i = m_events.size() - 1; i >= 0; --i)
		{
			if (m_events[i].Equals(action, objects))
			{
				results.push_back(m_events[i].result);
				return results;
			}
		}		
	}
	break;

	case ECommand::PLUG_SYSTEM_CHECK:
	{
		switch (action)
		{
		case CONNECTION:
		{
			/*if ((WhichObjImConnected(objects[0]).compare(objects[1]) == 0) || 
				(WhichObjImConnected(objects[1]).compare(objects[0]) == 0))
			{
				EventResult res;
				res.nValue = 1;
				results.push_back(res);
				return results;
			}*/

			
			if (2 != objects.size())
			{
				CryWarning(VALIDATOR_MODULE_GAME,
					VALIDATOR_ERROR,
					"[CStateManager]: There must be two objects to cmp. Not less, not more.");
				return results;
			}
			TEventResults res = Query(IS_DONE, PLUG, objects);
			if (res.size() && res[0].bValue == true)
			{
				return res;
			}
			return results;
			
		}
		break;
		case IF_CONNECTED:
		{			
			/*EventResult res;
			TStateObject connectedTo = WhichObjImConnected(objects[0]);
			if (connectedTo.size() == 0) break;
			std::memcpy(res.sValue, connectedTo.c_str(), connectedTo.size() + 1);
			results.push_back(res);
			return results;*/	

			
			if (1 != objects.size())
			{
				CryWarning(VALIDATOR_MODULE_GAME,
					VALIDATOR_ERROR,
					"[CStateManager]: There must be one object to cmp. Not less, not more.");
				return results;
			}

			for (int i = m_events.size() - 1; i >= 0; --i)
			{
				auto pluggedTo = m_events[i].IsIn(PLUG, objects[0]);
				if (pluggedTo.size() > 0)
				{
					if (!m_events[i].result.bValue)
						return results;
					else
					{
						EventResult res;				
						std::memcpy(res.sValue, pluggedTo.c_str(), pluggedTo.size() + 1);
						results.push_back(res);
						return results;
					}
					break;
				}
			}
			
		}
		break;
		}
	}
	break;

	case ECommand::SCENARIO_CHECK:
	{
		return Query(command, action);
	}
	break;
	}
	
	return results;
}

TEventResults CStateManager::Query(ECommand command, StateManager::TAction action, TStateObjects& objects)
{
	TEventResults results;

	std::vector<std::string> objs{ "", "" };
	for (int i = 0; i < objects.size(); ++i)
	{
		objs[i] = objects[i];
	}

	switch (command)
	{
	case ECommand::IS_DONE:
	{	
		results.push_back(m_lastStates[action][objs[0]][objs[1]]);		
	}
	break;

	case ECommand::PLUG_SYSTEM_CHECK:
	{
		switch (action)
		{
		case CONNECTION:
		{
			if (2 != objects.size())
			{
				CryWarning(VALIDATOR_MODULE_GAME,
					VALIDATOR_ERROR,
					"[CStateManager]: There must be two objects to cmp. Not less, not more.");
				return results;
			}

			TEventResults res = Query(IS_DONE, PLUG, objects);
			
			if (res.size() > 0 && res[0].bValue == true)
			{
				return res;
			}
		}
		break;
		case IF_CONNECTED:
		{
			if (1 != objects.size())
			{
				CryWarning(VALIDATOR_MODULE_GAME,
					VALIDATOR_ERROR,
					"[CStateManager]: There must be one object to cmp. Not less, not more.");
				return results;
			}

			if(IEntity* obj = gEnv->pEntitySystem->FindEntityByName(objects[0].c_str()))
			{
				if (auto male = obj->GetComponent<CPlugMaleTerminal>())
				{
					if (!male->IsConnected()) break;
					std::string pluggedTo{ male->GetConnectedFemale()->GetEntity()->GetName() };

					EventResult res;
					std::memcpy(res.sValue, pluggedTo.c_str(), pluggedTo.size() + 1);
					results.push_back(res);
				}
			}	
		}
		break;
		}
	}
	break;

	case ECommand::SCENARIO_CHECK:
	{
		results.push_back(m_lastStates[action][objs[0]][objs[1]]);
	}
	break;
	}

	return results;
}

TEventResults CStateManager::Query(ECommand command, StateManager::TAction action)
{
	return Query(command, action, TStateObjects{});
	/*
	TEventResults results;

	switch (command)
	{
	case ECommand::SCENARIO_CHECK:
	{
		// start from back to catch the latest event!
		for (int i = m_events.size() - 1; i >= 0; --i)
		{
			if (m_events[i].Equals(action))
			{
				results.push_back(m_events[i].result);
				return results;
			}
		}
	}
	break;
	}
	return results;*/
}

TStateObject CStateManager::WhichObjImConnected(TStateObject me)
{
	IEntity* pMe = gEnv->pEntitySystem->FindEntityByName(me.c_str());
	
	CPlugMaleTerminal* pMale = pMe->GetComponent<CPlugMaleTerminal>(false);	
	if (pMale == nullptr)	return "";
	CPlugFemaleTerminal* pFemale = pMale->GetConnectedFemale();
	if (pFemale == nullptr)	return "";

	return pFemale->GetName();
}
