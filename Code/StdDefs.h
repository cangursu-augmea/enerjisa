// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 09:42 28.12.2018, Ali Mehmet Altundag
// & Alper Sekerci

#pragma once

#include <string>
#include "Interaction/Interaction.h"
#include "HighLevelInteraction/HighLevelInteraction.h"

// standard defs will be placed here

const std::string AUG_CVAR_NEAR_PLANE			= "aug_nearPlane";

enum ESecenarioQueryActions : TInteractionComponentId
{
	TRAINING_SET = EHighLevelInteractionIDs::END_OF_DEFAULT_HIGH_LEVEL_INT_COMP + 1,

	// action defs for plug sys
	CONNECTION,
	IF_CONNECTED,

	// result : {result.fVal = current}
	CURRENT,
	// result : {result.fVal = volt}
	VOLT,

	OPERATION_TYPE
};

const std::string AUG_SCENARIO_CVAR_NAME		= "aug_scenario";
const std::string AUG_OPERATION_CVAR_NAME       = "aug_operation";
const std::string AUG_TTRSTATE_CVAR_NAME		= "aug_ttr_value";

const std::string AUG_SET_TRIFAZE = "TRIFAZE";
const std::string AUG_SET_X5 = "X5";
const std::string AUG_SET_HIGHVOL = "HIGHVOL";

const std::string AUG_ENTITY_NAME_LEFT_HAND		= "PlayerHandLeft";
const std::string AUG_ENTITY_NAME_RIGHT_HAND	= "PlayerHandRight";
const std::string AUG_ENTITY_NAME_PLAYER_HEAD	= "PlayerHead";
const std::string AUG_LEVEL_CONF_FILE			= "Assets/LevelConfigs.xml";

// SCENARIO //
const std::string OPERATION_REPLACEMENT			= "DEGISTIRME";
const std::string OPERATION_CUT					= "KESME";
const std::string OPERATION_OPEN				= "ACMA";
const std::string OPERATION_CONTROL             = "KONTROL";
const std::string TTR_CONTROL					= "VALID";
// ~SCENARIO //

// TRIFAZE //
const std::string ENTITY_NAME_HELMET			= "BASLIK";
const std::string ENTITY_NAME_GLOVE_L			= "SOL_ELDIVEN";
const std::string ENTITY_NAME_GLOVE_R			= "SAG_ELDIVEN";
const std::string ENTITY_NAME_HEAD			    = "KAFA";
const std::string ENTITY_NAME_PANEL				= "KAPAK";
const std::string ENTITY_NAME_PANEL_BODY		= "KUTU_GOVDE";
const std::string ENTITY_NAME_PANEL_BODY_STATIC = "KUTU_GOVDE_REAL";
const std::string ENTITY_NAME_CONTROL_PROBE		= "KONTROL_KALEMI";
const std::string ENTITY_NAME_CAMERA			= "KAMERA";
const std::string ENTITY_NAME_FUSE				= "SIGORTA";
const std::string ENTITY_NAME_SEAL				= "MUHUR";
const std::string ENTITY_NAME_MULTIMETER		= "RedProbe";
const std::string ENTITY_NAME_KLEMENS_COVER		= "KLEMENS_KAPAGI";
const std::string ENTITY_NAME_CABLEPREFIX		= "FAZ_KABLO_";
const std::string ENTITY_NAME_KLEMENSPREFIX		= "FAZ_KLEMENS-";
const std::string ENTITY_NAME_SALTERPREFIX		= "SALTER_";
const std::string ENTITY_NAME_TRAFOUSTPREFIX	= "TRAFO_U";
const std::string ENTITY_NAME_TRAFOALTPREFIX	= "TRAFO_D";
const std::string ENTITY_NAME_OPTIC_READER		= "OPTIK_OKUYUCU";
const std::string ENTITY_NAME_DRILL			    = "MATKAP";
const std::string ENTITY_NAME_SCREWDRIVER		= "MATKAP_UCU";
const std::string ENTITY_NAME_INITIAL_SAYAC		= "ILK_SAYAC";
const std::string ENTITY_NAME_NEW_SAYAC			= "YENI_SAYAC";
const std::string ENTITY_NAME_SAYAC_FEMALE		= "SAYAC_FEMALE";
const std::string ENTITY_NAME_PENSAMPERMETRE	= "PENSAMPERMETRE";
const std::string ENTITY_NAME_FASE_CABLE_2		= "FAZ_KABLO_2";
const std::string ENTITY_NAME_FASE_CABLE_4		= "FAZ_KABLO_4";
const std::string ENTITY_NAME_FASE_CABLE_6		= "FAZ_KABLO_6";
// ~TRIFAZE //

// X5 //
const std::string ENTITY_NAME_PANO				= "PANO";
const std::string ENTITY_NAME_PANO_FEMALE		= "PANO_FEMALE";
const std::string ENTITY_NAME_POLE				= "DIREK";
const std::string ENTITY_NAME_POLE_GRIP			= "DIREK_KOL";
const std::string ENTITY_NAME_FUSE_OUT			= "SIGORTA_CIKIS";

const std::string ENTITY_NAME_CROCODILE_RED_1	= "CROCODILE_RED_1";
const std::string ENTITY_NAME_CROCODILE_RED_2	= "CROCODILE_RED_2";
const std::string ENTITY_NAME_CROCODILE_BLACK_1 = "CROCODILE_BLACK_1";
const std::string ENTITY_NAME_CROCODILE_BLACK_2 = "CROCODILE_BLACK_2";

const std::string ENTITY_NAME_CROCODILE_HOLE_UST_1_1 = "UST_1_1";
const std::string ENTITY_NAME_CROCODILE_HOLE_UST_1_2 = "UST_1_2";
const std::string ENTITY_NAME_CROCODILE_HOLE_UST_2_1 = "UST_2_1";
const std::string ENTITY_NAME_CROCODILE_HOLE_UST_2_2 = "UST_2_2";
const std::string ENTITY_NAME_CROCODILE_HOLE_UST_3_1 = "UST_3_1";
const std::string ENTITY_NAME_CROCODILE_HOLE_UST_3_2 = "UST_3_2";

const std::string ENTITY_NAME_CROCODILE_HOLE_ALT_1_1 = "ALT_1_1";
const std::string ENTITY_NAME_CROCODILE_HOLE_ALT_1_2 = "ALT_1_2";
const std::string ENTITY_NAME_CROCODILE_HOLE_ALT_2_1 = "ALT_2_1";
const std::string ENTITY_NAME_CROCODILE_HOLE_ALT_2_2 = "ALT_2_2";
const std::string ENTITY_NAME_CROCODILE_HOLE_ALT_3_1 = "ALT_3_1";
const std::string ENTITY_NAME_CROCODILE_HOLE_ALT_3_2 = "ALT_3_2";
// X5 //

// HIGHVOL//
const std::string ENTITY_NAME_OUT_CELL_KEY = "AYIRICI_OUT_CELL_ANAHTAR_DELIGI";
const std::string ENTITY_NAME_AYIRICI_LOAD_CELL_KRIKO_TOPRAK_DELIGI = "AYIRICI_LOAD_CELL_KRIKO_TOPRAK_DELIGI";
const std::string ENTITY_NAME_AYIRICI_OUT_CELL_KRIKO_TOPRAK_DELIGI = "AYIRICI_OUT_CELL_KRIKO_TOPRAK_DELIGI";
const std::string ENTITY_NAME_AYIRICI_LOAD_CELL_KRIKO_DELIGI = "AYIRICI_LOAD_CELL_KRIKO_DELIGI";
const std::string ENTITY_NAME_AYIRICI_OUT_CELL_KRIKO_DELIGI = "AYIRICI_OUT_CELL_KRIKO_DELIGI";

const std::string ENTITY_NAME_BARA_KAPAK = "BARA_KAPAK";
const std::string ENTITY_NAME_BARA_FIRST = "BARA_FIRST";
const std::string ENTITY_NAME_BARA_SECOND = "BARA_SECOND";
const std::string ENTITY_NAME_BARA_THIRD = "BARA_THIRD";

const std::string ENTITY_NAME_STAKA = "STAKA";
const std::string ENTITY_NAME_KRIKO = "KRIKO";
const std::string ENTITY_NAME_ANAHTAR = "ANAHTAR";

const std::string ENTITY_NAME_CUTTING_KEY = "KESICI_ANAHTAR_DELIGI";
const std::string ENTITY_NAME_CUTTING_BUTTON = "KESICI_BUTTON";

const std::string ENTITY_NAME_CROCODILE_FIRST = "CROCODILE_FIRST";
const std::string ENTITY_NAME_CROCODILE_SECOND = "CROCODILE_SECOND";
const std::string ENTITY_NAME_CROCODILE_THIRD = "CROCODILE_THIRD";
const std::string ENTITY_NAME_CROCODILE_RED = "CROCODILE_RED";

const std::string ENTITY_NAME_CROCODILE_HOLE_FIRST = "CROCODILE_HOLE_FIRST";
const std::string ENTITY_NAME_CROCODILE_HOLE_SECOND = "CROCODILE_HOLE_SECOND";
const std::string ENTITY_NAME_CROCODILE_HOLE_THIRD = "CROCODILE_HOLE_THIRD";
const std::string ENTITY_NAME_CROCODILE_HOLE_RED = "CROCODILE_HOLE_RED";

// ~HIGHVOL //

// CABLE & KLEMENS //
// ~TRIFAZE //
const auto TRIFAZE_FAZ_GIRIS = { 1, 3, 5 };
const auto TRIFAZE_FAZ_CIKIS = { 2, 4, 6 };
const auto TRIFAZE_FAZ = { 1, 2, 3, 4, 5, 6 };
const auto TRIFAZE_NOTR = { 7, 8 };
const auto TRIFAZE_ALL = { 1, 2, 3, 4, 5, 6, 7, 8 };
const auto TRIFAZE_EXPLODE = { 2, 4, 6, 7, 8 };
// ~TRIFAZE //
// ~X5 //
const auto X5TRIFAZE_ALL = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
const auto X5TRIFAZE_FAZ_GIRIS = { 1, 4, 7 };
const auto X5TRIFAZE_GIRIS = { 1, 2, 4, 5, 7, 8 };
const auto X5TRIFAZE_EXPLODE = { 3, 6, 9, 12, 13 };
const auto X5TRIFAZE_LABEL = { 1, 3, 4, 6, 7, 9, 12, 13 };
// ~X5 //
// ~CABLE & KLEMENS //
