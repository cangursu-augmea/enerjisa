﻿// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 26.12.2018, Alper Şekerci

#include "SessionManager.h"
#include "StateManager.h"
#include "Interaction\Interaction.h"
#include "StdDefs.h"
#include "Components/AudioController.h"
#include "Components\Toggle.h"
#include "Components\Cable.h"
#include "HighLevelInteraction/Measurable.h"

auto Log(const char* s)
{
	return [=]()
	{
		CryLog("%s", s);
	};
}

template<int i1, int i2>
bool equal()
{
	return i1 == i2;
}

static bool ReturnTrue()
{
	return true;
}

static bool OK()
{
	if (CGamePlugin::GetSessionManager().IsOK())
	{
		CGamePlugin::GetSessionManager().SetOK(false);
		return true;
	}

	return false;
}

static bool Yes()
{
	if (CGamePlugin::GetSessionManager().IsYes())
	{
		CGamePlugin::GetSessionManager().SetYes(false);
		return true;
	}

	return false;
}

static bool No()
{
	if (CGamePlugin::GetSessionManager().IsNo())
	{
		CGamePlugin::GetSessionManager().SetNo(false);
		return true;
	}

	return false;
}

auto ShowOK(const std::string textName)
{
	return [=]()
	{
		const std::string text = CGamePlugin::GetTextManager().GetText(textName);
		CryLogAlways(text.c_str());
		CGamePlugin::GetBasicUI().ShowOk(text, [](auto ans) { CGamePlugin::GetSessionManager().SetOK(ans == 1); });
	};
}

auto ShowYesNo(const std::string textName)
{
	return [=]()
	{
		const std::string text = CGamePlugin::GetTextManager().GetText(textName);
		CryLogAlways(text.c_str());
		CGamePlugin::GetBasicUI().ShowYesNo(text, [](auto ans)
		{
			CGamePlugin::GetSessionManager().SetYes(ans == 1);
			CGamePlugin::GetSessionManager().SetNo(ans == 0);
		});
	};
}

static bool ApproxEqual(float a, float b)
{
	return a >= (b - 0.0001f) && a <= (b + 0.0001f);
}

template<typename Lambda>
auto Not(Lambda lambda) // Inverts the effect of the function (like IsOpen becomes IsClosed).
{
	return [=]() { return !lambda(); };
}

template<typename Func1, typename Func2>
auto And(Func1 f1, Func2 f2)
{
	return [=]()
	{
		return f1() && f2();
	};
}

template<typename Func1, typename Func2>
auto Or(Func1 f1, Func2 f2)
{
	return [=]()
	{
		return f1() || f2();
	};
}

auto Query(StateManager::ECommand command, StateManager::TAction action, const std::string obj1, const std::string obj2, int val, const std::string str)
{
	return [=]()
	{
		std::vector<std::string> objects;
		if (obj1.size() > 0) objects.push_back(obj1);
		if (obj2.size() > 0) objects.push_back(obj2);

		StateManager::TEventResults res = CGamePlugin::GetStateManager().Query(command, action, objects);

		if (res.size() > 0)
		{
			if (val == -1) return true;

			if (str.size() > 0)
				return std::string(res[0].sValue) == str;

			return res[0].nValue == val;
		}

		return false;
	};
}

auto Query(StateManager::ECommand command, StateManager::TAction action, const std::string obj1, const std::string obj2, int val)
{
	return Query(command, action, obj1, obj2, val, "");
}

auto Query(StateManager::ECommand command, StateManager::TAction action, const std::string obj1, const std::string obj2)
{
	return Query(command, action, obj1, obj2, 1);
}

auto Query(StateManager::ECommand command, StateManager::TAction action, const std::string obj)
{
	return Query(command, action, obj, "");
}

auto Query(StateManager::ECommand command, StateManager::TAction action, const std::string obj, int val)
{
	return Query(command, action, obj, "", val);
}

auto Query(ESecenarioQueryActions action, const std::string str)
{
	return Query(StateManager::ECommand::SCENARIO_CHECK, action, "", "", 0, str);
}

static std::string Cable(int i)
{
	return ENTITY_NAME_CABLEPREFIX + std::to_string(i);
}

static std::string Klemens(int i)
{
	return ENTITY_NAME_KLEMENSPREFIX + std::to_string(i);
}

static std::string Salter(int i)
{
	return ENTITY_NAME_SALTERPREFIX + std::to_string(i);
}

static std::string TrafoUst(int i)
{
	return ENTITY_NAME_TRAFOUSTPREFIX + std::to_string(i);
}

static std::string TrafoAlt(int i)
{
	return ENTITY_NAME_TRAFOALTPREFIX + std::to_string(i);
}

template<typename CableList, typename KlemensList>
inline auto SearchCable(CableList cableList, KlemensList klemensList)
{
	return [=]() // TODO: Unefficient, creates a new lambda at every call. Fold expressions do not work in this version.
	{
		auto cmd = StateManager::ECommand::PLUG_SYSTEM_CHECK;
		auto act = ESecenarioQueryActions::CONNECTION;

		for (auto cable : cableList)
		{
			for (auto klemens : klemensList)
			{
				bool connected = Query(cmd, act, Cable(cable), Klemens(klemens), 1)();
				if (connected) return true;
			}
		}

		return false;
	};
}

auto CheckCables(bool allPlugged, std::initializer_list<int> max)
{
	return [=]() // TODO: Unefficient, creates a new lambda at every call. Fold expressions do not work in this version.
	{
		auto cmd = StateManager::ECommand::PLUG_SYSTEM_CHECK;
		auto act = ESecenarioQueryActions::IF_CONNECTED;

		for (int i : max)
		{
			bool plugged = Query(cmd, act, Cable(i), -1)();

			if ((allPlugged && !plugged) || (!allPlugged && plugged))
				return false;
		}

		return true;
	};
}

auto AreCablesLabeled(std::initializer_list<int> max)
{
	return [=]() // TODO: Unefficient, creates a new lambda at every call. Fold expressions do not work in this version.
	{
		auto cmd = StateManager::ECommand::IS_DONE;
		auto act = TOGGLE;

		for (int i : max)
		{
			bool labeled = Query(cmd, act, Cable(i), 1)();
			if (!labeled) return false;
		}

		return true;
	};
}

auto CheckScrews(bool allOn, std::initializer_list<int> max)
{
	return [=]() // TODO: Unefficient, creates a new lambda at every call. Fold expressions do not work in this version.
	{
		auto cmd = StateManager::ECommand::IS_DONE;
		auto act = TOGGLE;

		for (int i : max)
		{
			bool on = Query(cmd, act, Klemens(i), 1)();

			if ((allOn && !on) || (!allOn && on))
				return false;
		}

		return true;
	};
}

static int  m_failedCount = 0;

auto CheckCableOrder(std::initializer_list<int> max, bool transformerOrder)
{
	return [=]()
	{
		bool result = true;

		using Cmd = StateManager::ECommand;
		auto CONNECTED = ESecenarioQueryActions::CONNECTION;

		const std::vector<int> cable   = {1, 3, 4, 6};
		const std::vector<int> klemens = {4, 6, 1, 3};
		int index = 0;

		for (const auto i : max) {
			if (!transformerOrder && i == cable[index]) // transformerOrder == INCORRECT  
			{
				if (!Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, Cable(i), Klemens(klemens[index]), -1)())
				{
					CryLogAlways("Some cables plugged on wrong klemens");
					result = false;
				}
				++index;
			}
			else {
				if (!Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, Cable(i), Klemens(i), -1)())
				{
					CryLogAlways("Some cables plugged on wrong klemens");
					result = false;
				}
			}
		}
		
		if (!result)
		{
			++m_failedCount;

			/*std::string text = CGamePlugin::GetTextManager().GetText("kablo_sıra_yanlıs");
			CGamePlugin::GetBasicUI().ShowOk(text, [](auto ans) { CGamePlugin::GetSessionManager().SetOK(ans == 1); });
		    text = CGamePlugin::GetTextManager().GetText("kablo_sıra_duzelt");
			CGamePlugin::GetBasicUI().ShowOk(text, [](auto ans) { CGamePlugin::GetSessionManager().SetOK(ans == 1); });*/

			return false;
		}
		else
		{
			CryLogAlways("All cables plugged on right klemens");
			return true;
		}
	};
}

auto SetFuse(bool on)
{
	return [=]()
	{

		IEntity *entityFuse = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_FUSE_OUT.c_str());
		IEntity *entityPole = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_POLE_GRIP.c_str());
		if (entityFuse)
		{
			CToggleComponent* fuse = entityFuse->GetComponent<CToggleComponent>();
			fuse->SetState(on);
		}


		if (entityPole)
		{
			CToggleComponent* pole = entityPole->GetComponent<CToggleComponent>();
			pole->SetState(on);
		}

		/*
		if(auto fuse = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_FUSE.c_str())->GetComponent<CToggleComponent>())
			fuse->SetState(on);
		auto pole = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_POLE_GRIP.c_str())->GetComponent<CToggleComponent>();

		pole->SetState(on);
		*/
	};
}

static void ClearUI()
{
	CGamePlugin::GetBasicUI().Clear();
}

static void ExitToIOS()
{
	// TODO: Quits game opens IOS.
	CryLogAlways("Program kapatiliyor ve IOS aciliyor.");
}

static void Explode(const char* path, const ParticleLoc& loc)
{
	IParticleEffect*  m_pEffect = gEnv->pParticleManager->FindEffect(path);

	if (m_pEffect) {
		m_pEffect->Spawn(true, loc);
	}
	else {
		CryLog("Could not find explosion particle.");
	}

	// TODO: Kacaktan carpilir.
	CGamePlugin::GetBasicUI().Clear();
	CryLogAlways("<<<EXPLODED>>>");
	CGamePlugin::GetBasicUI().ShowOk(CGamePlugin::GetTextManager().GetText("exploded"), [](auto _) {ExitToIOS(); });
}

CSessionManager::CSessionManager()
{
	
}

bool CSessionManager::Init()
{
	if (gEnv->pEntitySystem->FindEntityByName("KLEMENS_KAPAGI"))
		m_explodePos = gEnv->pEntitySystem->FindEntityByName("KLEMENS_KAPAGI")->GetWorldPos() + Vec3(0, -0.0778, 0);
	gEnv->pGameFramework->RegisterListener(this, "SessionManager", FRAMEWORKLISTENERPRIORITY_HUD);

	auto Pole = gEnv->pConsole->GetCVar("aug_electricity_pole");
	auto transformer = gEnv->pConsole->GetCVar("aug_transformer_state");
	auto monitor = gEnv->pConsole->GetCVar("aug_monitor_state");

	m_IsTherePole = !strcmp(Pole->GetString(), "USED");
	m_transformerState = !strcmp(transformer->GetString(), "CORRECT");
	m_monitorState = !strcmp(monitor->GetString(), "CORRECT");
	m_transformerOrder = !strcmp(gEnv->pConsole->GetCVar("aug_transformer_order")->GetString(), "CORRECT");


	using State = FlowState & ;
	State start = FlowState::create();

	FlowStartEnd tutorial = CreateTutorial();

	State training_decision = FlowState::create();
	training_decision.next(Query(ESecenarioQueryActions::TRAINING_SET, "TRIFAZE")).next(CreateTrifazeUntilOpDecision().start);
	training_decision.next(Query(ESecenarioQueryActions::TRAINING_SET, "X5")).next(CreateX5UntilOpDecision().start);
	training_decision.next(Query(ESecenarioQueryActions::TRAINING_SET, "HIGHVOL")).next(CreateHighVolUntilOpDecision().start);

	start.next(tutorial.start);
	tutorial.end->next(&training_decision);

	m_pState = &start;
	m_pStart = m_pState;

	auto set = gEnv->pConsole->GetCVar("aug_training_set");

	if (!strcmp(set->GetString(), "X5"))
	{
		if (!m_IsTherePole)
		{
			gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_POLE.c_str())->Invisible(true);
			gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_POLE_GRIP.c_str())->Invisible(true);
		}

		const std::vector<int> default = { 1, 4, 8 };
		const std::vector<int> target = { 4, 8, 1 };
		if (!strcmp(gEnv->pConsole->GetCVar("aug_cable_order")->GetString(), "MIXED"))
		{
			int index = 0;
			for (const auto i : default) {
				if (auto pPhaseCable = gEnv->pEntitySystem->FindEntityByName(Cable(i).c_str()))
					pPhaseCable->GetComponent<CCable>()->SetKlemensId(target[index]);
				++index;
			}
		}
		else {
			for (const auto i : default) {
				if (auto pPhaseCable = gEnv->pEntitySystem->FindEntityByName(Cable(i).c_str()))
					pPhaseCable->GetComponent<CCable>()->SetKlemensId(i);
			}

		}
	}

	if (!m_monitorState)
	{
		int i = 1;
		int v1 = rand() % 100;

		if (v1 < 34)
			i = 1;
		else if (v1 < 67 && v1 > 33)
			i = 4;
		else if (v1 > 66)
			i = 7;

		IEntity* cable = gEnv->pEntitySystem->FindEntityByName(Cable(i).c_str());
		CMeasurableComponent* measurable = cable->GetComponent<CMeasurableComponent>();
		measurable->SetType(4); // hatalı kablo
	}
	if (!strcmp(set->GetString(), "X5"))
	{
		if (!m_transformerState)
		{
			int i = 1;
			int v1 = rand() % 100;

			if (v1 < 34)
				i = 1;
			else if (v1 < 67 && v1 > 33)
				i = 2;
			else if (v1 > 66)
				i = 3;

			for (int k = 1; k <= 3; ++k) {
				if (IEntity* cable = gEnv->pEntitySystem->FindEntityByName(TrafoUst(k).c_str()))
				{
					CMeasurableComponent* measurable = cable->GetComponent<CMeasurableComponent>();
					if(k == i)
						measurable->SetType(5); // hatalı trafo
					else 
						measurable->SetType(1);
				}
			}
			
		}
	}
	

	return true;
}

void CSessionManager::Reset()
{
	m_pState = m_pStart;

	ok = false;
	yes = false;
	no = false;
	failed = false;
	m_explodeFlag = false;
	m_failedCount = 0;
}

bool CSessionManager::CheckFailure()
{
	if (failed)
		return true;

	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;

	if (Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0)())
		return failed;
	

	bool failedPanel = And(Not(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_CONTROL_PROBE, ENTITY_NAME_PANEL)), Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_PANEL))();

	string operationType = gEnv->pConsole->GetCVar("aug_training_set")->GetString();
	if (operationType == "X5")
		m_fazOnNotr = SearchCable(X5TRIFAZE_GIRIS, X5TRIFAZE_EXPLODE)();
	else
		m_fazOnNotr = SearchCable(TRIFAZE_FAZ_GIRIS, TRIFAZE_EXPLODE)();

	failed = m_explodeFlag || failedPanel || m_fazOnNotr ;

	if (failed)
	{
		SEntitySpawnParams spawnParams;
		spawnParams.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();
		spawnParams.vPosition = m_explodePos;

		// Spawn the entity
		IEntity* pEntity;
		if (pEntity = gEnv->pEntitySystem->SpawnEntity(spawnParams))
		{
			auto compAudio = pEntity->CreateComponentClass<CAudioController>();
			compAudio->PlayByName("pfxMonitorLingeringElec");
		}

		Explode("particles/electric_shock.pfx", ParticleLoc(pEntity->GetWorldTM()));
	}

	return failed;
}

FlowStartEnd CSessionManager::CreateTutorial()
{
	using State = FlowState & ;
	State start = FlowState::create();

	State yesno_check = start.act(ShowOK("tutorial_ok"))
		.next(OK).act(ShowYesNo("tutorial_yesno"));

	yesno_check.next(No, &start);

	State end = yesno_check.next(Yes).act(ShowOK("tutorial_headset"))
		.next(OK).act(ShowOK("tutorial_room"))
		.next(OK).act(ShowOK("tutorial_finish"))
		.next(OK).act(ClearUI);

	return FlowStartEnd{ &start, &end };
}

FlowStartEnd CSessionManager::CreateTrifazeUntilOpDecision()
{

	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	using Scenario = ESecenarioQueryActions;
	using State = FlowState & ;
	State start = FlowState::create();

	State OpDecisionFirstPart = start.act(ShowOK("dogru_adres"))
		.next(OK).act(ShowOK("kiyafet"))
		.next(OK)
		.next(Query(Cmd::IS_DONE, Act::WEAR, AUG_ENTITY_NAME_LEFT_HAND))
		.next(Query(Cmd::IS_DONE, Act::WEAR, AUG_ENTITY_NAME_RIGHT_HAND))
		.next(Query(Cmd::IS_DONE, Act::WEAR, ENTITY_NAME_HEAD))
		.act(ShowOK("panel_enerji_kontrol")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_CONTROL_PROBE, ENTITY_NAME_PANEL))
		.act(ShowOK("panel_ac")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_PANEL))
		.act(ShowYesNo("kopuk_mu"));
	//Kablo Durum Kontrol
	//if(Yes)
	OpDecisionFirstPart.next(Yes)
		.act(ShowOK("kopuk_evet")).next(OK)
		.act(ExitToIOS);
	//if(Yes)
	//if(No)
	std::string cableCondition = gEnv->pConsole->GetCVar("aug_cable_condition")->GetString();
	State whichOp = OpDecisionFirstPart.next(No)
		.act(ShowOK("foto"))
		.next(OK)
		.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY));
	//if(No)
	//Kablo Durum Kontrol
	const auto &opPrm = gEnv->pSystem->GetIConsole()->GetCVar(AUG_OPERATION_CVAR_NAME.c_str());
	if (cableCondition == "NORMAL")
	{
		if (0 == strcmp(opPrm->GetString(), OPERATION_REPLACEMENT.c_str()))
		{
			whichOp.next().next(CreateSayacDegisim().start);
		}
		else if (0 == strcmp(opPrm->GetString(), OPERATION_OPEN.c_str()))
		{
			whichOp.next().next(CreateSayacAcma().start);
		}
		else if (0 == strcmp(opPrm->GetString(), OPERATION_CUT.c_str()))
		{
			whichOp.next().next(CreateSayacKesme().start);
		}
	}
	else
	{
		whichOp.act(SetFailed(true));
	}


	return FlowStartEnd{ &start, &whichOp };
}

FlowStartEnd CSessionManager::CreateSayacDegisim()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using State = FlowState & ;
	State start = FlowState::create();

	start
		.act(SetFuse(true)).next()
		.act(ShowOK("sayac_enerji_kes")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
		.act(ShowOK("sayac_muhur_sok")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_KLEMENS_COVER, 0)) // Muhur klemens kapaginin ustunde, toggle ile sokuluyor.
		.act(ShowOK("klemens_cikar")).next(OK)
		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1))) // -1 means return true if found any result																													   
		.act(ShowOK("sayac_baglanti_enerjitest_tekrar")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, ENTITY_NAME_FASE_CABLE_2))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, ENTITY_NAME_FASE_CABLE_4))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, ENTITY_NAME_FASE_CABLE_6))
		.act(ShowOK("faz_sirasi_isaretle")).next(OK)
		.next(AreCablesLabeled(TRIFAZE_FAZ_GIRIS))
		.act(ShowOK("vida_sok")).next(OK)
		.next(CheckScrews(false, TRIFAZE_ALL))
		.act(ShowOK("faz_sok_birlesik")).next(OK)
		.next(CheckCables(false, TRIFAZE_ALL))
		.act(ShowOK("sayac_endeks_al")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_OPTIC_READER, ENTITY_NAME_INITIAL_SAYAC))
		.act(ShowOK("sayac_sok")).next(OK)
		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_INITIAL_SAYAC, ENTITY_NAME_SAYAC_FEMALE, -1)))
		.act(ShowOK("yeni_sayac_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_NEW_SAYAC, ENTITY_NAME_SAYAC_FEMALE, -1))
		.act(ShowOK("kablo_hepsi_tak")).next(OK)
		.next(CheckCables(true, TRIFAZE_ALL))
		.act(ShowOK("vida_tak")).next(OK)
		.next(CheckScrews(true, TRIFAZE_ALL))
		.act(ShowOK("klemens_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_NEW_SAYAC, -1))
		.act(ShowOK("sayac_muhurle")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_KLEMENS_COVER))
		.act(ShowOK("degisim_ciktisi")).next(OK).act(ExitToIOS);

	return FlowStartEnd{ &start, &start };
}

FlowStartEnd CSessionManager::CreateSayacKesme()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using State = FlowState & ;
	State start = FlowState::create();

	start
		.act(SetFuse(true)).next()
		.act(ShowOK("kesme_endeks_al")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_OPTIC_READER, ENTITY_NAME_INITIAL_SAYAC))
		.act(ShowOK("sayac_enerji_kes")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
		.act(ShowOK("sayac_muhur_sok")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_KLEMENS_COVER, 0)) // Muhur klemens kapaginin ustunde, toggle ile sokuluyor.
		.act(ShowOK("klemens_cikar")).next(OK)
		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1))) // -1 means return true if found any result
		.act(ShowOK("sayac_baglanti_enerjitest")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, ENTITY_NAME_FASE_CABLE_2))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, ENTITY_NAME_FASE_CABLE_4))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, ENTITY_NAME_FASE_CABLE_6))
		.act(ShowOK("vida_sok")).next(OK)
		.next(CheckScrews(false, TRIFAZE_ALL))
		.act(ShowOK("faz_sok_birlesik")).next(OK)
		.next(CheckCables(false, TRIFAZE_ALL))
		.act(ShowOK("vida_tak")).next(OK)
		.next(CheckScrews(true, TRIFAZE_ALL))
		.act(ShowOK("klemens_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1))
		.act(ShowOK("enerji_ver")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
		.act(ShowOK("sayac_baglanti_enerjitest_tekrar")).next(OK)
		.next((Query(Cmd::IS_DONE, Act::MULTIMETER, ENTITY_NAME_FASE_CABLE_2)))
		.next((Query(Cmd::IS_DONE, Act::MULTIMETER, ENTITY_NAME_FASE_CABLE_4)))
		.next((Query(Cmd::IS_DONE, Act::MULTIMETER, ENTITY_NAME_FASE_CABLE_6)))
		.act(ShowOK("sayac_muhurle")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_KLEMENS_COVER))
		.act(ShowOK("foto")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY))
		.act(ShowOK("kesme_ciktisi")).next(OK).act(ExitToIOS);

	return FlowStartEnd{ &start, &start };
}

FlowStartEnd CSessionManager::CreateSayacAcma()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using State = FlowState & ;
	State start = FlowState::create();

	State sealCheck = start
		.act(SetFuse(false)).next()
		.act(ShowYesNo("kesik_muhurlu"));
	State sealYesState = sealCheck.next(Yes);

	State orderCancelCheck = sealCheck.next(No)
		.act(ShowOK("sayac_endeks_al")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_OPTIC_READER, ENTITY_NAME_INITIAL_SAYAC))
		.act(ShowYesNo("siparis"));

	orderCancelCheck.next(Yes)
		.act(ShowOK("kacak")).next(OK)
		.act(ExitToIOS);

	orderCancelCheck.next(No, &sealYesState);

	sealYesState
		.act(ShowOK("sayac_muhur_sok")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_KLEMENS_COVER, 0)) // Muhur klemens kapaginin ustunde, toggle ile sokuluyor.
		// .act(ShowOK("durum_kontrol_et")).next(OK)
		// TODO: Kablolar duzgun degilse bitir.
		.act(ShowOK("klemens_cikar")).next(OK)
		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1))) // -1 means return true if found any result		
		.act(ShowOK("sayac_baglanti_enerjitest")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, ENTITY_NAME_FASE_CABLE_2))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, ENTITY_NAME_FASE_CABLE_4))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, ENTITY_NAME_FASE_CABLE_6))
		.act(ShowOK("vida_sok")).next(OK)
		.next(CheckScrews(false, TRIFAZE_ALL))
		.act(ShowOK("dogru_sirayla_tak")).next(OK)
		.next(CheckCables(true, TRIFAZE_ALL))
		.act(ShowOK("vida_tak")).next(OK)
		.next(CheckScrews(true, TRIFAZE_ALL))
		.act(ShowOK("klemens_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1))
		.act(ShowOK("sayac_muhurle")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_KLEMENS_COVER))
		.act(ShowOK("sayac_enerji_ac")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
		.act(ShowOK("foto")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY))
		.act(ShowOK("acma_ciktisi")).next(OK).act(ExitToIOS);

	return FlowStartEnd{ &start, &start };
}

FlowStartEnd CSessionManager::CreateCableTest()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	using State = FlowState & ;
	State start = FlowState::create();

	start.next(CheckCables(false, TRIFAZE_ALL))
		.act(ShowOK("kablo_hepsi_tak")).next(OK)
		.next(CheckCables(true, TRIFAZE_ALL))
		.act(ShowOK("degisim_ciktisi")).next(OK);

	/*start.act(ShowOK("faz_sok_birlesik")).next(OK)
	.next(Query(Cmd::PLUG_SYSTEM_CHECK, ESecenarioQueryActions::IF_CONNECTED, "FAZ_KABLO_1"))
	.act(ShowOK("degisim_ciktisi")).next(OK);*/

	return FlowStartEnd{ &start, &start };
}

FlowStartEnd CSessionManager::CreateX5UntilOpDecision()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	using Scenario = ESecenarioQueryActions;
	using State = FlowState & ;
	State start = FlowState::create();

	CryLogAlways("X5 GETTTT");//DEBUG
	State OpDecisionFirstPart = start.act(ShowOK("dogru_adres"))
		.next(OK).act(ShowOK("kiyafet"))
		.next(OK)
		.next(Query(Cmd::IS_DONE, Act::WEAR, AUG_ENTITY_NAME_LEFT_HAND))
		.next(Query(Cmd::IS_DONE, Act::WEAR, AUG_ENTITY_NAME_RIGHT_HAND))
		.next(Query(Cmd::IS_DONE, Act::WEAR, ENTITY_NAME_HEAD))
		.act(ShowOK("panel_enerji_kontrol")).next(OK)//panoya göre değiştirilmesi gerekebilir
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_CONTROL_PROBE, ENTITY_NAME_PANEL))
		.act(ShowOK("panel_ac")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_PANEL))
		.act(ShowYesNo("kopuk_mu"));
	//Kablo Durum Kontrol
	//if(Yes)
	OpDecisionFirstPart.next(Yes)
		.act(ShowOK("kopuk_evet")).next(OK)
		.act(ExitToIOS);
	//if(Yes)
	//if(No)
	std::string cableCondition = gEnv->pConsole->GetCVar("aug_cable_condition")->GetString();
	State whichOp = OpDecisionFirstPart.next(No)
		.act(ShowOK("foto"))
		.next(OK)
		.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY));
	//if(No)
	//Kablo Durum Kontrol
	const auto &opPrm = gEnv->pSystem->GetIConsole()->GetCVar(AUG_OPERATION_CVAR_NAME.c_str());
	if (cableCondition == "NORMAL")
	{
		if (0 == strcmp(opPrm->GetString(), OPERATION_REPLACEMENT.c_str()))
		{
			whichOp.next().next(CreateX5SayacDegisim().start);
		}
		else if (0 == strcmp(opPrm->GetString(), OPERATION_OPEN.c_str()))
		{
			whichOp.next().next(CreateX5SayacAcma().start);
		}
		else if (0 == strcmp(opPrm->GetString(), OPERATION_CUT.c_str()))
		{
			whichOp.next().next(CreateX5SayacKesme().start);
		}
		else if (0 == strcmp(opPrm->GetString(), OPERATION_CONTROL.c_str()))
		{
			whichOp.next().next(CreateX5Kontrol(false).start);
		}
	}
	else
	{
		whichOp.act(SetFailed(true));
	}
	/*
	whichOp.next(Query(Scenario::OPERATION_TYPE, OPERATION_REPLACEMENT)).next(CreateSayacDegisim().start);
	whichOp.next(Query(Scenario::OPERATION_TYPE, OPERATION_OPEN)).next(CreateSayacAcma().start);
	whichOp.next(Query(Scenario::OPERATION_TYPE, OPERATION_CUT)).next(CreateSayacKesme().start);
	*/
	return FlowStartEnd{ &start, &whichOp };
}

FlowStartEnd CSessionManager::CreateX5SayacDegisim()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using State = FlowState & ;
	State start = FlowState::create();



	State FirstPart = start
		.act(SetFuse(true)).next();

	if (m_IsTherePole)
	{
		FirstPart
			.act(ShowOK("sayac_enerji_kes")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
			.act(ShowOK("direk_enerji_kes")).next(OK) ////////////////////////////////TEXT direk indir
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_POLE_GRIP, 0))
			//Mühür klemens üstünde değil pano üstünde
			.act(ShowOK("pano_muhur_sok")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 0))
			.act(ShowOK("pano_cikar")).next(OK)
			.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1)))
			//Mühür klemens üstünde değil pano üstünde
			.act(ShowOK("klemens_cikar")).next(OK)
			.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1)))
			.act(ShowOK("faz_sirasi_isaretle")).next(OK)
			.next(AreCablesLabeled(X5TRIFAZE_LABEL))
			.act(ShowOK("vida_sok")).next(OK)
			.next(CheckScrews(false, X5TRIFAZE_ALL))
			.act(ShowOK("faz_sok_birlesik")).next(OK)
			.next(CheckCables(false, X5TRIFAZE_ALL))
			.act(ShowOK("sayac_endeks_al")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_OPTIC_READER, ENTITY_NAME_INITIAL_SAYAC))
			.act(ShowOK("sayac_sok")).next(OK)
			.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_INITIAL_SAYAC, ENTITY_NAME_SAYAC_FEMALE, -1)))
			.act(ShowOK("yeni_sayac_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_NEW_SAYAC, ENTITY_NAME_SAYAC_FEMALE, -1))
			.act(ShowOK("kablo_hepsi_tak")).next(OK)
			.next(CheckCables(true, X5TRIFAZE_ALL))
			.act(ShowOK("vida_tak")).next(OK)
			.next(CheckScrews(true, X5TRIFAZE_ALL))
			.act(ShowOK("direk_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_POLE_GRIP, 1))
			.act(ShowOK("sayac_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
			
			//KONTROL ET ???

			.act(ShowOK("sayac_baglanti_enerjitest")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(3)))
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(6)))
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(9)))

			//KONTROL ET ???
			.act(ShowOK("sayac_enerji_kes")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
			.act(ShowOK("direk_enerji_kes")).next(OK) ////////////////////////////////TEXT direk indir
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_POLE_GRIP, 0))
			.act(ShowOK("klemens_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_NEW_SAYAC, -1))
			.act(ShowOK("pano_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1))
			.act(ShowOK("pano_muhurle")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 1))
			.act(ShowOK("direk_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_POLE_GRIP, 1))
			.act(ShowOK("sayac_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
			.act(ShowOK("foto")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY))
			.act(ShowOK("degisim_ciktisi")).next(OK).act(ExitToIOS);

	}
	else
	{
		FirstPart
			.act(ShowOK("sayac_enerji_kes")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
			//Mühür klemens üstünde değil pano üstünde
			.act(ShowOK("pano_muhur_sok")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 0))
			.act(ShowOK("pano_cikar")).next(OK)
			.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1)))
			//Mühür klemens üstünde değil pano üstünde
			.act(ShowOK("klemens_cikar")).next(OK)
			.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1)))
			.act(ShowOK("faz_sirasi_isaretle")).next(OK)
			.next(AreCablesLabeled(X5TRIFAZE_LABEL))
			.act(ShowOK("vida_sok")).next(OK)
			.next(CheckScrews(false, X5TRIFAZE_ALL))
			.act(ShowOK("faz_sok_birlesik")).next(OK)
			.next(CheckCables(false, X5TRIFAZE_ALL))
			.act(ShowOK("sayac_endeks_al")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_OPTIC_READER, ENTITY_NAME_INITIAL_SAYAC))
			.act(ShowOK("sayac_sok")).next(OK)
			.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_INITIAL_SAYAC, ENTITY_NAME_SAYAC_FEMALE, -1)))
			.act(ShowOK("yeni_sayac_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_NEW_SAYAC, ENTITY_NAME_SAYAC_FEMALE, -1))
			.act(ShowOK("kablo_hepsi_tak")).next(OK)
			.next(CheckCables(true, X5TRIFAZE_ALL))
			.act(ShowOK("vida_tak")).next(OK)
			.next(CheckScrews(true, X5TRIFAZE_ALL))
			.act(ShowOK("sayac_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
			//KONTROL ET ???

			.act(ShowOK("sayac_baglanti_enerjitest")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(3)))
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(6)))
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(9)))

			//KONTROL ET ???
			.act(ShowOK("sayac_enerji_kes")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
			.act(ShowOK("klemens_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_NEW_SAYAC, -1))
			.act(ShowOK("pano_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1))
			.act(ShowOK("pano_muhurle")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 1))
			.act(ShowOK("sayac_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
			.act(ShowOK("foto")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY))
			.act(ShowOK("degisim_ciktisi")).next(OK).act(ExitToIOS);

	}

	return FlowStartEnd{ &start, &start };
}

FlowStartEnd CSessionManager::CreateX5SayacKesme()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using State = FlowState & ;
	State start = FlowState::create();

	State FirstPart = start
		.act(SetFuse(true)).next()
		.act(ShowOK("pano_muhur_sok")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 0)) // Klemens kapağı mührü değik pano kapağının mührü
		.act(ShowOK("pano_cikar")).next(OK)
		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1))) //TODO: klemens i pano kapağı ile değiştir
		.act(ShowOK("kesme_endeks_al")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_OPTIC_READER, ENTITY_NAME_INITIAL_SAYAC));


	FirstPart
		.act(ShowOK("cıkıs_sayac_enerji_kes")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE_OUT, 0))
		.act(ShowOK("pano_tak")).next(OK)//pano geri tak
		.next((Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1)))
		.act(ShowOK("pano_muhurle")).next(OK)// pano mühürle
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 1))
		.act(ShowOK("foto")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY))
		.act(ShowOK("kesme_ciktisi")).next(OK).act(ExitToIOS);

	return FlowStartEnd{ &start, &start };
}

//FlowStartEnd CSessionManager::CreateX5SayacKesme()
//{
//	using Cmd = StateManager::ECommand;
//	using Act = EInteractionComponenetIDs;
//	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
//	using State = FlowState & ;
//	State start = FlowState::create();
//
//	State firstPart = FlowState::create();
//	firstPart.act(SetFuse(true)).next()
//		.act(ShowOK("pano_muhur_sok")).next(OK)
//		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 0)) // Klemens kapağı mührü değik pano kapağının mührü
//		.act(ShowOK("pano_cikar")).next(OK)
//		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1))) //TODO: klemens i pano kapağı ile değiştir
//		.act(ShowOK("kesme_endeks_al")).next(OK)
//		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_OPTIC_READER, ENTITY_NAME_INITIAL_SAYAC));
//
//	
//	State lastPart = FlowState::create();
//	lastPart.act(ShowOK("sayac_enerji_kes")).next(OK)
//		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
//		.act(ShowOK("klemens_cikar")).next(OK)
//		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1)))//klemens plug
//
//		.act(ShowOK("kablo_çıkış_çıkar")).next(OK)
//		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, Cable(3), Klemens(3), -1)))//klemens plug
//		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, Cable(6), Klemens(6), -1)))//klemens plug
//		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, Cable(9), Klemens(9), -1)))//klemens plug
//
//		.act(ShowOK("pano_tak")).next(OK)//pano geri tak
//		.next((Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1)))
//		.act(ShowOK("pano_muhurle")).next(OK)// pano mühürle
//		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 1))
//		.act(ShowOK("foto")).next(OK)
//		.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY))
//		.act(ShowOK("kesme_ciktisi")).next(OK).act(ExitToIOS);
//
//
//
//	if (m_IsTherePole)
//	{
//
//		firstPart
//			.act(ShowOK("direk_enerji_kes")).next(OK)
//			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_POLE_GRIP, 0));
//	}
//
//	firstPart.next(&lastPart);
//	
//
//
//	return FlowStartEnd{ &start, &start };
//}

FlowStartEnd CSessionManager::CreateX5SayacAcma()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using State = FlowState & ;
	State start = FlowState::create();

	CryLogAlways("CreateX5SayacAcma scenario is running");
	State check = start
		.act(ShowOK("acma_endeks_al")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_OPTIC_READER, ENTITY_NAME_INITIAL_SAYAC))
		.act(SetFuse(false)).next()
		.act(ShowYesNo("kesik_muhurlu"))  //Sayaç ve Akım Trafoları Sistemle Örtüşüyor mu?
		.next(Yes) // What if No?
		.act(ShowOK("pano_muhur_sok")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 0))
		.act(ShowOK("kontrol_et_isle")).next(OK)
		.act(ShowOK("pano_cikar")).next(OK)
		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1)))
		.act(ShowYesNo("ortusuyor_mu"));
	
	FlowStartEnd kontrol = CreateX5Kontrol(true);

	check.next(No)
		.next(kontrol.start);

	State SecondPart = FlowState::create();
	check.next(Yes).next(&SecondPart);

	SecondPart
		.act(ShowOK("klemens_cikar")).next(OK)
		.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1)))
		.act(ShowOK("sayac_baglanti_enerjitest")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, Cable(3)))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, Cable(6)))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_MULTIMETER, Cable(9)));
		
	State thirdPart = FlowState::create();
	kontrol.end->next(&thirdPart);

	if (m_IsTherePole)
	{
		///*thirdPart= */SecondPart.next()
		CryLogAlways("WITH POLE");
		thirdPart.next()
			.act(ShowOK("direk_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_POLE_GRIP, 1))
			.act(ShowOK("sayac_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
			.act(ShowOK("sayac_baglanti_enerjitest_tekrar")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(3)))
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(6)))
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(9)))
			.act(ShowOK("sayac_enerji_kes")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
			.act(ShowOK("direk_enerji_kes")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_POLE_GRIP, 0))
			.act(ShowOK("klemens_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1))
			.act(ShowOK("pano_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1))
			.act(ShowOK("pano_muhurle")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 1))
			.act(ShowOK("direk_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_POLE_GRIP, 1))
			.act(ShowOK("sayac_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
			.act(ShowOK("foto")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY))
			.act(ShowOK("acma_ciktisi")).next(OK).act(ExitToIOS);
	}
	else
	{
		SecondPart
			.act(ShowOK("sayac_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
			.act(ShowOK("sayac_baglanti_enerjitest_tekrar")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(3)))
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(6)))
			.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Cable(9)))
			.act(ShowOK("sayac_enerji_kes")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 0))
			.act(ShowOK("klemens_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1))
			.act(ShowOK("pano_tak")).next(OK)
			.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1))
			.act(ShowOK("pano_muhurle")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 1))
			.act(ShowOK("sayac_enerji_ac")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_FUSE, 1))
			.act(ShowOK("foto")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::CAMERA, ENTITY_NAME_CAMERA, ENTITY_NAME_PANEL_BODY))
			.act(ShowOK("acma_ciktisi")).next(OK).act(ExitToIOS);
	}

	SecondPart.next(&thirdPart);

	return FlowStartEnd{ &start, &start };
}

FlowStartEnd CSessionManager::CreateX5Kontrol(bool forOpenOperation) //TODO: write senario for this operation
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using State = FlowState & ;
	State start = FlowState::create();
	State end = FlowState::create();

	State monitorCheck = FlowState::create();
	monitorCheck.act(ShowYesNo("monitor_kontrol"));

	if (!forOpenOperation)
	{
		monitorCheck = start
			.act(SetFuse(true)).next()
			.act(ShowOK("pano_muhur_sok")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 0))
			.act(ShowOK("pano_cikar")).next(OK)
			.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1)))
			.act(ShowOK("klemens_cikar")).next(OK)
			.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1)))
			.act(ShowOK("vida_sok")).next(OK)
			.next(CheckScrews(false, X5TRIFAZE_ALL))
			//SAYAÇ MONITOR
			.act(ShowOK("sayac_monitor")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::MONITOR_LAST, ENTITY_NAME_INITIAL_SAYAC, 1))
			//SAYAÇ MONITOR
			.next(&monitorCheck);
	}
	else
	{
		monitorCheck = start			
			.act(ShowOK("klemens_cikar")).next(OK)
			.next(Not(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1)))
			.act(ShowOK("vida_sok")).next(OK)
			.next(CheckScrews(false, X5TRIFAZE_ALL))
			//SAYAÇ MONITOR
			.act(ShowOK("sayac_monitor")).next(OK)
			.next(Query(Cmd::IS_DONE, Act::MONITOR_LAST, ENTITY_NAME_INITIAL_SAYAC , 1))
			//SAYAÇ MONITOR
			.next(&monitorCheck);
	}

	State oranTesti = FlowState::create();

	State monitorYes = monitorCheck.next(Yes);
	if (m_monitorState)
	{
		monitorYes.next(&oranTesti);
	}
	else
	{
		monitorYes
			.act(ShowOK("monitor_hatalı")).next(OK)
			.act(ExitToIOS);
	}

	State monitorNo = monitorCheck.next(No)
		.act(ShowOK("sayac_baglanti_enerjitest")).next(OK)

		.act(ShowOK("ilk_faz_enerjitest")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::MONITOR, ENTITY_NAME_INITIAL_SAYAC, 1))
		.next(Query(Cmd::IS_DONE, Act::AMPERMETRE, Cable(2)))
		
		.act(ShowOK("ikinci_faz_enerjitest")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::MONITOR, ENTITY_NAME_INITIAL_SAYAC, 1))
		.next(Query(Cmd::IS_DONE, Act::AMPERMETRE, Cable(5)))

		.act(ShowOK("ucuncu_faz_enerjitest")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::MONITOR, ENTITY_NAME_INITIAL_SAYAC, 1))
		.next(Query(Cmd::IS_DONE, Act::AMPERMETRE, Cable(8)))

		.act(ShowOK("salter_baglanti_enerjitest")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Salter(1)))
		.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Salter(2)))
		.next(Query(Cmd::IS_DONE, Act::MULTIMETER, Salter(3)))
		;

	
	State question = monitorNo
		.act(ShowYesNo("kablo_sıra_kontrol"));

	question.next(No).act(ShowOK("kablo_sıra_duzelt")).next(OK).next(&question);
	
	State result = question.next(Yes).next(CheckCableOrder(X5TRIFAZE_ALL, m_transformerOrder)); //CheckCables


	if (m_monitorState)
	{
		result
			.act(ShowOK("hata_tespit_edilmedi")).next(OK)
			.next(&oranTesti);

	}
	else
	{
		result
			.act(ShowOK("hata_tespit_edilip_düzeltildi")).next(OK)
			.next(&oranTesti);
	}



	//Oran testi
	State oranCheck = oranTesti
		.act(ShowOK("trafo_oran_testi_1")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::PENSAMPERMETRE, TrafoUst(1)))
		.next(Query(Cmd::IS_DONE, Act::PENSAMPERMETRE, TrafoAlt(1)))
		.act(ShowOK("trafo_oran_testi_2")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::PENSAMPERMETRE, TrafoUst(2)))
		.next(Query(Cmd::IS_DONE, Act::PENSAMPERMETRE, TrafoAlt(2)))
		.act(ShowOK("trafo_oran_testi_3")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::PENSAMPERMETRE, TrafoUst(3)))
		.next(Query(Cmd::IS_DONE, Act::PENSAMPERMETRE, TrafoAlt(3)))
		.act(ShowOK("trafo_ttr_testi_1")).next(OK)
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_1, ENTITY_NAME_CROCODILE_HOLE_UST_1_1, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_2, ENTITY_NAME_CROCODILE_HOLE_UST_1_1, -1)))
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_1, ENTITY_NAME_CROCODILE_HOLE_UST_1_2, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_2, ENTITY_NAME_CROCODILE_HOLE_UST_1_2, -1)))
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_1, ENTITY_NAME_CROCODILE_HOLE_ALT_1_1, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_2, ENTITY_NAME_CROCODILE_HOLE_ALT_1_1, -1)))
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_1, ENTITY_NAME_CROCODILE_HOLE_ALT_1_2, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_2, ENTITY_NAME_CROCODILE_HOLE_ALT_1_2, -1)))
		.act(ShowOK("trafo_ttr_sonuc_1")).next(OK)
		.act(ShowOK("trafo_ttr_testi_2")).next(OK)
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_1, ENTITY_NAME_CROCODILE_HOLE_UST_2_1, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_2, ENTITY_NAME_CROCODILE_HOLE_UST_2_1, -1)))
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_1, ENTITY_NAME_CROCODILE_HOLE_UST_2_2, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_2, ENTITY_NAME_CROCODILE_HOLE_UST_2_2, -1)))
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_1, ENTITY_NAME_CROCODILE_HOLE_ALT_2_1, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_2, ENTITY_NAME_CROCODILE_HOLE_ALT_2_1, -1)))
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_1, ENTITY_NAME_CROCODILE_HOLE_ALT_2_2, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_2, ENTITY_NAME_CROCODILE_HOLE_ALT_2_2, -1)))
		.act(ShowOK("trafo_ttr_sonuc_1")).next(OK)
		.act(ShowOK("trafo_ttr_testi_3")).next(OK)
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_1, ENTITY_NAME_CROCODILE_HOLE_UST_3_1, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_2, ENTITY_NAME_CROCODILE_HOLE_UST_3_1, -1)))
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_1, ENTITY_NAME_CROCODILE_HOLE_UST_3_2, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED_2, ENTITY_NAME_CROCODILE_HOLE_UST_3_2, -1)))
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_1, ENTITY_NAME_CROCODILE_HOLE_ALT_3_1, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_2, ENTITY_NAME_CROCODILE_HOLE_ALT_3_1, -1)))
		.next(Or(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_1, ENTITY_NAME_CROCODILE_HOLE_ALT_3_2, -1), Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_BLACK_2, ENTITY_NAME_CROCODILE_HOLE_ALT_3_2, -1)))
		.act(ShowOK("trafo_ttr_sonuc_1")).next(OK)
		.act(ShowYesNo("trafo_testi"));

	State trafoYes = oranCheck.next(Yes);
	
	if (m_transformerState)
	{
		if (!forOpenOperation)
		{
			end = trafoYes
				.act(ShowOK("vida_tak")).next(OK)
				.next(CheckScrews(true, X5TRIFAZE_ALL))
				.act(ShowOK("klemens_tak")).next(OK)
				.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KLEMENS_COVER, ENTITY_NAME_INITIAL_SAYAC, -1))
				.act(ShowOK("pano_tak")).next(OK)
				.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_PANO, ENTITY_NAME_PANO_FEMALE, -1))
				.act(ShowOK("pano_muhurle")).next(OK)
				.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_PANO, 1))
				.act(ShowOK("kontrol_son")).next(OK)
				.next(); //Mühürle kapat
		}
		else
		{
			end = trafoYes;
		}
	}
	else
	{
		end = trafoYes
			.act(ShowOK("trafo_hata")).next(OK)
			.act(ExitToIOS);
	}

	State trafoNO = oranCheck.next(No)
		.act(ShowOK("kopuk_evet")).next(OK)
		.act(ExitToIOS);

	return FlowStartEnd{ &start, &end };
}

FlowStartEnd CSessionManager::CreateHighVolUntilOpDecision()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	using Scenario = ESecenarioQueryActions;
	using State = FlowState & ;
	State start = FlowState::create();

	State whichOp = start.act(ShowOK("dogru_adres"))
		.next(OK).act(ShowOK("kiyafet"))
		.next(OK)
		.next(Query(Cmd::IS_DONE, Act::WEAR, AUG_ENTITY_NAME_LEFT_HAND))
		.next(Query(Cmd::IS_DONE, Act::WEAR, AUG_ENTITY_NAME_RIGHT_HAND))
		.next(Query(Cmd::IS_DONE, Act::WEAR, ENTITY_NAME_HEAD));

	const auto &opPrm = gEnv->pSystem->GetIConsole()->GetCVar(AUG_OPERATION_CVAR_NAME.c_str());

	if (0 == strcmp(opPrm->GetString(), OPERATION_REPLACEMENT.c_str()))
	{
		CryLog("OPERATION_REPLACEMENT");
		whichOp.next().next(HighVolDegistirme().start);
	}
	else if (0 == strcmp(opPrm->GetString(), OPERATION_OPEN.c_str()))
	{
		CryLog("OPERATION_OPEN");
		whichOp.next().next(HighVolAcma().start);
	}
	else if (0 == strcmp(opPrm->GetString(), OPERATION_CUT.c_str()))
	{
		CryLog("OPERATION_CUT");
		whichOp.next().next(HighVolKesme().start);
	}

	return FlowStartEnd{ &start, &whichOp };
}

FlowStartEnd CSessionManager::HighVolKesme()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using Scenario = ESecenarioQueryActions;
	using State = FlowState & ;
	State start = FlowState::create();

	CryLog("degistime-> kesme");
	State OpDecisionFirstPart = start.next()
		.act(ShowOK("anahtari_kesiciye_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_ANAHTAR, ENTITY_NAME_CUTTING_KEY, -1))
		.act(ShowOK("anahtari_cevir")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_ANAHTAR))
		.act(ShowOK("kesici_dugmesine_bas")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_CUTTING_BUTTON))
		.act(ShowOK("anahari_cikis_hücresine_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_ANAHTAR, ENTITY_NAME_OUT_CELL_KEY, -1))
		.act(ShowOK("anahari_cevir")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_ANAHTAR))
		.act(ShowOK("manevra_cubugu_cikis_hücresi_ayiriciya_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KRIKO, ENTITY_NAME_AYIRICI_OUT_CELL_KRIKO_DELIGI, -1))
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_KRIKO))
		.act(ShowOK("manevra_cubugu_cikis_hücresi_topraklama_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KRIKO, ENTITY_NAME_AYIRICI_OUT_CELL_KRIKO_TOPRAK_DELIGI, -1))
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_KRIKO))
		.act(ShowOK("manevra_cubugu_yuk_hücresi_ayiriciya_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KRIKO, ENTITY_NAME_AYIRICI_LOAD_CELL_KRIKO_DELIGI, -1))
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_KRIKO))
		.act(ShowOK("manevra_cubugu_yuk_hücresi_topraklama_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_KRIKO, ENTITY_NAME_AYIRICI_LOAD_CELL_KRIKO_DELIGI, -1))
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_KRIKO))
		.act(ShowOK("stanka_kontrol")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_STAKA, ENTITY_NAME_BARA_FIRST))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_STAKA, ENTITY_NAME_BARA_SECOND))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_STAKA, ENTITY_NAME_BARA_THIRD))
		.act(ShowOK("fazlarda_enerji_yok")).next(OK);

	return FlowStartEnd{ &start, &start };
}

FlowStartEnd CSessionManager::HighVolAcma()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using Scenario = ESecenarioQueryActions;
	using State = FlowState & ;
	State start = FlowState::create();

	State OpDecisionFirstPart = start
		.act(ShowOK("anahari_kesiciye_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CUTTING_KEY, ENTITY_NAME_ANAHTAR, -1))
		.act(ShowOK("anahari_cevir")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_ANAHTAR))
		.act(ShowOK("kesici_dugmesine_bas")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOGGLE, ENTITY_NAME_CUTTING_BUTTON))
		.act(ShowOK("anahari_cikis_hücresine_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_OUT_CELL_KEY, ENTITY_NAME_ANAHTAR, -1))
		.act(ShowOK("anahari_cevir")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_ANAHTAR))
		.act(ShowOK("manevra_cubugu_cikis_hücresi_ayiriciya_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_AYIRICI_OUT_CELL_KRIKO_DELIGI, ENTITY_NAME_KRIKO, -1))
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_KRIKO))
		.act(ShowOK("manevra_cubugu_cikis_hücresi_topraklama_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_AYIRICI_OUT_CELL_KRIKO_TOPRAK_DELIGI, ENTITY_NAME_KRIKO, -1))
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_KRIKO))
		.act(ShowOK("manevra_cubugu_yuk_hücresi_ayiriciya_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_AYIRICI_LOAD_CELL_KRIKO_DELIGI, ENTITY_NAME_KRIKO, -1))
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_KRIKO))
		.act(ShowOK("manevra_cubugu_yuk_hücresi_topraklama_tak")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_AYIRICI_LOAD_CELL_KRIKO_DELIGI, ENTITY_NAME_KRIKO, -1))
		.next(Query(Cmd::IS_DONE, Act::TURN, ENTITY_NAME_KRIKO))
		.act(ShowOK("stanka_kontrol")).next(OK)
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_STAKA, ENTITY_NAME_BARA_FIRST))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_STAKA, ENTITY_NAME_BARA_SECOND))
		.next(Query(Cmd::IS_DONE, Act::TOUCH, ENTITY_NAME_STAKA, ENTITY_NAME_BARA_THIRD))
		.act(ShowOK("fazlarda_enerji_yok")).next(OK);

	return FlowStartEnd{ &start, &start };
}

FlowStartEnd CSessionManager::HighVolDegistirme()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using Scenario = ESecenarioQueryActions;
	using State = FlowState & ;
	State start = FlowState::create();

	State whichOp = start.next().next(HighVolKesme().start);
	State afterTTROp = FlowState::create();

	const auto &opPrm = gEnv->pSystem->GetIConsole()->GetCVar(AUG_TTRSTATE_CVAR_NAME.c_str());

	if (0 == strcmp(opPrm->GetString(), TTR_CONTROL.c_str()))
	{
		afterTTROp = whichOp.next().next(HighVolTTRKontrol().start);
	}
	else
	{
		afterTTROp = whichOp.next();
	}

	afterTTROp.next(CreateX5SayacDegisim().start).next(HighVolAcma().start);

	return FlowStartEnd{ &start, &afterTTROp };
}

FlowStartEnd CSessionManager::HighVolTTRKontrol()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	using Scenario = ESecenarioQueryActions;
	using State = FlowState & ;
	State start = FlowState::create();

	start.act(ShowOK("ttr_kontrol")).next(OK)
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_FIRST, ENTITY_NAME_CROCODILE_HOLE_FIRST, -1))
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_SECOND, ENTITY_NAME_CROCODILE_HOLE_SECOND, -1))
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_THIRD, ENTITY_NAME_CROCODILE_HOLE_THIRD, -1))
		.next(Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, ENTITY_NAME_CROCODILE_RED, ENTITY_NAME_CROCODILE_HOLE_RED, -1));

	return FlowStartEnd{ &start, &start };
}