// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 21.02.2018, Ali Mehmet Altundag

#include "StdAfx.h"
#include <DefaultComponents/Physics/RigidBodyComponent.h>
#include "StaticCarry.h"
#include "Interaction/InteractionManagerComponent.h"


void CStaticCarryComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CStaticCarryComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CStaticCarryComponent::Register)

CStaticCarryComponent::CStaticCarryComponent()
	: m_owenerHandId(INVALID_ENTITYID)
{
	Reset();
}

void CStaticCarryComponent::Initialize()
{
	CInteractionManagerComponent* pInteraction = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	CRY_ASSERT_MESSAGE(pInteraction != nullptr, "StaticCarry component needs to have interaction component attached to the entity.");

	Reset();
}

void CStaticCarryComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_RESET:
	{
		Reset();
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (m_owenerHandId == INVALID_ENTITYID) return;

		auto tracker = GetTrackerState();
		Matrix34 trackerWorldTM = Matrix34(Matrix33(tracker.GetRotation()), tracker.GetPosition());
		m_pEntity->SetWorldTM(trackerWorldTM * m_offsetTM);
	}
	break;
	}
}

void CStaticCarryComponent::Reset()
{
	m_owenerHandId = INVALID_ENTITYID;
}

void CStaticCarryComponent::Pick(const CTrackerState& hand)
{
	m_owenerHandId = hand.GetHandId();
	auto tracker = GetTrackerState();
	Matrix34 trackerWorldTM = Matrix34(Matrix33(tracker.GetRotation()), tracker.GetPosition());
	m_offsetTM = trackerWorldTM.GetInverted() * m_pEntity->GetWorldTM();
}

void CStaticCarryComponent::Drop(const CTrackerState& hand)
{
	Reset();
}

SInteractionFeedback CStaticCarryComponent::InteractionTick()
{
	const CTrackerState &state = GetTrackerState();

	switch (m_feedback.phase)
	{
		// check if we can carry and try to carry...
	case EInteractionPhase::FIRST:
	{
		if (state.GetButtonState(eKI_Motion_OpenVR_Grip) == 1.f && m_owenerHandId == INVALID_ENTITYID)
		{
			Pick(state);
			m_feedback.feedback = EInteractionFeedback::SUCCESS;
		}
	}
	break;
	// ongoing process: 1. nothing to do 2. we might be asked to drop
	case EInteractionPhase::ONGOING:
	{
		if (state.GetButtonState(eKI_Motion_OpenVR_Grip) == 0.f)
		{
			Drop(state);
			m_feedback.feedback = EInteractionFeedback::NONE;
		}
		else
		{
			m_feedback.feedback = EInteractionFeedback::INTERACTION_LOCK;
		}
	}
	break;
	// somehow we got the signal...
	case EInteractionPhase::LAST:
	{
		m_feedback.feedback = EInteractionFeedback::NONE;
	}
	break;
	}

	// calc next state
	StateMachineTick(m_feedback);

	// return current state
	return m_feedback;
}

void CStaticCarryComponent::StateMachineTick(const SInteractionFeedback & feedback)
{
	switch (m_feedback.phase)
	{
	case EInteractionPhase::FIRST:
	{
		if (EInteractionFeedback::SUCCESS == feedback.feedback)
			m_feedback.phase = EInteractionPhase::ONGOING;
		else
			m_feedback.phase = EInteractionPhase::LAST;
	}
	break;
	case EInteractionPhase::ONGOING:
	{
		if (EInteractionFeedback::NONE == feedback.feedback)
			m_feedback.phase = EInteractionPhase::LAST;
	}
	break;
	case EInteractionPhase::LAST:
	{
		m_feedback.phase = EInteractionPhase::FIRST;
		break;
	}
	}
}