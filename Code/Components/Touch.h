// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 4:08 PM 01/16/2019, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"

struct Detectable
{
	std::string Object;
	float Timer;
};

class CTouchComponent  
	: public CInteractionBaseComponent

{
public:
	CTouchComponent() ;
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CTouchComponent>& desc)
	{
		desc.SetGUID("{7E9E2415-E8EA-4D0B-A8C1-F2E1BD126499}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Touch");
		desc.SetDescription("To Touch an object, you use this component.");
		//EDITOR MEMBERS
		desc.AddMember(&CTouchComponent::m_OffSet, 'mdir', "RaycastVec", "Way to Scan(-1 ... 1)", "Way to Scan", (0.f, 1.f, 0.f));
		desc.AddMember(&CTouchComponent::m_PosOffSet, 'mpof', "OffSetVec", "OffSet", "Way to Move", (0.f, 0.f, 0.f));
		desc.AddMember(&CTouchComponent::m_ScanEntityName, 'scan', "ScanName", "Entity Name to Scan:", "Get name of Entity thats gonna be scaned", "");
		desc.AddMember(&CTouchComponent::m_Lengt, 'mlen', "LengtofRay", "Lengt of Ray", "LengtofRay", 1.f);
		desc.AddMember(&CTouchComponent::m_CoolDown, 'cool', "DetectCoolDown", "CoolDown time beetween detects", "DetectCoolDown", 2.f);
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::TOUCH; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

private:
	Vec3 m_OffSet;
	Vec3 m_PosOffSet;
	Schematyc::CSharedString m_ScanEntityName;
	float m_Lengt;
	bool m_operate;
	Quat offsetRotator;
	std::vector<Detectable> m_Detactable;
	float m_CoolDown;
};