#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryMath/ISplines.h>

class TubeSurface : public _i_reference_target_t
{

public:
	Matrix34 m_worldTM;

	// If rkUpVector is not the zero vector, it will be used as 'up' in the frame calculations.  If it is the zero
	// vector, the Frenet frame will be used.  If bWantColors is 'true',
	// the vertex colors are allocated and set to black.  The application
	// needs to assign colors as needed.  If either of pkTextureMin or
	// pkTextureMax is not null, both must be not null.  In this case,
	// texture coordinates are generated for the surface.
	void GenerateSurface(spline::CatmullRomSpline<Vec3>* pSpline, float fRadius, bool bClosed,
		const Vec3& rkUpVector, int iMedialSamples, int iSliceSamples,
		bool bWantNormals, bool bWantColors, bool bSampleByArcLength,
		bool bInsideView, const Vec2* pkTextureMin, const Vec2* pkTextureMax);

	TubeSurface();
	~TubeSurface();

	// member access
	Vec3& UpVector();
	const Vec3& GetUpVector() const;
	int GetSliceSamples() const;

	// Generate vertices for the end slices.  These are useful when you build
	// an open tube and want to attach meshes at the ends to close the tube.
	// The input array must have size (at least) S+1 where S is the number
	// returned by GetSliceSamples.  Function GetTMinSlice is used to access
	// the slice corresponding to the medial curve evaluated at its domain
	// minimum, tmin.  Function GetTMaxSlice accesses the slice for the
	// domain maximum, tmax.  If the curve is closed, the slices are the same.
	void GetTMinSlice(Vec3* akSlice);
	void GetTMaxSlice(Vec3* akSlice);

	// If the medial curve is modified, for example if it is control point
	// based and the control points are modified, then you should call this
	// update function to recompute the tube surface geometry.
	void UpdateSurface();

	int  Index(int iS, int iM) { return iS + (m_iSliceSamples + 1) * iM; };
	void ComputeSinCos();
	void ComputeVertices(Vec3* akVertex);
	void ComputeNormals();
	void ComputeTextures(const Vec2& rkTextureMin, const Vec2& rkTextureMax, Vec2* akTexture);
	void ComputeConnectivity(int& riTQuantity, vtx_idx*& raiConnect, bool bInsideView);

	float m_fRadius;
	int m_iMedialSamples;
	int m_iSliceSamples;
	Vec3 m_kUpVector;
	float* m_afSin;
	float* m_afCos;
	bool m_bClosed;
	bool m_bSampleByArcLength;
	bool m_bCap;

	int nAllocVerts;
	int nAllocIndices;
	int nAllocSinCos;

	int iVQuantity;
	int iNumIndices;

	int iTQuantity; // Num triangles (use iNumIndices instead).

	Vec3* m_akTangents;
	Vec3* m_akBitangents;

	Vec3* m_akVertex;
	Vec3* m_akNormals;
	Vec2* m_akTexture;
	vtx_idx* m_pIndices;
	spline::CatmullRomSpline<Vec3>* m_pSpline;
};
