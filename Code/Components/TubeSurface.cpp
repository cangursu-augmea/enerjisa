#include "TubeSurface.h"

TubeSurface::TubeSurface()
{
	m_afSin = NULL;
	m_afCos = NULL;

	m_bCap = false;
	m_bClosed = false;
	m_bSampleByArcLength = false;
	m_fRadius = 0;
	m_iMedialSamples = 0;
	m_iSliceSamples = 0;

	nAllocVerts = 0;
	nAllocIndices = 0;
	nAllocSinCos = 0;

	iVQuantity = 0;
	iTQuantity = 0;
	iNumIndices = 0;

	m_pSpline = NULL;
	m_akTangents = NULL;
	m_akBitangents = NULL;

	m_akVertex = NULL;
	m_akNormals = NULL;
	m_akTexture = NULL;
	m_pIndices = NULL;
}

TubeSurface::~TubeSurface()
{
	delete[] m_pIndices;
	delete[] m_akTexture;
	delete[] m_akBitangents;
	delete[] m_akTangents;
	delete[] m_akNormals;
	delete[] m_akVertex;
	delete[] m_afSin;
	delete[] m_afCos;
}

void TubeSurface::GenerateSurface(spline::CatmullRomSpline<Vec3>* pSpline, float fRadius,
	bool bClosed, const Vec3& rkUpVector, int iMedialSamples,
	int iSliceSamples, bool bWantNormals, bool bWantColors,
	bool bSampleByArcLength, bool bInsideView, const Vec2* pkTextureMin, const Vec2* pkTextureMax)
{
	assert((pkTextureMin && pkTextureMax) || (!pkTextureMin && !pkTextureMax));

	m_pSpline = pSpline;
	m_fRadius = fRadius;
	m_kUpVector = rkUpVector;
	m_iMedialSamples = iMedialSamples;
	m_iSliceSamples = iSliceSamples;
	m_bClosed = bClosed;
	m_bSampleByArcLength = bSampleByArcLength;

	m_bCap = !m_bClosed;
	// compute the surface vertices
	if (m_bClosed)
		iVQuantity = (m_iSliceSamples + 1) * (m_iMedialSamples + 1);
	else
		iVQuantity = (m_iSliceSamples + 1) * m_iMedialSamples;

	bool bAllocVerts = false;
	if (iVQuantity > nAllocVerts || !m_akVertex)
	{
		bAllocVerts = true;
		nAllocVerts = iVQuantity;

		delete[] m_akVertex;
		m_akVertex = new Vec3[nAllocVerts];
	}

	ComputeSinCos();
	ComputeVertices(m_akVertex);

	// compute the surface normals
	if (bWantNormals)
	{
		if (bAllocVerts)
		{
			delete[] m_akNormals;
			delete[] m_akTangents;
			delete[] m_akBitangents;

			m_akNormals = new Vec3[iVQuantity];
			m_akTangents = new Vec3[iVQuantity];
			m_akBitangents = new Vec3[iVQuantity];
		}
		ComputeNormals();		
	}

	// compute the surface textures coordinates
	if (pkTextureMin && pkTextureMax)
	{
		if (bAllocVerts)
		{
			delete[] m_akTexture;
			m_akTexture = new Vec2[iVQuantity];
		}

		ComputeTextures(*pkTextureMin, *pkTextureMax, m_akTexture);
	}

	// compute the surface triangle connectivity
	ComputeConnectivity(iTQuantity, m_pIndices, bInsideView);

	// create the triangle mesh for the tube surface
	//Reconstruct(iVQuantity,akVertex,akNormal,akColor,akTexture,iTQuantity,aiConnect);
}

void TubeSurface::ComputeSinCos()
{
	// Compute slice vertex coefficients.  The first and last coefficients
	// are duplicated to allow a closed cross section that has two different
	// pairs of texture coordinates at the shared vertex.

	if (m_iSliceSamples + 1 != nAllocSinCos)
	{
		nAllocSinCos = m_iSliceSamples + 1;
		delete[]m_afSin;
		delete[]m_afCos;
		m_afSin = new float[nAllocSinCos];
		m_afCos = new float[nAllocSinCos];
	}

	PREFAST_ASSUME(m_iSliceSamples > 0 && m_iSliceSamples == nAllocSinCos - 1);

	float fInvSliceSamples = 1.0f / (float)m_iSliceSamples;
	for (int i = 0; i < m_iSliceSamples; i++)
	{
		float fAngle = gf_PI2 * fInvSliceSamples * i;
		m_afCos[i] = cosf(fAngle);
		m_afSin[i] = sinf(fAngle);
	}
	m_afSin[m_iSliceSamples] = m_afSin[0];
	m_afCos[m_iSliceSamples] = m_afCos[0];
}

void TubeSurface::ComputeVertices(Vec3* akVertex)
{
	float fTMin = m_pSpline->GetRangeStart();
	float fTRange = m_pSpline->GetRangeEnd() - fTMin;

	// sampling by arc length requires the total length of the curve
	/*	float fTotalLength;
	if ( m_bSampleByArcLength )
	//fTotalLength = m_pkMedial->GetTotalLength();
	fTotalLength = 0.0f;
	else
	fTotalLength = 0.0f;
	*/
	/*if (Cry3DEngineBase::GetCVars()->e_Ropes == 2)
	{
		for (int i = 0, npoints = m_pSpline->num_keys() - 1; i < npoints; i++)
		{
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(m_worldTM * m_pSpline->value(i), ColorB(0, 255, 0, 255), m_worldTM * m_pSpline->value(i + 1), ColorB(0, 255, 0, 255), 6);
		}
	}*/

	// vertex construction requires a normalized time (uses a division)
	float fDenom;
	if (m_bClosed)
		fDenom = 1.0f / (float)m_iMedialSamples;
	else
		fDenom = 1.0f / (float)(m_iMedialSamples - 1);

	Vec3 kT_Prev = Vec3(1.0f, 0, 0), kB = m_kUpVector;
	for (int iM = 0, iV = 0; iM < m_iMedialSamples; iM++)
	{
		float fT = fTMin + iM * fTRange * fDenom;

		float fRadius = m_fRadius;
		/*if (iM > 0.95f * m_iMedialSamples) // TODO: hardcoded persentage
		{
			fRadius = fRadius * 0.5f;
		}*/
			
		// compute frame (position P, tangent T, normal N, bitangent B)
		Vec3 kP, kP1, kT, kN;

		{
			// Always use 'up' vector N rather than curve normal.  You must
			// constrain the curve so that T and N are never parallel.  To
			// build the frame from this, let
			//     B = Cross(T,N)/Length(Cross(T,N))
			// and replace
			//     N = Cross(B,T)/Length(Cross(B,T)).

			//kP = m_pkMedial->GetPosition(fT);
			//kT = m_pkMedial->GetTangent(fT);
			if (iM < m_iMedialSamples - 1)
			{
				m_pSpline->interpolate(fT, kP);
				m_pSpline->interpolate(fT + 0.01f, kP1);
				kT = (kP1 - kP);
			}
			else
			{
				m_pSpline->interpolate(fT - 0.01f, kP1);
				m_pSpline->interpolate(fT, kP);
				kT = (kP - kP1);
			}

			if (!kT.IsZero())
				kT.NormalizeFast();
			else
			{
				// Take direction of last points.
				kT = kT_Prev;
			}
			kT_Prev = kT;
			//kT = m_pkMedial->GetTangent(fT);

			kB -= kT * (kB * kT);
			if (kB.len2() < sqr(0.001f))
				kB = kT.GetOrthogonal();
			//kB = kT.Cross(m_kUpVector);
			//if (kB.IsZero())
			//	kB = Vec3(1.0f,0,0);
			kB.NormalizeFast();
			kN = kB.Cross(kT);
			kN.NormalizeFast();
		}

		// compute slice vertices, duplication at end point as noted earlier
		int iSave = iV;
		for (int i = 0; i < m_iSliceSamples; i++)
		{
			akVertex[iV] = kP + fRadius * (m_afCos[i] * kN + m_afSin[i] * kB);

			/*if (Cry3DEngineBase::GetCVars()->e_Ropes == 2)
			{
				ColorB col = ColorB(255, 0, 0, 255);
				if (i == 0)
					col = ColorB(255, 0, 0, 255);
				else if (i == 1)
					col = ColorB(0, 255, 0, 255);
				else
					col = ColorB(0, 0, 255, 255);
				gEnv->pRenderer->GetIRenderAuxGeom()->DrawPoint(m_worldTM * akVertex[iV], col, 8);
			}*/

			iV++;
		}
		akVertex[iV] = akVertex[iSave];
		iV++;
	}

	if (m_bClosed)
	{
		for (int i = 0; i <= m_iSliceSamples; i++)
		{
			int i1 = Index(i, m_iMedialSamples);
			int i0 = Index(i, 0);
			akVertex[i1] = akVertex[i0];
		}
	}
}
//----------------------------------------------------------------------------
void TubeSurface::ComputeNormals()
{
	int iS, iSm, iSp, iM, iMm, iMp;
	Vec3 kDir0, kDir1;

	// interior normals (central differences)
	for (iM = 1; iM <= m_iMedialSamples - 2; iM++)
	{
		for (iS = 0; iS < m_iSliceSamples; iS++)
		{
			iSm = (iS > 0 ? iS - 1 : m_iSliceSamples - 1);
			iSp = iS + 1;
			iMm = iM - 1;
			iMp = iM + 1;

			kDir0 = m_akVertex[Index(iSm, iM)] - m_akVertex[Index(iSp, iM)];
			kDir1 = m_akVertex[Index(iS, iMm)] - m_akVertex[Index(iS, iMp)];

			if (kDir0.IsZero(1e-10f))
				kDir0 = Vec3(1, 0, 0);
			if (kDir1.IsZero(1e-10f))
				kDir1 = Vec3(0, 1, 0);

			Vec3 kN = kDir0.Cross(kDir1); // Normal
			Vec3 kT = kDir0;              // Tangent
			Vec3 kB = kDir1;              // Bitangent

			if (kN.IsZero(1e-10f))
				kN = Vec3(0, 0, 1);

			kN.NormalizeFast();
			kT.NormalizeFast();
			kB.NormalizeFast();

			int nIndex = Index(iS, iM);
			m_akNormals[nIndex] = kN;
			m_akTangents[nIndex] = kT;
			m_akBitangents[nIndex] = kB;
		}

		m_akNormals[Index(m_iSliceSamples, iM)] = m_akNormals[Index(0, iM)];
		m_akTangents[Index(m_iSliceSamples, iM)] = m_akTangents[Index(0, iM)];
		m_akBitangents[Index(m_iSliceSamples, iM)] = m_akBitangents[Index(0, iM)];
	}

	// boundary normals
	if (m_bClosed)
	{
		// central differences
		for (iS = 0; iS < m_iSliceSamples; iS++)
		{
			iSm = (iS > 0 ? iS - 1 : m_iSliceSamples - 1);
			iSp = iS + 1;

			// m = 0
			kDir0 = m_akVertex[Index(iSm, 0)] - m_akVertex[Index(iSp, 0)];
			kDir1 = m_akVertex[Index(iS, m_iMedialSamples - 1)] - m_akVertex[Index(iS, 1)];
			m_akNormals[iS] = kDir0.Cross(kDir1).GetNormalized();

			// m = max
			m_akNormals[Index(iS, m_iMedialSamples)] = m_akNormals[Index(iS, 0)];
		}
		m_akNormals[Index(m_iSliceSamples, 0)] = m_akNormals[Index(0, 0)];
		m_akNormals[Index(m_iSliceSamples, m_iMedialSamples)] = m_akNormals[Index(0, m_iMedialSamples)];
	}
	else
	{
		// one-sided finite differences

		// m = 0
		for (iS = 0; iS < m_iSliceSamples; iS++)
		{
			iSm = (iS > 0 ? iS - 1 : m_iSliceSamples - 1);
			iSp = iS + 1;

			kDir0 = m_akVertex[Index(iSm, 0)] - m_akVertex[Index(iSp, 0)];
			kDir1 = m_akVertex[Index(iS, 0)] - m_akVertex[Index(iS, 1)];
			//m_akNormals[Index(iS,0)] = kDir0.Cross(kDir1).GetNormalized();
			
			if (kDir0.IsZero())
				kDir0 = Vec3(1, 0, 0);
			if (kDir1.IsZero())
				kDir1 = Vec3(0, 1, 0);

			Vec3 kN = kDir0.Cross(kDir1); // Normal
			Vec3 kT = kDir0;              // Tangent
			Vec3 kB = kDir1;              // Bitangent

			if (kN.IsZero(1e-10f))
				kN = Vec3(0, 0, 1);

			kN.NormalizeFast();
			kT.NormalizeFast();
			kB.NormalizeFast();

			int nIndex = iS;
			m_akNormals[nIndex] = kN;
			m_akTangents[nIndex] = kT;
			m_akBitangents[nIndex] = kB;
		}
		m_akNormals[Index(m_iSliceSamples, 0)] = m_akNormals[Index(0, 0)];
		m_akTangents[Index(m_iSliceSamples, 0)] = m_akTangents[Index(0, 0)];
		m_akBitangents[Index(m_iSliceSamples, 0)] = m_akBitangents[Index(0, 0)];

		// m = max-1
		for (iS = 0; iS < m_iSliceSamples; iS++)
		{
			iSm = (iS > 0 ? iS - 1 : m_iSliceSamples - 1);
			iSp = iS + 1;
			kDir0 = m_akVertex[Index(iSm, m_iMedialSamples - 1)] - m_akVertex[Index(iSp, m_iMedialSamples - 1)];
			kDir1 = m_akVertex[Index(iS, m_iMedialSamples - 2)] - m_akVertex[Index(iS, m_iMedialSamples - 1)];
			//m_akNormals[iS] = kDir0.Cross(kDir1).GetNormalized();

			if (kDir0.IsZero())
				kDir0 = Vec3(1, 0, 0);
			if (kDir1.IsZero())
				kDir1 = Vec3(0, 1, 0);

			Vec3 kN = kDir0.Cross(kDir1); // Normal
			Vec3 kT = kDir0;              // Tangent
			Vec3 kB = kDir1;              // Bitangent

			if (kN.IsZero(1e-10f))
				kN = Vec3(0, 0, 1);

			kN.NormalizeFast();
			kT.NormalizeFast();
			kB.NormalizeFast();

			int nIndex = Index(iS, m_iMedialSamples - 1);
			m_akNormals[nIndex] = kN;
			m_akTangents[nIndex] = kT;
			m_akBitangents[nIndex] = kB;
		}
		m_akNormals[Index(m_iSliceSamples, m_iMedialSamples - 1)] = m_akNormals[Index(0, m_iMedialSamples - 1)];
		m_akTangents[Index(m_iSliceSamples, m_iMedialSamples - 1)] = m_akTangents[Index(0, m_iMedialSamples - 1)];
		m_akBitangents[Index(m_iSliceSamples, m_iMedialSamples - 1)] = m_akBitangents[Index(0, m_iMedialSamples - 1)];
	}
}

//----------------------------------------------------------------------------
void TubeSurface::ComputeTextures(const Vec2& rkTextureMin,
	const Vec2& rkTextureMax, Vec2* akTexture)
{
	Vec2 kTextureRange = rkTextureMax - rkTextureMin;
	int iMMax = (m_bClosed ? m_iMedialSamples : m_iMedialSamples - 1);
	for (int iM = 0, iV = 0; iM <= iMMax; iM++)
	{
		float fMRatio = ((float)iM) / ((float)iMMax);
		float fMValue = rkTextureMin.y + fMRatio * kTextureRange.y;
		for (int iS = 0; iS <= m_iSliceSamples; iS++)
		{
			float fSRatio = ((float)iS) / ((float)m_iSliceSamples);
			float fSValue = rkTextureMin.x + fSRatio * kTextureRange.x;
			akTexture[iV].x = fSValue;
			akTexture[iV].y = fMValue;
			iV++;
		}
	}
}
//----------------------------------------------------------------------------
void TubeSurface::ComputeConnectivity(int& riTQuantity, vtx_idx*& raiConnect, bool bInsideView)
{
	if (m_bClosed)
		riTQuantity = 2 * m_iSliceSamples * m_iMedialSamples;
	else
		riTQuantity = 2 * m_iSliceSamples * (m_iMedialSamples - 1);

	iNumIndices = riTQuantity * 3;

	if (m_bCap)
	{
		riTQuantity += ((m_iSliceSamples - 2) * 3) * 2;
		iNumIndices += ((m_iSliceSamples - 2) * 3) * 2;
	}

	if (nAllocIndices != iNumIndices)
	{
		if (raiConnect)
			delete[]raiConnect;
		nAllocIndices = iNumIndices;
		raiConnect = new vtx_idx[nAllocIndices];
	}

	int iM, iMStart, i0, i1, i2, i3, i;
	vtx_idx* aiConnect = raiConnect;
	for (iM = 0, iMStart = 0; iM < m_iMedialSamples - 1; iM++)
	{
		i0 = iMStart;
		i1 = i0 + 1;
		iMStart += m_iSliceSamples + 1;
		i2 = iMStart;
		i3 = i2 + 1;
		for (i = 0; i < m_iSliceSamples; i++, aiConnect += 6)
		{
			if (bInsideView)
			{
				aiConnect[0] = i0++;
				aiConnect[1] = i2;
				aiConnect[2] = i1;
				aiConnect[3] = i1++;
				aiConnect[4] = i2++;
				aiConnect[5] = i3++;
			}
			else  // outside view
			{
				aiConnect[0] = i0++;
				aiConnect[1] = i1;
				aiConnect[2] = i2;
				aiConnect[3] = i1++;
				aiConnect[4] = i3++;
				aiConnect[5] = i2++;

				/*if (Cry3DEngineBase::GetCVars()->e_Ropes == 2)
				{
					ColorB col = ColorB(100, 100, 0, 50);
					gEnv->pRenderer->GetIRenderAuxGeom()->DrawTriangle(m_worldTM * m_akVertex[aiConnect[0]], col, m_worldTM * m_akVertex[aiConnect[1]], col, m_worldTM * m_akVertex[aiConnect[2]], col);
					col = ColorB(100, 0, 100, 50);
					gEnv->pRenderer->GetIRenderAuxGeom()->DrawTriangle(m_worldTM * m_akVertex[aiConnect[3]], col, m_worldTM * m_akVertex[aiConnect[4]], col, m_worldTM * m_akVertex[aiConnect[5]], col);
				}*/
			}
		}
	}

	if (m_bClosed)
	{
		i0 = iMStart;
		i1 = i0 + 1;
		i2 = 0;
		i3 = i2 + 1;
		for (i = 0; i < m_iSliceSamples; i++, aiConnect += 6)
		{
			if (bInsideView)
			{
				aiConnect[0] = i0++;
				aiConnect[1] = i2;
				aiConnect[2] = i1;
				aiConnect[3] = i1++;
				aiConnect[4] = i2++;
				aiConnect[5] = i3++;
			}
			else  // outside view
			{
				aiConnect[0] = i0++;
				aiConnect[1] = i1;
				aiConnect[2] = i2;
				aiConnect[3] = i1++;
				aiConnect[4] = i3++;
				aiConnect[5] = i2++;
			}
		}
	}
	else if (m_bCap)
	{
		i0 = 0;
		i1 = 1;
		i2 = 2;
		// Begining Cap.
		for (i = 0; i < m_iSliceSamples - 2; i++, aiConnect += 3)
		{
			aiConnect[0] = i0;
			aiConnect[1] = i2++;
			aiConnect[2] = i1++;
		}
		i0 = iMStart;
		i1 = i0 + 1;
		i2 = i0 + 2;
		// Ending Cap.
		for (i = 0; i < m_iSliceSamples - 2; i++, aiConnect += 3)
		{
			aiConnect[0] = i0;
			aiConnect[1] = i1++;
			aiConnect[2] = i2++;
		}
	}
}
