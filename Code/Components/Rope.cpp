// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 31.10.2018, Alper �ekerci

#include "Rope.h"
#include "CrySchematyc\CoreAPI.h"

void CRope::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CRope));
		{
		}
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&CRope::Register)

void CRope::Initialize()
{
	mPointB = m_pEntity->GetWorldPos();
	mPointA = mPointB + m_pEntity->GetUpDir() * mMaxLength;

	CreatePoints();
	mSegmentL2 = pow(mSegmentLength, 2);

	// Render //	
	m_tubeSurface = new TubeSurface();

	string colorType = gEnv->pConsole->GetCVar("aug_cable_color")->GetString();
	if (colorType == "MONOCOLOR")
	{
		m_material = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(m_materialPath.value, false);
	}
	else if (colorType == "MULTICOLOR")
	{
		m_material = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(m_colorMaterialPath.value, false);
	}
}

CRope::~CRope()
{
	delete m_tubeSurface;
}

uint64 CRope::GetEventMask() const
{
	return ENTITY_EVENT_BIT(ENTITY_EVENT_PREPHYSICSUPDATE) | ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE) | ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_RESET) | ENTITY_EVENT_BIT(ENTITY_EVENT_PHYS_POSTSTEP) | ENTITY_EVENT_BIT(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);
}

void CRope::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		float dt = event.fParam[0];

		if (mBezier)
			MakeBezierCurve(); // Simply make a bezier curve with the mid points.
		else
			SolveMidPoints(); // Solves the mid points so that they are equally distanced.

		bool dirty = (GetPointA() - mLastDirtyPos).GetLengthFast() > 0.00001f;

		if (dirty)
		{
			mLastDirtyPos = mPoints[0].pos;
			UpdateRenderMesh();			
		}

		if (mDrawDebug) DrawDebugPoints();		
	}
	break;
	case ENTITY_EVENT_PREPHYSICSUPDATE:
	{
		float dt = event.fParam[0];
	}
	break;
	case ENTITY_EVENT_PHYS_POSTSTEP:
	{
	}
	break;
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
		// case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	case ENTITY_EVENT_RESET:
	{
		Reset();
	}
	break;
	}
}

void CRope::Reset()
{
	mPoints.clear();
	CreatePoints();	
	mLastDirtyPos = Vec3(9999, 9999, 9999);
}

void CRope::CreatePoints()
{
	mPoints.push_back(mPointA);

	Vec3 AtoB = mPointB - mPointA;
	mSegmentLength = AtoB.GetLength() / (mPointCount - 1);
	Vec3 AtoBDir = AtoB.GetNormalized();

	for (int i = 0; i < mPointCount - 2; ++i)
	{
		Vec3 pos = mPointA + AtoBDir * mSegmentLength * (i + 1.0f);
		mPoints.push_back(pos);
	}

	mPoints.push_back(mPointB);
}

void CRope::DrawDebugPoints()
{
	for (int i = 0; i < mPoints.size(); i++)
		mPoints[i].DrawDebug();
}

void CRope::AlignMidPoints()
{
	int lastIndex = mPoints.size() - 1;
	Vec3 pointA = mPoints[0].pos;
	Vec3 pointB = mPoints[lastIndex].pos;
	Vec3 AtoB = pointB - pointA;
	Vec3 midAB = (pointB + pointA) * 0.5f;
	Vec3 polePos = midAB + mPoleTarget;

	for (int i = 1; i < lastIndex; ++i)
	{
		float t = (float)i / lastIndex;
		mPoints[i].pos = BezierCurve(pointA, polePos, pointB, t);
	}
}

Vec3 CRope::BezierCurve(Vec3 p1, Vec3 p2, Vec3 p3, float t)
{
	return pow(1 - t, 2) * p1 + 2 * (1 - t) * t * p2 + t * t * p3;
}

void CRope::SolveMidPoints()
{
	int segmentCt = mPoints.size() - 1;

	for (int n = 0; n < mIterationCount; ++n)
	{
		std::vector<RopePoint> thetas;
		thetas.reserve(segmentCt);

		for (int i = 1; i <= segmentCt; ++i)
		{
			RopePoint theta;
			RopePoint prev = mPoints[i - 1];
			RopePoint point = mPoints[i];

			for (int axis = 0; axis < 3; ++axis)
			{
				theta[axis] = (prev.GetLength2(point) - mSegmentL2) * (prev[axis] - point[axis]);
			}

			thetas.push_back(theta);
		}

		for (int i = 1; i <= segmentCt - 1; ++i)
		{
			for (int axis = 0; axis < 3; ++axis)
			{
				mPoints[i][axis] -= mLearnRate * (-thetas[i - 1][axis] + thetas[i][axis]);
			}
		}
	}
}

void CRope::MakeBezierCurve()
{
	Vec3 pointA = mPoints[0];
	Vec3 pointB = mPoints[mPoints.size() - 1];
	Vec3 midPoint = (pointA + pointB) * 0.5f;

	float midLength = (midPoint - pointA).GetLength();
	float bezierDist2 = 0.25f * mMaxLength * mMaxLength - midLength * midLength;

	if (bezierDist2 < 0)
	{
		// CryLog("max rope length is exceeded");
		mExceededMaxLength = true;
		return;
	}

	mExceededMaxLength = false;

	float bezierDist = sqrt(bezierDist2);

	midPoint += mPoleTarget * bezierDist;

	for (int i = 1; i <= mPoints.size() - 2; ++i)
	{
		float t = i / (mPoints.size() - 1.0f);
		mPoints[i].pos = BezierCurve(pointA, midPoint, pointB, t);
	}
}

float CRope::CalcSSE()
{
	float sse = 0;

	for (int i = 1; i <= mPoints.size() - 1; ++i)
	{
		sse += pow(mPoints[i].GetLength2(mPoints[i - 1]) - mSegmentL2, 2);
	}

	return sse;
}

void CRope::CreateRenderMesh()
{
	m_pRenderMesh = gEnv->pRenderer->CreateRenderMeshInitialized(
		NULL, 3, EDefaultInputLayouts::P3F_C4B_T2F,
		NULL, 3, prtTriangleList,
		"Rope", GetName(),
		eRMT_Dynamic, 1, 0, NULL, NULL, false, false);

	if (!m_material)
	{
		if (!m_materialPath.value.empty())
			m_material = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(m_materialPath.value, false);

		if (!m_material)
			m_material = gEnv->p3DEngine->GetMaterialManager()->GetDefaultMaterial();
	}

	CRenderChunk chunk;
	memset(&chunk, 0, sizeof(chunk));
	if (m_material)
		chunk.m_nMatFlags = m_material->GetFlags();
	chunk.nFirstIndexId = 0;
	chunk.nFirstVertId = 0;
	chunk.nNumIndices = 3;
	chunk.nNumVerts = 3;
	chunk.m_texelAreaDensity = 1.0f;
	m_pRenderMesh->SetChunk(0, chunk);
}

void CRope::UpdateRenderMesh()
{
	if (!m_pRenderMesh)
	{
		CreateRenderMesh();
		if (!m_pRenderMesh)
			return;
	}

	int numSplinePoints = mPoints.size();
	m_spline.resize(numSplinePoints);

	for (int pnt = 0; pnt < numSplinePoints; pnt++)
	{
		m_spline.key(pnt).flags = 0;
		m_spline.time(pnt) = (float)pnt;
		m_spline.value(pnt) = mPoints[pnt].pos;
	}
	m_spline.SetRange(0, (float)numSplinePoints - 1);
	m_spline.SetModified(true);

	int nLengthSamples = nNumSegments + 1;

	Vec2 vTexMin(0, 0);
	Vec2 vTexMax(fTextureTileU, fTextureTileV);

	// m_pEntity->SetPos(mPoints[numSplinePoints - 1].pos);
	Matrix34 worldTM = m_pEntity->GetWorldTM();
	Matrix34 invWorldTM = worldTM.GetInverted();
	m_tubeSurface->m_worldTM = worldTM;

	m_tubeSurface->GenerateSurface(&m_spline, fThickness, false, -(mPoints[numSplinePoints - 1].pos - mPoints[0].pos).GetOrthogonal().GetNormalized(),
		nLengthSamples, nNumSides, true, false, false, false, &vTexMin, &vTexMax);

	int nNewVertexCount = m_tubeSurface->iVQuantity;

	m_pRenderMesh->LockForThreadAccess();
	m_pRenderMesh->UpdateVertices(NULL, nNewVertexCount, 0, VSF_GENERAL, 0u);

	int nPosStride = 0;
	int nUVStride = 0;
	int nTangsStride = 0;

	byte* pVertPos = m_pRenderMesh->GetPosPtr(nPosStride, FSL_VIDEO_CREATE);
	byte* pVertTexUV = m_pRenderMesh->GetUVPtr(nUVStride, FSL_VIDEO_CREATE);
	byte* pTangents = m_pRenderMesh->GetTangentPtr(nTangsStride, FSL_VIDEO_CREATE);

	for (int i = 0; i < nNewVertexCount; i++)
	{
		Vec3 vP = Vec3(m_tubeSurface->m_akVertex[i].x, m_tubeSurface->m_akVertex[i].y, m_tubeSurface->m_akVertex[i].z);
		Vec2 vT = Vec2(m_tubeSurface->m_akTexture[i].x, m_tubeSurface->m_akTexture[i].y);

		*((Vec3*)pVertPos) = invWorldTM * vP;
		*((Vec2*)pVertTexUV) = vT;

		/**(SPipTangents*)pTangents = SPipTangents(
			m_tubeSurface->m_akTangents[i],
			m_tubeSurface->m_akBitangents[i], 1); // original */

		*(SPipTangents*)pTangents = SPipTangents(
			Vec3(1, 0, 0),
			Vec3(0, 1, -0.1f), 1);//*/

			// vP = *((Vec3*)pVertPos) + m_pEntity->GetWorldPos();
		Vec3 myNormal = m_tubeSurface->m_akTangents[i].Cross(m_tubeSurface->m_akBitangents[i]);

		/*gEnv->pAuxGeomRenderer->DrawLine(vP, Col_Red, vP + m_tubeSurface->m_akTangents[i]*0.05f, Col_Red, 2);
		gEnv->pAuxGeomRenderer->DrawLine(vP, Col_Blue, vP + m_tubeSurface->m_akNormals[i]*0.05f, Col_Blue, 2);
		// gEnv->pAuxGeomRenderer->DrawLine(vP, Col_Blue, vP + myNormal * 0.05f, Col_Blue, 2);
		gEnv->pAuxGeomRenderer->DrawLine(vP, Col_Green, vP + m_tubeSurface->m_akBitangents[i] * 0.05f, Col_Green, 2);*/

		pVertPos += nPosStride;
		pVertTexUV += nUVStride;
		pTangents += nTangsStride;
	}

	m_pRenderMesh->UnlockStream(VSF_GENERAL);
	m_pRenderMesh->UnlockStream(VSF_TANGENTS);
	m_pRenderMesh->UpdateIndices(m_tubeSurface->m_pIndices, m_tubeSurface->iNumIndices, 0, 0u);

	CRenderChunk* pChunk = &m_pRenderMesh->GetChunks()[0];
	if (m_material)
		pChunk->m_nMatFlags = m_material->GetFlags();
	pChunk->nNumIndices = m_tubeSurface->iNumIndices;
	pChunk->nNumVerts = m_tubeSurface->iVQuantity;
	m_pRenderMesh->SetChunk(0, *pChunk);

	m_pRenderMesh->UnLockForThreadAccess();

	if (m_renderSlot == -1)
	{
		m_statObj = gEnv->p3DEngine->CreateStatObj();
		m_indexedMesh = m_statObj->GetIndexedMesh();

		m_pRenderMesh->GetIndexedMesh(m_indexedMesh);

		m_indexedMesh->CalcBBox();

		m_statObj->SetBBoxMin(m_indexedMesh->GetBBox().min);
		m_statObj->SetBBoxMax(m_indexedMesh->GetBBox().max);

		m_statObj->Invalidate();

		m_renderSlot = m_pEntity->SetStatObj(m_statObj, -1, false);
		m_pEntity->SetSlotMaterial(m_renderSlot, m_material);
	}
	else
	{
		m_pRenderMesh->GetIndexedMesh(m_indexedMesh);

		m_indexedMesh->CalcBBox();

		m_statObj->SetBBoxMin(m_indexedMesh->GetBBox().min);
		m_statObj->SetBBoxMax(m_indexedMesh->GetBBox().max);

		m_statObj->Invalidate();
	}
}