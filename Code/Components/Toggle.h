// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 9:10 AM 12/25/2018, Utku Guler
// Edited by Ali Mehmet Altundag on 11:54 19.01.2019

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"

class CToggleComponent
	: public CInteractionBaseComponent
{
private:
	Vec3 m_meshScale{ 1,1,1 };
	Vec3 m_meshPos{ 0,0,0 };
	Vec3 m_meshRot{ 0,0,0 };

public:
	CToggleComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CToggleComponent>& desc)
	{
		desc.SetGUID("{1B307A11-4142-4638-AF2B-1153CDCD7507}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Toggle");
		desc.SetDescription("Basic toggle functionality.");

		desc.AddMember(&CToggleComponent::m_state, 'iiso', "IsInitialStateOn", "Is Initial StateOn", "Initial state", true);
		desc.AddMember(&CToggleComponent::m_modelOn, 'moon', "OnStateModel", "On State Model", "On State Model", "");
		desc.AddMember(&CToggleComponent::m_modelOff, 'moof', "OffStateModel", "Off State Model", "Off State Model", "");

		desc.AddMember(&CToggleComponent::m_meshScale, 'mscl', "MeshScale", "Mesh Scale", "", (1.0f, 1.0f, 1.0f));
		desc.AddMember(&CToggleComponent::m_meshPos, 'mpos', "MeshPos", "Mesh Pos", "", (0.0f, 0.0f, 0.0f));
		desc.AddMember(&CToggleComponent::m_meshRot, 'mrot', "MeshRot", "Mesh Rot", "", (0.0f, 0.0f, 0.0f));
	}

	// IEntityComponent
	virtual void Initialize() override;
	virtual void ProcessEvent(const SEntityEvent& event) override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override 
	{ return 
		  ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_RESET)		
		| ENTITY_EVENT_BIT(ENTITY_EVENT_START_GAME)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_START_LEVEL);
	}
	// ~IEntitycomponent

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::TOGGLE; }
	// ~CInteractionBaseComponent

	bool GetCurrentState() const { return m_state; }

	void Toggle() { m_state = !m_state; Reset(); }
	void SetState(bool state) { m_state = state; Reset(); }

private:
	void Reset();

	bool m_state;
	bool m_triggerBuffer;

	Schematyc::GeomFileName m_modelOn;
	Schematyc::GeomFileName m_modelOff;
};