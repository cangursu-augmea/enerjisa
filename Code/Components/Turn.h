// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 9:25 AM 01/07/2019, Utku Guler
// Edited by Ali Mehmet Altundag on 11:54 19.01.2019

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntity.h>
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>

class CTurnComponent
	: public CInteractionBaseComponent
{
public:
	CTurnComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CTurnComponent>& desc)
	{
		desc.SetGUID("{9DEAE4B5-7B82-4C88-9FAF-38169E9E4D8B}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Turn");
		desc.SetDescription("Basic Turn functionality.");
		desc.AddMember(&CTurnComponent::m_turnAxis, 'axis', "TurnAxis", "Turn Axis", "Turn Axis", FORWARD_DIRECTION);
		desc.AddMember(&CTurnComponent::m_turnAxisOffset, 'axof', "TurnAxisOffset", "Turn Axis Offset", "Turn Axis Offset", ZERO);
		desc.AddMember(&CTurnComponent::m_turnAmount, 'tamo', "TurnAmount", "Turn Amount", "Turn Amount", 90.f);
		desc.AddMember(&CTurnComponent::m_turnSpeed, 'tspd', "TurnSpeed", "Turn Speed", "Turn Speed", 10.f);
		desc.AddMember(&CTurnComponent::m_turnMeshComponent, 'tmsc', "TurnMeshComponent", "Turn Mesh Component", "Turn Mesh Component", false);
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::TURN; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual void ProcessEvent(const SEntityEvent& event) override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override 
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			 | ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE);
	}
	// ~IEntitycomponent

	bool GetTurnActionStarted();

private:
	Cry::DefaultComponents::CStaticMeshComponent* m_pMeshComponent;

	bool m_triggerBuffer;
	bool m_isTurnActionStarted;

	Vec3 m_turnAxis;
	Vec3 m_turnAxisOffset;
	float m_turnAmount;
	float m_sumOfTurn;
	float m_turnSpeed;
	float m_directionOfRotation;

	bool m_turnMeshComponent;

	Matrix34 m_calculatedTM;

	void DebugDraw();
	void Reset();
	void GetMatrix(Matrix34& TM);
	void SetMatrix(Matrix34& TM);

	void Log(bool state);
};