// Copyright 2017 Augmea Inc. All rights reserved.
// Created: 10:04 22.05.2018 by Ali Mehmet Altundag

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include <functional>
#include "Interaction/InteractionBaseComponent.h"

const int UNKNOWN_TERMINAL_ID = -1;

typedef std::function<void(const char*, bool)> FemaleTerminalCallback;

class CPlugMaleTerminal;
class CPlugFemaleTerminal
	: public CInteractionBaseComponent
{
public:
	CPlugFemaleTerminal();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CPlugFemaleTerminal>& desc)
	{
		desc.SetGUID("{6BE12DD7-92D6-40B8-983E-B10B01234728}"_cry_guid);
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Augmea Plug Female Terminal");
		desc.SetDescription("Terminal female ends for augmea-plugs.");

		desc.AddMember(&CPlugFemaleTerminal::m_terminalId, 'tid', "TerminalId", "Terminal Id", "Terminal id (doesn't have to be unique)", true);

		desc.AddMember(&CPlugFemaleTerminal::m_isConstraintPlacementActive, 'cplc', "ConstraintPlacement", "Constraint Placement", "Set if you want to place terminal by physics constraints", true);
		desc.AddMember(&CPlugFemaleTerminal::m_constraintPlacementDir, 'pvec', "ConstraintPlacementVector", "Constraint Placement Vector", "Placement direction vector", true);

		desc.AddMember(&CPlugFemaleTerminal::m_lockVec, 'lvec', "LockVector", "Lock Vector.", "Lock vector, will be normalized", true);
	}

	// IEntityComponent
	virtual uint64 GetEventMask() const override { 
	return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_RESET)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_INIT)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_LEVEL_LOADED);
	}
	virtual void Initialize() override;
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	// CInteractionBaseComponent
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::PLUG; }
	virtual SInteractionFeedback InteractionTick() override { return m_feedback; }
	// ~CInteractionBaseComponent

	/*! returns true if connected */
	bool IsConnected() { return nullptr != m_pConnectedTo; }
	CPlugMaleTerminal* GetConnectedMaleTerminal() { return m_pConnectedTo; }

	/* assigns new connection if available for it. returns true if
	connection is successfull */
	bool Connect(CPlugMaleTerminal* pMaleTerminal);
	void Disconnect();

	void SetEnabled(bool enabled) { m_enabled = enabled; }
	bool IsEnabled() { return m_enabled; }

	int GetTerminalId() { return m_terminalId; }
	void SetTerminalId(int id) { m_terminalId = id; }
	IPhysicalEntity* GetConstrainedPhysicalEntity() { return m_pConstrainedTo; }
	Vec3 GetLockVec() { return m_lockVec; }

	void AddListener(uint64_t id, FemaleTerminalCallback fn);
	void RemoveListener(uint64_t id);

private:
	int m_terminalId;
	bool m_enabled = true;

	CPlugMaleTerminal* m_pConnectedTo;

	// members to provide constrained placement
	// this feature used when you want a cable is connected by default
	bool m_isConstraintPlacementActive;
	Vec3 m_constraintPlacementDir;
	int m_constraintPlacementId;
	IPhysicalEntity* m_pConstrainedTo;

	Vec3 m_lockVec;

	std::map<uint64_t, FemaleTerminalCallback> m_callbacks;
	void Reset();
	void Stick();
	void Unstick();
	void Notify(CPlugMaleTerminal* pMaleTerminal, bool isConnected);
};