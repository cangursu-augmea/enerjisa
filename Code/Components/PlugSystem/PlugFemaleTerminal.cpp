// Copyright 2017 Augmea Inc. All rights reserved.
// Created: 10:04 22.05.2018 by Ali Mehmet Altundag

#include "StdAfx.h"
#include "PlugFemaleTerminal.h"
#include "PlugMaleTerminal.h"
#include "Interaction/InteractionManagerComponent.h"

void CPlugFemaleTerminal::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CPlugFemaleTerminal));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CPlugFemaleTerminal::Register)

CPlugFemaleTerminal::CPlugFemaleTerminal()
	: m_terminalId(UNKNOWN_TERMINAL_ID)
	, m_pConnectedTo(nullptr)
	, m_isConstraintPlacementActive(false)
	, m_constraintPlacementDir(FORWARD_DIRECTION)
	, m_constraintPlacementId(-1)
	, m_pConstrainedTo(nullptr)
	, m_lockVec(0, 0, 1)
{
}

void CPlugFemaleTerminal::Initialize()
{
	Reset();
}

void CPlugFemaleTerminal::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	case ENTITY_EVENT_INIT:
	case ENTITY_EVENT_RESET:
	case ENTITY_EVENT_LEVEL_LOADED:
	{
		Reset();
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
#ifdef _DEBUG
		if (gEnv->IsEditor() && m_isConstraintPlacementActive)
		{
			Vec3 dir = Matrix33(m_pEntity->GetWorldTM()) * m_constraintPlacementDir;
			Vec3 pos = m_pEntity->GetWorldPos();
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(pos, Col_Green, pos + dir, Col_Green);
		}
#endif
	}
	break;
	}
}

bool CPlugFemaleTerminal::Connect(CPlugMaleTerminal* pMaleTerminal)
{
	if (!IsConnected())
	{
		m_pConnectedTo = pMaleTerminal;
		Notify(pMaleTerminal, true);
		return true;
	}

	return false;
}

void CPlugFemaleTerminal::Disconnect()
{
	Notify(m_pConnectedTo, false);
	m_pConnectedTo = nullptr;
}

void CPlugFemaleTerminal::AddListener(uint64_t id, FemaleTerminalCallback fn)
{
	m_callbacks.insert(std::make_pair(id, fn));
}

void CPlugFemaleTerminal::RemoveListener(uint64_t id)
{
	auto it = m_callbacks.find(id);
	if (m_callbacks.end() == it) return;
	m_callbacks.erase(it);
}

void CPlugFemaleTerminal::Reset()
{
	Disconnect();
	Unstick();
	Stick();
}

void CPlugFemaleTerminal::Stick()
{
	// nothing to do or too early to do something...
	if (!m_isConstraintPlacementActive || !m_pEntity->GetPhysicalEntity()) return;

	// try to find a physical entity to stick
	IPhysicalEntity* pSkipEntites[1];
	// dont hit yourself!
	pSkipEntites[0] = m_pEntity->GetPhysicalEntity();
	ray_hit hit;
	Vec3 dir = Matrix33(m_pEntity->GetWorldTM()) * m_constraintPlacementDir;
	gEnv->pPhysicalWorld->RayWorldIntersection(m_pEntity->GetWorldPos(), dir, ent_static | ent_rigid | ent_sleeping_rigid, rwi_colltype_any | rwi_stop_at_pierceable, &hit, 1, pSkipEntites, 1);
	// nothing found to stick...
	if (hit.dist == -1.f) return;

	// ok, stick
	pe_action_add_constraint aac;
	aac.pt[0] = hit.pt;
	aac.partid[0] = hit.partid;
	aac.flags = constraint_no_tears | constraint_no_enforcement | constraint_ignore_buddy;// | constraint_no_rotation;
	m_pConstrainedTo = aac.pBuddy = hit.pCollider;
	//aac.maxPullForce = 1000;
	//aac.maxBendTorque = 10000;
	aac.hardnessLin = 16;
	aac.xlimits[0] = 0.f;
	aac.yzlimits[0] = 0.f;
	m_constraintPlacementId = m_pEntity->GetPhysicalEntity()->Action(&aac);
}

void CPlugFemaleTerminal::Unstick()
{
	// nothing to do or too early to do something...
	if (!m_isConstraintPlacementActive || -1 == m_constraintPlacementId || !m_pEntity->GetPhysicalEntity()) return;

	pe_action_update_constraint pauc;
	pauc.bRemove = 1;
	pauc.idConstraint = m_constraintPlacementId;
	m_pEntity->GetPhysicalEntity()->Action(&pauc);
	// actual reset
	m_constraintPlacementId = -1;
}

void CPlugFemaleTerminal::Notify(CPlugMaleTerminal* pMaleTerminal, bool isConnected)
{
	if (!pMaleTerminal) return;

	for (auto fn : m_callbacks)
		fn.second(pMaleTerminal->GetEntity()->GetName(), isConnected);
}