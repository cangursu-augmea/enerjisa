// Copyright 2017 Augmea Inc. All rights reserved.
// Created: 10:04 22.05.2018 by Ali Mehmet Altundag

#include "StdAfx.h"
#include "PlugMaleTerminal.h"
#include "GamePlugin.h"
#include "Interaction/Interaction.h"

#include <CrySensorSystem/Interface/ISensorSystem.h>
#include <CrySensorSystem/Interface/ISensorMap.h>
#include <CrySensorSystem/Interface/ICrySensorSystemPlugin.h>

#include "Components/Carry.h"

void CPlugMaleTerminal::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CPlugMaleTerminal));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CPlugMaleTerminal::Register)

CPlugMaleTerminal::CPlugMaleTerminal()
	: m_terminalId(UNKNOWN_TERMINAL_ID)
	, m_terminalPairId(UNKNOWN_TERMINAL_ID)
	, m_pSensorMap(nullptr)
	, m_pSensorSystem(nullptr)
	, m_bound(Sphere(Vec3(type_zero::ZERO), 0.1f))
	, m_boundOffset(ZERO)
	, m_boundRadius(0.1f)
	, m_pConnectedTo(nullptr)
	, m_connectionDir(FORWARD_DIRECTION)
	, m_connectionOffset(ZERO)
	, m_offsetTM(IDENTITY)
	, m_constraintId(-1)
	, m_pInteraction(nullptr)
	, m_hasLock(false)
	, m_lockVec(0, 0, 1)
	, m_tolerance(1.f)
	, m_isBreakable(false)
{
	auto pPluginManager = GetISystem()->GetIPluginManager();

	auto *pSensorSystemPlugin = pPluginManager->QueryPlugin<Cry::SensorSystem::ICrySensorSystemPlugin>();

	if (pSensorSystemPlugin == nullptr) {
		CryWarning(EValidatorModule::VALIDATOR_MODULE_SYSTEM, EValidatorSeverity::VALIDATOR_ASSERT, "Sensor volume plugin not found!");
		return;
	}

	m_pSensorSystem = &(pSensorSystemPlugin->GetSensorSystem());
	m_pSensorMap = &(m_pSensorSystem->GetMap());
}

void CPlugMaleTerminal::Initialize()
{
	m_pInteraction = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	if (!m_pInteraction)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "CPlugMaleTerminal component needs to have interaction component attached to the entity.");
	}

	Disconnect();
	Reset();
}

void CPlugMaleTerminal::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	case ENTITY_EVENT_INIT:
	case ENTITY_EVENT_RESET:
	case ENTITY_EVENT_LEVEL_LOADED:
	{
		Reset();
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{

		if (gEnv->IsEditor())
		{
			//m_bound.DebugDraw(Col_Blue);
		}

		// check interaction state to determine whether or not we re carried by
		// if we re carried and have a current connection, disconnect...
		//if (m_pInteraction && m_pConnectedTo)
		//{
		//	auto interactionMap = m_pInteraction->GetInteractionFeedbackMap();
		//	auto it = interactionMap.find(EInteractionComponenetId::CARRY);
		//
		//	if (interactionMap.end() != it && (EInteractionPhase::ONGOING != it->second.phase && EInteractionFeedback::SUCCESS != it->second.feedback))
		//	{
		//		Disconnect();
		//		return;
		//	}
		//}

		if (m_pInteraction && m_pInteraction->IsInteractionLocked())
		{
			Disconnect();
			return;
		}

		// scan for new connectionsfv
		m_bound = Cry::SensorSystem::CSensorBounds(Sphere(m_pEntity->GetWorldTM() * m_boundOffset, m_boundRadius));
		CPlugFemaleTerminal* pFemaleTerminal = FindTerminalToConnect();

		// check if we lost the connection we had before
		if (m_isBreakable && IsConnected() && m_pConnectedTo != pFemaleTerminal)
			Disconnect();

		// nothing to do
		if (nullptr == pFemaleTerminal)
			return;

		// check if we have the correct type of dest
		if (m_terminalId != pFemaleTerminal->GetTerminalId())
			return;

		// check if this terminal has lock mechanism and if so check the lock state
		if (m_hasLock)
		{
			Vec3 n1 = Matrix33(m_pEntity->GetWorldTM()) * m_lockVec.GetNormalized();
			Vec3 n2 = Matrix33(pFemaleTerminal->GetEntity()->GetWorldTM()) * pFemaleTerminal->GetLockVec().GetNormalized();
			float errMag = (1.f - n1.dot(n2)) * 0.5f;
			// no, you cannot connect...
			if (errMag >= m_tolerance) return;
		}

		// check if the female terminal is available when 
		// we try for the first connection. if not, do nothing
		if (nullptr == m_pConnectedTo && pFemaleTerminal->IsConnected())
			return;

		// we still carry it... so, do nothing!
		//if (m_pInteraction && EInteractionFeedback::CARRIED == m_pInteraction->GetLastFeedback().interactionFeedback)
		//	return;

		// we already have a connection
		if (m_pConnectedTo)
			return;

		// ok all conditions are stisified so try to connect...
		Connect(pFemaleTerminal);
	}
	break;
	}
}

void CPlugMaleTerminal::Reset()
{
	Disconnect();
	m_offsetTM = Matrix34(Matrix33::CreateRotationVDir(m_connectionDir, 0.f), m_connectionOffset);

	// TODO
	//if (gPlugRegistry) gPlugRegistry->Register(m_terminalPairId, this);
}

void CPlugMaleTerminal::Disconnect()
{
	// nothing to do
	if (!m_pConnectedTo || !m_pEntity->GetPhysicalEntity()) return;

	Log(false);

	if (IsPhysicalConstraintNeeded())
	{
		pe_action_update_constraint pauc;
		pauc.bRemove = 1;
		pauc.idConstraint = m_constraintId;
		m_pEntity->GetPhysicalEntity()->Action(&pauc);
		m_constraintId = -1;
	}

	m_pConnectedTo->Disconnect();
	m_pConnectedTo = nullptr;
}

void CPlugMaleTerminal::Connect(CPlugFemaleTerminal* pDest)
{
	// try to connect...
	if (!pDest->Connect(this))
		return;

	Matrix34 worldTM = pDest->GetEntity()->GetWorldTM() * m_offsetTM;

	// set physical tm
	pe_params_pos ppp;
	ppp.pMtx3x4 = &worldTM;
	if(auto phyEnt = m_pEntity->GetPhysicalEntity())
	{
		phyEnt->SetParams(&ppp);
	}
	else
	{
		CryLogAlways("physical entity is null");
		return;
	}

	// ..now for the entity sys
	m_pEntity->SetWorldTM(worldTM);

	if (IsPhysicalConstraintNeeded())
	{
		pe_action_add_constraint aac;
		aac.pt[0] = m_pEntity->GetWorldTM().GetTranslation();
		aac.flags = constraint_no_tears | constraint_no_enforcement | constraint_ignore_buddy | constraint_no_rotation;
		IPhysicalEntity* pConstrainedEntity = pDest->GetConstrainedPhysicalEntity();
		if (pConstrainedEntity)
			aac.pBuddy = pConstrainedEntity;
		else
			aac.pBuddy = WORLD_ENTITY;
		aac.maxPullForce = 1000;
		aac.maxBendTorque = 10000;
		aac.hardnessLin = 16;
		aac.xlimits[0] = 0.f;
		aac.yzlimits[0] = 0.f;
		m_constraintId = m_pEntity->GetPhysicalEntity()->Action(&aac);
	}

	// ok, we know whom we re attached...
	m_pConnectedTo = pDest;

	Log(true);
}

CPlugFemaleTerminal* CPlugMaleTerminal::FindTerminalToConnect()
{
	m_sensorVolumeQueryResult.clear();
	m_pSensorMap->Query(m_sensorVolumeQueryResult, m_bound, m_pSensorSystem->GetTagLibrary().GetTag("Default"));

	CPlugFemaleTerminal* pClosestFemaleTerminal = nullptr;
	float dist = std::numeric_limits<float>::max();
	// collect all female terminals
	for (auto it : m_sensorVolumeQueryResult)
	{
		auto hitParams = m_pSensorMap->GetVolumeParams(it);

		IEntity* pDestEntity = gEnv->pEntitySystem->GetEntity(hitParams.entityId);
		if (!pDestEntity)
			continue;

		CPlugFemaleTerminal* pFemaleComp = pDestEntity->GetComponent<CPlugFemaleTerminal>();
		if (!pFemaleComp) continue;

		// check if we have the correct type of dest
		if (m_terminalId != pFemaleComp->GetTerminalId() || !pFemaleComp->IsEnabled())
			continue;

		if (dist > pFemaleComp->GetEntity()->GetWorldPos().GetDistance(m_pEntity->GetWorldPos()))
		{
			pClosestFemaleTerminal = pFemaleComp;
		}

		// calc. dist for next cmp
		dist = pClosestFemaleTerminal->GetEntity()->GetWorldPos().GetDistance(m_pEntity->GetWorldPos());
	}

	return pClosestFemaleTerminal;
}

void CPlugMaleTerminal::Log(bool isConnected)
{
	StateManager::Event event;
	event.result.bValue = isConnected;
	event.objects = { m_pEntity->GetName(), m_pConnectedTo->GetEntity()->GetName()};
	event.action = EInteractionComponenetIDs::PLUG;
	CGamePlugin::GetStateManager().Insert(event);
	// TODO:
	//SAugLogParameters logParams;
	//logParams.AddParameter("port", m_pConnectedTo->GetEntity()->GetName());
	//const char* val = isConnected ? "1" : "0";
	//logParams.AddParameter("value", val);
	//gAugLogger->LogAction("PlugConnection", m_pEntity->GetName(), logParams);
}

CPlugMaleTerminal* CPlugMaleTerminal::GetPair()
{
	// TODO
	//if (!gPlugRegistry) return nullptr;

	//return gPlugRegistry->GetPair(m_terminalPairId, this);

	return nullptr;
}

void CPlugMaleTerminal::SetConnectionOffSet(Vec3 newOffSet)
{	
	m_connectionOffset = newOffSet;
}

Vec3 CPlugMaleTerminal::GetConnectionOffSet() const
{
	return m_connectionOffset;
}

bool CPlugMaleTerminal::IsPhysicalConstraintNeeded()
{
	CCarryComponent* pCarry = m_pEntity->GetComponent<CCarryComponent>();
	if (pCarry && pCarry->IsPhysicalPositioningForced()) return false;

	return true;
}