// Copyright 2017 Augmea Inc. All rights reserved.
// Created: 10:04 22.05.2018 by Ali Mehmet Altundag

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include <CrySensorSystem/Interface/ISensorSystem.h>
#include <CrySensorSystem/Interface/ISensorMap.h>
#include "Interaction/InteractionBaseComponent.h"
#include "PlugFemaleTerminal.h"

class CPlugMaleTerminal
	: public CInteractionBaseComponent
{
public:
	CPlugMaleTerminal();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CPlugMaleTerminal>& desc)
	{
		desc.SetGUID("{53EC5C11-0D91-4775-BFFF-0D27DA8DD7C1}"_cry_guid);
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Augmea Plug Male Terminal");
		desc.SetDescription("Terminal ends for augmea-plugs.");
		//desc.AddBase<CInteractionBaseComponent>();

		desc.AddMember(&CPlugMaleTerminal::m_terminalId, 'tid', "TerminalId", "Terminal Id", "Terminal id (doesn't have to be unique)", true);
		desc.AddMember(&CPlugMaleTerminal::m_terminalPairId, 'tpid', "TerminalPairId", "Terminal Pair Id", "Terminal pair id (doesn't have to be unique)", true);
		desc.AddMember(&CPlugMaleTerminal::m_boundOffset, 'off', "Offset", "Bound Offset", "Bound offset", true);
		desc.AddMember(&CPlugMaleTerminal::m_boundRadius, 'rdi', "Radius", "Bound Radius", "Boundradius", true);
		desc.AddMember(&CPlugMaleTerminal::m_connectionDir, 'dir', "ConnectionDir", "Connection direction", "Boundradius", true);
		desc.AddMember(&CPlugMaleTerminal::m_connectionOffset, 'pos', "ConnectionOffset", "Connection position offset", "Connection position offset", true);

		desc.AddMember(&CPlugMaleTerminal::m_hasLock, 'lock', "LockValid", "Has Lock", "Set if lock needed for this terminal", true);
		desc.AddMember(&CPlugMaleTerminal::m_lockVec, 'lvec', "LockVector", "Lock Vector", "Lock vector, will be normalized", true);
		desc.AddMember(&CPlugMaleTerminal::m_tolerance, 'ltol', "LockTolerance", "Lock tolerance", "Lock Tolerance", true);

		desc.AddMember(&CPlugMaleTerminal::m_isBreakable, 'brk', "Breakable", "Is breakable", "check if this terminal breakable", true);
	}

	// IEntityComponent
	virtual uint64 GetEventMask() const override { 
	return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_RESET)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_INIT)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_LEVEL_LOADED);
	}
	virtual void Initialize() override;
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	// CInteractionBaseComponent
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::PLUG; }
	virtual SInteractionFeedback InteractionTick() override { return m_feedback; }
	// ~CInteractionBaseComponent

	bool IsConnected() { return nullptr != m_pConnectedTo; }
	CPlugFemaleTerminal* GetConnectedFemale() { return m_pConnectedTo; }

	/* might return nullptr, always check if null */
	CPlugMaleTerminal* GetPair();

	void SetConnectionOffSet(Vec3 newOffSet);
	Vec3 GetConnectionOffSet()const;

private:
	// this will be our constraint providing us the connection
	// between the male and female
	int m_constraintId;

	// will not be changed in the sim
	int m_terminalId;

	// terminal id that is used to pair terminals
	int m_terminalPairId;

	// male terminal params
	Vec3 m_boundOffset;
	float m_boundRadius;

	// vectors to define connector placement
	Vec3 m_connectionDir;
	Vec3 m_connectionOffset;

	// male terminals will be searching for female ones
	Cry::SensorSystem::CSensorBounds m_bound;
	Cry::SensorSystem::SensorVolumeIdSet m_sensorVolumeQueryResult;
	Cry::SensorSystem::ISensorMap* m_pSensorMap;
	Cry::SensorSystem::ISensorSystem* m_pSensorSystem;
	Matrix34 m_offsetTM;

	// will be maintained in the sim
	CPlugFemaleTerminal* m_pConnectedTo;

	CInteractionManagerComponent* m_pInteraction;

	bool m_hasLock;
	Vec3 m_lockVec;
	float m_tolerance;

	bool m_isBreakable;

	void Reset();
	void Disconnect();
	void Connect(CPlugFemaleTerminal* pDest);
	void Log(bool isConnected);
	CPlugFemaleTerminal* FindTerminalToConnect();

	// TODO: this function returns true when Carry
	// component's ForcePhysicalEntity flag is set
	// (bad design!)
	bool IsPhysicalConstraintNeeded();
};