// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 21.02.2018, Ali Mehmet Altundag

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include "Player/PlayerHand.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include "Interaction/InteractionManagerComponent.h"
#include <DefaultComponents/Physics/RigidBodyComponent.h>

class CCarryComponent
	: public CInteractionBaseComponent
{
public:
	CCarryComponent();

	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CCarryComponent>& desc)
	{
		desc.SetGUID("{9CE94301-9623-4F1F-BF0A-B5D436AB7EB0}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Carry");
		desc.SetDescription("To carry an object, you use this component.");
		desc.AddMember(&CCarryComponent::m_forcePhysicalEntity, 'fpen', "ForcePhysicalEntity", "Is Physica lPositioning Forced", "Force Physical Positioning", false);
		desc.AddMember(&CCarryComponent::m_hideOwner, 'hiow', "HideOwner", "Hide Owner", "Hide Owner", true);

		desc.AddMember(&CCarryComponent::m_rotOffset, 'roto', "RotOffset", "Rotation Offset", "", Vec3(0.0f, 0.0f, 0.0f));
		desc.AddMember(&CCarryComponent::m_posOffset, 'poso', "PosOffset", "Position Offset", "", Vec3(0.0f, 0.0f, 0.0f));
		desc.AddMember(&CCarryComponent::m_applyOffset, 'appo', "ApplyOffset", "Apply Offset", 
			"When on, the object will be snapped to the hand according to the offsets.", true);
	}

	// CInteractionBaseComponent
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::CARRY; }
	virtual SInteractionFeedback InteractionTick() override;
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override { return ENTITY_EVENT_BIT(ENTITY_EVENT_RESET) | ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE); }
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	bool IsPhysicalPositioningForced() { return m_forcePhysicalEntity; }
	void LastPhase() { m_feedback.phase = EInteractionPhase::LAST; };
	void Drop();

	bool IsCarrying() const { return m_constraintId != -1; }

private:
	// hide flag
	bool m_hideOwner;

	// this will be used as carry flag
	EntityId m_ownerHandId;
	int m_constraintId;

	bool m_forcePhysicalEntity;
	//Matrix34 m_forceTM;

	// TRIGGER STATE //
	bool m_triggerHigh = false;
	bool m_prevTriggerState = false;
	bool m_triggerPressed = false;
	bool m_triggerReleased = false;
	// ~TRIGGER STATE //

	// OFFSET //
	Vec3 m_rotOffset;
	Vec3 m_posOffset;
	Matrix34 m_offsetMat;
	bool m_applyOffset = true;
	// ~OFFSET //

	void Reset();
	void Pick(const CTrackerState& hand);

	void StateMachineTick(const SInteractionFeedback &feedback);

	void ChangePhysicsType(Cry::DefaultComponents::CRigidBodyComponent::EPhysicalType phyType);

	CInteractionManagerComponent* m_pIntComp;
};