// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 18:04 19.01.2019, Ali Mehmet Altundag

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include <DefaultComponents/Physics/RigidBodyComponent.h>
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"

class CStaticCarryComponent
	: public CInteractionBaseComponent
{
public:
	CStaticCarryComponent();

	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CStaticCarryComponent>& desc)
	{
		desc.SetGUID("{F2129123-0319-4238-9595-5B52E0B461C0}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Static Carry");
		desc.SetDescription("To carry an unphysicalized object, you use this component.");
	}

	// CInteractionBaseComponent
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::CARRY; }
	virtual SInteractionFeedback InteractionTick() override;
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override { return ENTITY_EVENT_BIT(ENTITY_EVENT_RESET) | ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE); }
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

private:
	void Reset();
	void Pick(const CTrackerState& hand);
	void Drop(const CTrackerState& hand);

	void CStaticCarryComponent::StateMachineTick(const SInteractionFeedback & feedback);

	EntityId m_owenerHandId;
	Matrix34 m_offsetTM;
};