// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 3:20 PM 12/25/2018, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "DefaultComponents/Physics/RigidBodyComponent.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include "StdDefs.h"

class CWearComponent
	: public CInteractionBaseComponent
{
public:
	CWearComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CWearComponent>& desc)
	{
		desc.SetGUID("{F3E355E2-9860-4543-B993-DFB058979AF0}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Wear");
		desc.SetDescription("Basic Wear functionality.");
		//EDITOR MEMBERS
		desc.AddMember(&CWearComponent::m_SwapEntityName, 'swap', "SwapName", "Entity Name to Swap:", "Get name of Entity thats gonna be swaped", "");
		//desc.AddMember(&CWearComponent::m_SwapEntityName, 'swap', "SwapName", "Swap Entity Name", "Get name of Entity gonna be swaped", "");
	}

	// IEntityComponent
	virtual void Initialize() override;
	const int geometrySlot = 0;
	// ~IEntitycomponent

	// CInteractionBaseComponent
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::WEAR; }
	virtual SInteractionFeedback InteractionTick() override;
	// ~CInteractionBaseComponent

private:
	Schematyc::CSharedString m_SwapEntityName;
};