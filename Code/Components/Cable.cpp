// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 21.01.2019, Alper �ekerci

#include "Cable.h"
#include "CrySchematyc\CoreAPI.h"
#include "StdDefs.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>

void CCable::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CCable));
		{
		}
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&CCable::Register)

void CCable::Initialize()
{
	m_pCarry = m_pEntity->GetOrCreateComponentClass<CCarryComponent>();
	m_pRope = m_pEntity->GetOrCreateComponentClass<CRope>();
}

uint64 CCable::GetEventMask() const
{
	return ENTITY_EVENT_BIT(ENTITY_EVENT_PREPHYSICSUPDATE) | ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE) | ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_RESET) | ENTITY_EVENT_BIT(ENTITY_EVENT_PHYS_POSTSTEP) | ENTITY_EVENT_BIT(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);
}

bool CCable::SetPos(Vec3 pos)
{
	// Vec3 iniPos = m_pEntity->GetWorldPos();

	Matrix34 tm = m_pEntity->GetWorldTM();
	Vec3 deltaPos = pos - m_pEntity->GetWorldPos();
	tm.AddTranslation(deltaPos);
	m_pEntity->SetWorldTM(tm);

	/*auto pMeshComp = m_pEntity->GetComponent<Cry::DefaultComponents::CStaticMeshComponent>();
	if (pMeshComp)
	{
		auto meshTM = pMeshComp->GetTransformMatrix();		
		meshTM.AddTranslation(Vec3(0, 0.001f, 0));
		// pMeshComp->SetTransformMatrix(meshTM);
		/*meshTM.AddTranslation(Vec3(-1, 0, 0));
		pMeshComp->SetTransformMatrix(meshTM);
		pMeshComp->ResetObject();
	}*/

	// auto rigidbody = m_pEntity->GetComponent<Cry::DefaultComponents::CRigidBodyComponent>();
	// rigidbody->SetTransformMatrix(tm);

	/*rigidbody->m_type = Cry::DefaultComponents::CRigidBodyComponent::EPhysicalType::Rigid;
	rigidbody->GetComponentFlags().Add(EEntityComponentFlags::UserAdded);
	rigidbody->Enable(true);
	rigidbody->SendEvent(SEntityEvent(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED));*/

	/*pe_params_pos ppp;
	ppp.pos = pos;
	m_pEntity->GetPhysicalEntity()->SetParams(&ppp, 1);

	Vec3 newPos = m_pEntity->GetWorldPos();
	float loss = (pos - newPos).GetLengthFast();

	if (loss > 0.0001f)
	{
		return false;
	}*/
	
	return true;
}

void CCable::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		float dt = event.fParam[0];
		
		if (!mInitialPos.IsValid()) // Check if the editor value is read correctly.
		{
			return;
		}

		if (!mStarted)
		{
			mRopeStaticPos = m_pEntity->GetWorldPos();

			/*auto opRes = CGamePlugin::GetStateManager().Query(StateManager::ECommand::SCENARIO_CHECK, ESecenarioQueryActions::OPERATION_TYPE);
			auto operation = opRes[0].sValue;*/
			string training = gEnv->pSystem->GetIConsole()->GetCVar("aug_training_set")->GetString();


			std::string operation = "";
			const auto &opCVar = gEnv->pSystem->GetIConsole()->GetCVar("aug_operation");
			if (opCVar) operation.assign(opCVar->GetString());
			bool opOpen = operation == OPERATION_OPEN;
			if (training == "X5" || training == "HighVol")
				opOpen = false;
			int klemensId = opOpen ? -1 : mKlemensId;

			if (klemensId > 0)
			{
				std::string klemensName = ENTITY_NAME_KLEMENSPREFIX + std::to_string(klemensId);
				if (m_pTargetKlemens = gEnv->pEntitySystem->FindEntityByName(klemensName.c_str()))
				{
					mTargetIniPos = m_pTargetKlemens->GetWorldPos();
				}						
			}
			else
			{
				mTargetIniPos = mRopeStaticPos + mInitialPos;
			}			

			bool success = SetPos(mTargetIniPos);
			if (!success)
			{
				return;
			}

			/*auto pMeshComp = m_pEntity->GetComponent<Cry::DefaultComponents::CStaticMeshComponent>();
			if (pMeshComp)
			{			
				// pMeshComp->ResetObject();
			}*/

			mStarted = true;
		}

		m_pRope->SetPointA(m_pEntity->GetWorldPos());
		m_pRope->SetPointB(mRopeStaticPos);

		if (m_pRope->ExceededMaxLength())
		{
			Vec3 fixLength = m_pEntity->GetWorldPos() - mRopeStaticPos;
			fixLength = fixLength.normalized() * m_pRope->GetMaxLength() * 0.99f;
			fixLength += mRopeStaticPos;

			m_pRope->SetPointA(fixLength);
			m_pCarry->Drop();
			m_pCarry->LastPhase();
			SetPos(fixLength);						
		}
	}
	break;

	case ENTITY_EVENT_PREPHYSICSUPDATE:
	{
		float dt = event.fParam[0];
	}
	break;

	case ENTITY_EVENT_PHYS_POSTSTEP:
	{
	}
	break;

	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:	
	case ENTITY_EVENT_RESET:
	{
		mStarted = false;		
	}
	break;
	}
}