// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 08:26 am 20/02/2019, Tugba Karadeniz

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntity.h>

class CAudioController  : public IEntityComponent
{
public:
	CAudioController() {}
	virtual ~CAudioController() {}

	// IEntityComponent
	virtual void   Initialize() override;
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CAudioController>& desc)
	{
		desc.SetGUID("{5BDFBF9F-1308-40AA-8D9A-73066DEA5827}"_cry_guid);
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("AudioController");
		desc.SetDescription("Use this compnent for manage audio objects");
	}
	// ~IEntityComponent

	void PlayByName(string sSoundName);
	void StopByName(string sSoundName);


private:
	IEntityAudioComponent* m_AudioComponent = nullptr;
	CryAudio::AuxObjectId m_auxObjectId = CryAudio::InvalidAuxObjectId;

};


