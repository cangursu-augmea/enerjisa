// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 3:20 PM 12/25/2018, Utku Guler

#include "StdAfx.h"
#include "Wear.h"
#include "Interaction/InteractionManagerComponent.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>


void CWearComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CWearComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CWearComponent::Register)

CWearComponent::CWearComponent()
	: m_SwapEntityName("")
{}

void CWearComponent::Initialize()
{
	CInteractionManagerComponent* pInteraction = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	CRY_ASSERT_MESSAGE(pInteraction != nullptr, "Wear component needs to have interaction component attached to the entity.");
}

SInteractionFeedback CWearComponent::InteractionTick()
{
	auto state = GetTrackerState();

	if (state.GetButtonState(eKI_Motion_OpenVR_TriggerBtn) == 1.f )
	{	
		if (IEntity* pHandEntity = gEnv->pEntitySystem->FindEntityByName(m_SwapEntityName.c_str()))
		{	
			pHandEntity->RemoveComponent<Cry::DefaultComponents::CStaticMeshComponent>();

			Cry::DefaultComponents::CStaticMeshComponent* pMeshComponent = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CStaticMeshComponent>();
			Cry::DefaultComponents::CStaticMeshComponent* pHandMeshComponent = pHandEntity->GetOrCreateComponent<Cry::DefaultComponents::CStaticMeshComponent>();
			
			if (pHandMeshComponent && pMeshComponent )
			{
				pHandMeshComponent->GetComponentFlags().Add(EEntityComponentFlags::UserAdded);
				pHandMeshComponent->SetTransformMatrix(pMeshComponent->GetTransform());
				pHandMeshComponent->SetFilePath(pMeshComponent->GetFilePath());
				pHandMeshComponent->LoadFromDisk();
				pHandMeshComponent->ResetObject();
			}
		}
		/////EVENT SEND/////
		StateManager::Event event;
		event.objects = { m_SwapEntityName.c_str() };
		event.action = GetInteractionId();
		event.result.bValue = true;
		CGamePlugin::GetStateManager().Insert(event);
		/////EVENT SEND/////
		gEnv->pEntitySystem->RemoveEntity(GetEntityId());
	}

	return SInteractionFeedback();
}