// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 21.02.2018, Ali Mehmet Altundag

#include "StdAfx.h"
#include "Carry.h"
#include "GamePlugin.h"

void CCarryComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CCarryComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CCarryComponent::Register)

CCarryComponent::CCarryComponent()
	: m_constraintId(-1)
	, m_ownerHandId(INVALID_ENTITYID)
	, m_forcePhysicalEntity(false)
	, m_hideOwner(true)
	, m_pIntComp(nullptr)
	//, m_forceTM(IDENTITY)
{
	Reset();
}

void CCarryComponent::Initialize()
{
	m_pIntComp = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	CRY_ASSERT_MESSAGE(m_pIntComp != nullptr, "Carry component needs to have interaction component attached to the entity.");

	Reset();
}

void CCarryComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_RESET:
	{
		Reset();
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		// const CTrackerState &state = GetTrackerState();

		/*if (m_ownerHandId == INVALID_ENTITYID && m_forcePhysicalEntity)
		{
			auto pPhy = m_pEntity->GetPhysicalEntity();
			if (pPhy)
			{
				pe_params_pos ppp;
				ppp.pMtx3x4 = &m_forceTM;
				pPhy->SetParams(&ppp);
			}			
		}*/
	}
	break;
	}
}

void CCarryComponent::Reset()
{
	m_ownerHandId = INVALID_ENTITYID;
	m_constraintId = -1;

	m_triggerHigh = false;
	m_prevTriggerState = false;
	m_triggerPressed = false;
	m_triggerReleased = false;

	m_offsetMat = Matrix34{Vec3(1,1,1), Quat{ m_rotOffset * DEG2RAD(1) }, m_posOffset};

	//// yeah, might be too early...
	//if (!m_pEntity) return;
	//
	//auto pPhy = m_pEntity->GetPhysicalEntity();
	//if (pPhy)
	//{
	//	pe_status_pos psp;
	//	pPhy->GetStatus(&psp);
	//	m_forceTM = (*psp.pMtx3x4);
	//}
}

void CCarryComponent::Pick(const CTrackerState& hand)
{
	if (m_forcePhysicalEntity)
		ChangePhysicsType(Cry::DefaultComponents::CRigidBodyComponent::EPhysicalType::Rigid);

	m_ownerHandId = hand.GetHandId();
	IEntity* pHandEntity = gEnv->pEntitySystem->GetEntity(m_ownerHandId);

	// TODO: for snipping set snap pos/TM here than let the physics engine handle the rest

	if (pHandEntity && m_pEntity->GetPhysicalEntity())
	{
		if (m_applyOffset)
		{
			m_pEntity->SetWorldTM(Matrix34{ m_pEntity->GetWorldScale(), pHandEntity->GetWorldRotation(), pHandEntity->GetWorldPos() } *m_offsetMat);
		}

		pe_action_add_constraint aac;
		aac.pt[0] = pHandEntity->GetWorldPos();
		aac.flags = constraint_no_tears | constraint_ignore_buddy | constraint_no_rotation | world_frames;
		aac.pBuddy = pHandEntity->GetPhysicalEntity();
		aac.maxPullForce = 1000;
		aac.maxBendTorque = 10000;
		//aac.xlimits[0] = 1;
		//aac.yzlimits[1] = 1;
		aac.hardnessLin = aac.hardnessAng = 32;
		m_constraintId = m_pEntity->GetPhysicalEntity()->Action(&aac);
	}
}

void CCarryComponent::Drop()
{
	auto pSource = m_pIntComp->GetInteractionSource();
	if(pSource) pSource->Hide(false);

	m_feedback.feedback = EInteractionFeedback::NONE;

	// nothing to do...
	if (-1 == m_constraintId) return;

	if (m_pEntity->GetPhysicalEntity())
	{
		pe_action_update_constraint auc;
		auc.bRemove = 1;
		auc.idConstraint = m_constraintId;
		m_pEntity->GetPhysicalEntity()->Action(&auc);
		m_pEntity->GetPhysicalEntity()->Action(&pe_action_awake());
	}

	if(m_forcePhysicalEntity)
		ChangePhysicsType(Cry::DefaultComponents::CRigidBodyComponent::EPhysicalType::Static);

	Reset();
}

SInteractionFeedback CCarryComponent::InteractionTick()
{
	const CTrackerState &state = GetTrackerState();
	
	float trigger = state.GetButtonState(eKI_Motion_OpenVR_Trigger);
	m_triggerHigh = trigger >= 0.5f;
	/*m_triggerPressed = m_triggerHigh && !m_prevTriggerState;
	m_triggerReleased = !m_triggerHigh && m_prevTriggerState;
	m_prevTriggerState = m_triggerHigh;*/ // This does not work properly.

	switch (m_feedback.phase)
	{
	// check if we can carry and try to carry...
	case EInteractionPhase::FIRST:
	{
		if (m_triggerHigh && m_pEntity->GetPhysicalEntity() && (m_ownerHandId == INVALID_ENTITYID))
		{
			Pick(state);
			m_feedback.feedback = EInteractionFeedback::SUCCESS;
		}
	}
	break;
	// ongoing process: 1. nothing to do 2. we might be asked to drop
	case EInteractionPhase::ONGOING:
	{
		m_pIntComp->GetInteractionSource()->Hide(true);
		if (!m_triggerHigh)
		{
			Drop();
			//CInteractionManagerComponent* pMan = m_pEntity->GetComponent<CInteractionManagerComponent>();
			//if (pMan) pMan->SetInteractionSource(nullptr);
		}
		else
		{
			m_feedback.feedback = EInteractionFeedback::INTERACTION_LOCK;
		}
	}
	break;
	// somehow we got the signal...
	case EInteractionPhase::LAST:
	{
		m_feedback.feedback = EInteractionFeedback::NONE;
	}
	break;
	}

	// calc next state
	StateMachineTick(m_feedback);

	// return current state
	return m_feedback;
}

void CCarryComponent::StateMachineTick(const SInteractionFeedback & feedback)
{
	switch (m_feedback.phase)
	{
	case EInteractionPhase::FIRST:
	{
		if (EInteractionFeedback::SUCCESS == feedback.feedback)
			m_feedback.phase = EInteractionPhase::ONGOING;
		else
			m_feedback.phase = EInteractionPhase::LAST;
	}
	break;
	case EInteractionPhase::ONGOING:
	{
		if (EInteractionFeedback::NONE == feedback.feedback)
			m_feedback.phase = EInteractionPhase::LAST;
	}
	break;
	case EInteractionPhase::LAST:
	{
		m_feedback.phase = EInteractionPhase::FIRST;
		break;
	}
	}
}

void CCarryComponent::ChangePhysicsType(Cry::DefaultComponents::CRigidBodyComponent::EPhysicalType phyType)
{
	if (auto pRigidBody = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CRigidBodyComponent>())
	{
		pRigidBody->m_type = phyType;
		pRigidBody->GetComponentFlags().Add(EEntityComponentFlags::UserAdded);
		pRigidBody->Enable(true);
		pRigidBody->SendEvent(SEntityEvent(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED));
	}
}