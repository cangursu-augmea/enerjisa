// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 4:08 PM 01/16/2019, Utku Guler

#include "StdAfx.h"
#include "Touch.h"
#include "Interaction/InteractionManagerComponent.h"
#include <sstream>

void CTouchComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CTouchComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CTouchComponent::Register)

CTouchComponent::CTouchComponent()
	: m_OffSet(0.f, 0.f, 0.f)
	, m_ScanEntityName("")
	, m_Lengt(1.f)
	, m_operate(true)
	, m_CoolDown(3.f)
	, m_PosOffSet(0.f,0.f,0.f)
{

}


SInteractionFeedback CTouchComponent::InteractionTick()
{
	return SInteractionFeedback();
}
void CTouchComponent::Initialize()
{
	std::istringstream iss(std::string(m_ScanEntityName.c_str()));
	std::string line;
	Detectable buffer;
	while (std::getline(iss, line, ','))
	{
		buffer.Object = line;
		buffer.Timer = 0;
		m_Detactable.push_back(buffer);
	}


}

void CTouchComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{
		std::istringstream iss(std::string(m_ScanEntityName.c_str()));
		std::string line;
		Detectable buffer;
		while (std::getline(iss, line, ','))
		{
			buffer.Object = line;
			buffer.Timer = 0;
			m_Detactable.push_back(buffer);
		}
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{	
		if (m_operate != true) return;
		//CToggleComponent* ToggleCom = m_pEntity->GetOrCreateComponent<CToggleComponent>();
		//if (ToggleCom->GetCurrentState() == false)return;
		Vec3 target = m_pEntity->GetWorldRotation() * m_OffSet;
		Vec3 m_Dir = target;
		Vec3 Pos = m_pEntity->GetWorldPos();
		Pos = Pos + m_pEntity->GetWorldRotation() * m_PosOffSet;
		
		float DeltaTime = gEnv->pTimer->GetFrameTime();
		for (int i = 0; i<m_Detactable.size(); i++)
		{
			if (m_Detactable[i].Timer > 0)
			{
				m_Detactable[i].Timer -= DeltaTime;
			}			
		}

		gEnv->pAuxGeomRenderer->DrawLine(Pos , Col_Blue, Pos  +((m_Dir).normalize() * m_Lengt), Col_Blue , 2);

		const int maxHitCount = 16;
		int nHit = 0;
		ray_hit hits[maxHitCount];
		if ((nHit = gEnv->pPhysicalWorld->RayWorldIntersection(Pos , ((m_Dir).normalize() * m_Lengt), ent_static | ent_sleeping_rigid | ent_rigid, rwi_stop_at_pierceable | rwi_any_hit, hits, maxHitCount, m_pEntity->GetPhysicalEntity())))
		{
			//CryLogAlways("hit");
			for (int i = 0; i < nHit; ++i)
			{
				IPhysicalEntity* pPhyEnt = hits[i].pCollider;
				if (pPhyEnt)
				{
					IEntity* pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pPhyEnt);
					if (pEnt)
					{
						std::string name(pEnt->GetName());
						for(int i =0 ; i<m_Detactable.size();i++)
						{
							if (m_Detactable[i].Object == name && m_Detactable[i].Timer <= 0)
							{
								CryLogAlways("%s", m_Detactable[i].Object);
								/////EVENT SEND/////
								StateManager::Event event;
								event.objects = { m_pEntity->GetName() , name };
								event.action = GetInteractionId();
								event.result.bValue = true;
								CGamePlugin::GetStateManager().Insert(event);
								/////EVENT SEND/////
								m_Detactable[i].Timer = m_CoolDown;
								CryLogAlways("%s",m_Detactable[i].Object);
							}
						}
					}
				}
			}
		}
	}
	break;
	}
}
