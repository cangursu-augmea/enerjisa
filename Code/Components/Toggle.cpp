// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 9:10 AM 12/25/2018, Utku Guler
// Edited by Ali Mehmet Altundag on 11:54 19.01.2019

#include "StdAfx.h"
#include "Toggle.h"
#include "Interaction/InteractionManagerComponent.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>

void CToggleComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CToggleComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CToggleComponent::Register)

CToggleComponent::CToggleComponent()
	: m_state(true)
	, m_triggerBuffer(false)
{

}

void CToggleComponent::Initialize()
{
	CInteractionManagerComponent* pInteraction = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	CRY_ASSERT_MESSAGE(pInteraction != nullptr, "Toggle component needs to have interaction component attached to the entity.");
}

SInteractionFeedback CToggleComponent::InteractionTick()
{
	auto state = GetTrackerState();

	if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 0)
	{
		m_triggerBuffer = false;
	}

	if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 1.f && m_triggerBuffer == false)
	{
		m_triggerBuffer = true;
		Toggle();
	}
	
	return SInteractionFeedback();
}

void CToggleComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	case ENTITY_EVENT_RESET:
	case ENTITY_EVENT_START_GAME:
	case ENTITY_EVENT_START_LEVEL:
	{
		Reset();
	}
	break;
	}
}

void CToggleComponent::Reset()
{		
	/////EVENT SEND/////
	StateManager::Event event;
	event.objects = { m_pEntity->GetName() };
	event.action = GetInteractionId();
	event.result.bValue = m_state;
	CGamePlugin::GetStateManager().Insert(event);
	/////EVENT SEND/////

	// remove current mesh component if found
	Cry::DefaultComponents::CStaticMeshComponent* pMeshComp;
	pMeshComp = m_pEntity->GetComponent<Cry::DefaultComponents::CStaticMeshComponent>();
	if (pMeshComp)
		m_pEntity->RemoveComponent<Cry::DefaultComponents::CStaticMeshComponent>();

	std::string destModelFile("");

	if (m_state) destModelFile = std::string(m_modelOn.value.c_str());
	else destModelFile = std::string(m_modelOff.value.c_str());

	// nothing to do! this is important since we may want
	// to disappear mesh... (yeah, you did wear the glove!)
	if (!destModelFile.size()) return;

	// add mesh of the current state
	pMeshComp = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CStaticMeshComponent>();
	pMeshComp->GetComponentFlags().Add(EEntityComponentFlags::UserAdded);
	pMeshComp->SetFilePath(destModelFile.c_str());
	
	auto& phyParams = pMeshComp->GetPhysicsParameters();
	phyParams.m_mass = 0.1f; // So that the carry component works smoothly.	

	pMeshComp->SetTransformMatrix(Matrix34( m_meshScale, Quat(m_meshRot * DEG2RAD(1)), m_meshPos )); // TODO: Initialize quat once.
	
	pMeshComp->LoadFromDisk();	
	pMeshComp->ResetObject();	
}