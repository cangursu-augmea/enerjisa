// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 08:23 22.01.2019, Ali Mehmet Altundag
// TODO: implement multiple mesh component support
#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "Interaction/InteractionBaseComponent.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>

class CHighlightComponent
	: public CInteractionBaseComponent
{
public:
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CHighlightComponent>& desc)
	{
		desc.SetGUID("{69BC7C2A-C4CC-4E3D-9924-BC7F4F133FC2}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Highlight");
		desc.SetDescription("To highlight an object over the interaction system, use this component.");

		desc.AddMember(&CHighlightComponent::m_highlightMatFile, 'hlmt', "HighlightMaterial", "Highlight Material", "Highlight Material", "");	
		desc.AddMember(&CHighlightComponent::m_defaultMatFile, 'dmat', "DefaultMaterial", "Default Material", "Default Material", "");
	}

	CHighlightComponent();

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override { 
		return ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE) 
			| ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_INIT); }
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::HIGHLIGHT; }
	// ~CInteractionBaseComponent

	/* sets the material will be used for int-highlight */
	void SetHighlightMaterial(IMaterial* pMat) { m_pHighlightMaterial = pMat; }

private:
	bool m_isInteractionAvailable;

	int m_destSlot;

	Schematyc::MaterialFileName m_highlightMatFile;
	Schematyc::MaterialFileName m_defaultMatFile;

	IMaterial* m_pHighlightMaterial;
	IMaterial* m_pDefaultMaterial;
	Cry::DefaultComponents::CStaticMeshComponent* m_pMeshComponent;

	// Custom Component Interactions //
	CCarryComponent* m_pCarry;
	// ~Custom Component Interactions //

	void SwitchMaterial(IMaterial* pDesiredMaterial);
	void Reset();
};