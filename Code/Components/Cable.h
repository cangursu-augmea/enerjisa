// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 21.01.2019, Alper �ekerci

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include "Carry.h"
#include "Rope.h"

class CCable final : public IEntityComponent
{
private:
	CCarryComponent* m_pCarry;
	CRope* m_pRope;
	IEntity* m_pTargetKlemens = nullptr;
	int mKlemensId = 0; // Klemens id starts from 1.

	Vec3 mRopeStaticPos;
	Vec3 mInitialPos; // Initial pos of the cable.
	Vec3 mTargetIniPos;

	bool mStarted = false;
	
	bool SetPos(Vec3 pos);

public:
	CCable() = default;
	~CCable() = default;

	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(const SEntityEvent& event) override;
	
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CCable>& desc)
	{
		desc.SetGUID("{ED8ECBC9-4BB8-4D0B-B538-6006413FBC93}"_cry_guid);
		desc.SetEditorCategory("AUGMEA");
		desc.SetLabel("Cable Component");

		desc.AddMember(&CCable::mInitialPos, 'ips', "InitialPos", "Initial Pos", "Initial position of the cable.", Vec3(0, 0, 0));
		desc.AddMember(&CCable::mKlemensId, 'klm', "Klemens", "Klemens ID", "To which klemens this cable will be connected initially.", 0);
	}

	void SetKlemensId(int value)
	{
		mKlemensId = value;
	}

	int GetKlemensId()
	{
		return mKlemensId;
	}
};