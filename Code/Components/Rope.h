// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 31.10.2018, Alper �ekerci

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include "TubeSurface.h"
#include <CryMath/ISplines.h>
#include <Cry3DEngine/IIndexedMesh.h>

#define ROPE_DEBUG_COL Col_Blue
#define ROPE_DEBUG_RAD 0.01f

struct RopePoint
{
	Vec3 pos;

	RopePoint() : pos(ZERO) {}
	RopePoint(Vec3 pos) : pos(pos) {}

	float GetLength2(const RopePoint& other)
	{
		return pow(pos.x - other.pos.x, 2) + pow(pos.y - other.pos.y, 2) + pow(pos.z - other.pos.z, 2);
	}

	void DrawDebug()
	{
		gEnv->pAuxGeomRenderer->DrawSphere(pos, ROPE_DEBUG_RAD, ROPE_DEBUG_COL);
	}

	// x: 0, y: 1, z: 2 (default)
	float& operator [] (int index)
	{
		switch (index)
		{
		case 0: return pos.x;
		case 1: return pos.y;
		default: return pos.z;
		}
	}

	operator Vec3& ()
	{
		return pos;
	}

	RopePoint operator - (const RopePoint& other)
	{
		return pos - other.pos;
	}
};

class CRope final : public IEntityComponent
{
private:
	float mRadius = 0.1f;
	int mPointCount = 3;
	float mMaxLength = 1;

	float mSegmentLength;
	float mSegmentL2;

	std::vector<RopePoint> mPoints;
	Vec3 mPointA = Vec3(0, 0, 0);
	Vec3 mPointB = Vec3(1, 0, 0);
	Vec3 mPoleTarget = Vec3(1, 0, 0);

	bool mExceededMaxLength = false;	
	Vec3 mLastDirtyPos = Vec3(9999,9999,9999);

	void CreatePoints();
	void DrawDebugPoints();

	// Render //
	_smart_ptr<IRenderMesh> m_pRenderMesh;
	IStatObj* m_statObj;
	IIndexedMesh* m_indexedMesh;

	int m_renderSlot = -1;
	int nNumSegments = 5;
	float fTextureTileU = 1;
	float fTextureTileV = 1;
	float fThickness = 0.005f;
	int nNumSides = 6;
	
	TubeSurface* m_tubeSurface;
	std::vector<IEntity*> m_entity;
	spline::CatmullRomSpline<Vec3> m_spline;

	IMaterial* m_material = nullptr;
	Schematyc::MaterialFileName m_materialPath;
	Schematyc::MaterialFileName m_colorMaterialPath;

	void CreateRenderMesh();
	void UpdateRenderMesh();

	// Solver //
	float mLearnRate = 0.2f;
	int mIterationCount = 100;
	bool mBezier = true;
	bool mDrawDebug = true;

	void SolveMidPoints();
	void AlignMidPoints();
	Vec3 BezierCurve(Vec3 p1, Vec3 p2, Vec3 p3, float t);
	void MakeBezierCurve();
	float CalcSSE();

public:
	CRope() = default;
	~CRope();

	void Reset();
	void SetPointA(Vec3 point) { mPoints[0].pos = point; } // The tip of the rope.

	void SetPointB(Vec3 point) { mPoints[mPoints.size() - 1].pos = point; } // The static point of the rope.
	Vec3 GetPointA() { return mPoints[0].pos; }

	bool ExceededMaxLength() { return mExceededMaxLength; }
	float inline GetMaxLength() { return mMaxLength; }

	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(const SEntityEvent& event) override;

	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CRope>& desc)
	{
		desc.SetGUID("{BC55F876-BAE4-4A12-A6F2-559B14DE8571}"_cry_guid);
		desc.SetEditorCategory("AUGMEA");
		desc.SetLabel("Rope Component");

		desc.AddMember(&CRope::mRadius, 'rds', "Radius", "Radius", 0, 0.01f);
		desc.AddMember(&CRope::mPointCount, 'pct', "PointCount", "Point Count", 0, 2);
		desc.AddMember(&CRope::mMaxLength, 'mxl', "MaxLength", "Max Length", 0, 1.0f);
		desc.AddMember(&CRope::mPoleTarget, 'pol', "PoleTarget", "Pole Target", 0, Vec3(1, 0, 0));
		desc.AddMember(&CRope::mBezier, 'bzr', "Bezier", "Use Bezier Curve", 0, true);
		desc.AddMember(&CRope::mDrawDebug, 'dbg', "DrawDebug", "Draw Debug", 0, true);

		// Render //
		desc.AddMember(&CRope::m_materialPath, 'mat', "Material", "Material", "Path to a material", "");
		desc.AddMember(&CRope::m_colorMaterialPath, 'cmat', "ColorMaterial", "ColorMaterial", "Path to a Color material", "");
		desc.AddMember(&CRope::nNumSegments, 'nsg', "seg_num", "Num Segment", "Num Segment", 5);
		desc.AddMember(&CRope::fTextureTileU, 'tu', "tile_u", "Tile U", "Tile U", 1.0f);
		desc.AddMember(&CRope::fTextureTileV, 'tv', "tile_v", "Tile V", "Tile V", 1.0f);
		desc.AddMember(&CRope::fThickness, 'thc', "thickness", "Thickness", "Thickness", 0.1f);
		desc.AddMember(&CRope::nNumSides, 'nss', "seg_side", "Num Side", "Num Side", 6);		
	}
};