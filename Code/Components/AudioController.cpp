// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 08:26 am 20/02/2019, Tugba Karadeniz

#include "StdAfx.h"
#include "AudioController.h"


void CAudioController::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CAudioController));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CAudioController::Register)


void CAudioController::Initialize()
{
	m_AudioComponent = m_pEntity->CreateComponent<IEntityAudioComponent>();
	CRY_ASSERT(m_auxObjectId == CryAudio::InvalidAuxObjectId);
	m_auxObjectId = CryAudio::DefaultAuxObjectId;
}

void CAudioController::PlayByName(string sSoundName)
{
	CryAudio::ControlId audioControlId = CryAudio::StringToId(sSoundName);
	if (m_AudioComponent != nullptr && audioControlId != CryAudio::InvalidControlId)
	{
		CryAudio::SRequestUserData const userData(CryAudio::ERequestFlags::DoneCallbackOnExternalThread | CryAudio::ERequestFlags::DoneCallbackOnExternalThread, this);
		m_AudioComponent->ExecuteTrigger(audioControlId, m_auxObjectId, userData);
	}
}

void CAudioController::StopByName(string sSoundName)
{
	CryAudio::ControlId audioControlId = CryAudio::StringToId(sSoundName);
	if (m_AudioComponent != nullptr && audioControlId != CryAudio::InvalidControlId)
	{
		CryAudio::SRequestUserData const userData(CryAudio::ERequestFlags::DoneCallbackOnExternalThread | CryAudio::ERequestFlags::DoneCallbackOnExternalThread, this);
		m_AudioComponent->StopTrigger(audioControlId, m_auxObjectId, userData);
	}
}
