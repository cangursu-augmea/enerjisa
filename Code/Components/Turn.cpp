// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 9:25 AM 01/07/2019, Utku Guler
// Edited by Ali Mehmet Altundag on 11:54 19.01.2019

#include "StdAfx.h"
#include "Turn.h"
#include "Interaction/InteractionManagerComponent.h"

void CTurnComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CTurnComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CTurnComponent::Register)

CTurnComponent::CTurnComponent()
	: m_turnAxis(FORWARD_DIRECTION)
	, m_turnAxisOffset(ZERO)
	, m_turnAmount(90.f)
	, m_sumOfTurn(0.f)
	, m_turnSpeed(10.f)
	, m_directionOfRotation(0.f)
	, m_triggerBuffer(false)
	, m_isTurnActionStarted(false)
	, m_turnMeshComponent(false)
{

}

void CTurnComponent::Initialize()
{
	CInteractionManagerComponent* pInteraction = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	CRY_ASSERT_MESSAGE(pInteraction != nullptr, "Turn component needs to have interaction component attached to the entity.");

	Reset();
	Log(m_directionOfRotation == -1.f);
}

SInteractionFeedback CTurnComponent::InteractionTick()
{
	auto state = GetTrackerState();

	// we already started rotating... dont do something!
	if (m_isTurnActionStarted) return m_feedback;

	if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 0)
	{
		m_triggerBuffer = false;
	}

	if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 1.f && m_triggerBuffer == false)
	{
		Log(m_directionOfRotation != -1.f);
		m_triggerBuffer = true;
		GetMatrix(m_calculatedTM);
		m_isTurnActionStarted = true;
	}
	return m_feedback;
}

void CTurnComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{Reset(); } break;
	case ENTITY_EVENT_UPDATE:
	{
#ifndef _RELEASE
		DebugDraw();
#endif

		// nothing to do...
		if (!m_isTurnActionStarted) return;

		// calc turn in rad for each frame
		float rotVal = gEnv->pTimer->GetFrameTime() * m_directionOfRotation * m_turnSpeed;
		m_sumOfTurn = m_sumOfTurn + rotVal;

		// if we reach the max or min
		if (m_sumOfTurn > m_turnAmount)
		{
			m_sumOfTurn = m_turnAmount;
			Reset();
		}
		else if (m_sumOfTurn < -1.f*m_turnAmount)
		{
			m_sumOfTurn = -m_turnAmount;
			Reset();
		}

		// TODO: performance improvements needed
		Matrix34 offsetTM = Matrix34(IDENTITY);
		offsetTM.SetTranslation(m_turnAxisOffset);
		Matrix33 rotTM = Matrix33::CreateRotationAA(DEG2RAD(rotVal), m_turnAxis);
		Matrix34 TM = offsetTM * rotTM;
		offsetTM.SetTranslation(-m_turnAxisOffset);
		TM =  TM * offsetTM;

		m_calculatedTM = m_calculatedTM * TM;
		SetMatrix(m_calculatedTM);
	}
	break;
	}
}

bool CTurnComponent::GetTurnActionStarted()
{
	return m_isTurnActionStarted;
}

void CTurnComponent::DebugDraw()
{
	if (!gEnv->IsEditor()) return;

	// DO NOT MISINFORM THE USER AS CRYTEK DOES!

	if (!m_turnMeshComponent)
	{
		Matrix34 TM;
		GetMatrix(TM);
		Vec3 offset = Matrix33(TM) * m_turnAxisOffset;
		IRenderAuxGeom::GetAux()->DrawCylinder(m_pEntity->GetWorldPos() + offset, m_turnAxis, 0.01f, 0.04f, Col_Blue, false);
	}
}

void CTurnComponent::Reset()
{
	if (m_sumOfTurn == m_turnAmount) m_directionOfRotation = -1.f;
	else m_directionOfRotation = 1.f;
	m_isTurnActionStarted = false;
	m_sumOfTurn = 0.f;
}

void CTurnComponent::GetMatrix(Matrix34& TM)
{
	// TODO: Buggy mesh component behavior
	Cry::DefaultComponents::CStaticMeshComponent* pMeshComp;
	pMeshComp = m_pEntity->GetComponent<Cry::DefaultComponents::CStaticMeshComponent>();
	if (m_turnMeshComponent && pMeshComp)
	{
		TM = pMeshComp->GetTransformMatrix();
	}
	else
	{
		TM = m_pEntity->GetLocalTM();
	}
}

void CTurnComponent::SetMatrix(Matrix34& TM)
{
	// TODO: Buggy mesh component behavior
	Cry::DefaultComponents::CStaticMeshComponent* pMeshComp;
	pMeshComp = m_pEntity->GetComponent<Cry::DefaultComponents::CStaticMeshComponent>();
	if (m_turnMeshComponent && pMeshComp)
	{
		pMeshComp->SetTransformMatrix(TM);
	}
	else
	{
		m_pEntity->SetLocalTM(TM);
	}
}

void CTurnComponent::Log(bool state)
{
	StateManager::Event event;
	event.action = GetInteractionId();
	event.objects = { m_pEntity->GetName() };
	event.result.bValue = state;
	CGamePlugin::GetStateManager().Insert(event);
}