// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 08:23 22.01.2019, Ali Mehmet Altundag

#include "StdAfx.h"
#include "Highlight.h"

void CHighlightComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CHighlightComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CHighlightComponent::Register)

CHighlightComponent::CHighlightComponent()
	: m_isInteractionAvailable(false)
	, m_pMeshComponent(nullptr)
	, m_pDefaultMaterial(nullptr)
	, m_destSlot(-1) // TODO: confirm that -1 is an invalid slot id. just in case...
{}

void CHighlightComponent::Initialize()
{
	// TODO: this is the shit we are tried to do... in each
	// interaction component we say this... move this to 
	// the base class.
	CInteractionManagerComponent* pInteraction = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	CRY_ASSERT_MESSAGE(pInteraction != nullptr, "Highlight component needs to have interaction component attached to the entity.");

	Reset();
}

void CHighlightComponent::ProcessEvent(const SEntityEvent& event)
{
	// nothing to do...
	if (!m_pMeshComponent) return;

	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{
		Reset();
	}
	break;
	case ENTITY_EVENT_INIT:
	{
		// try to get the default material here!
		m_destSlot = m_pMeshComponent->GetEntitySlotId();
		// TODO: Delete --- m_pDefaultMaterial = m_pEntity->GetSlotMaterial(m_destSlot);
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (m_isInteractionAvailable)
		{
			// Highlight, set the highlight mat here
			SwitchMaterial(m_pHighlightMaterial);
		}
		else
		{
			SwitchMaterial(m_pDefaultMaterial);
		}

		m_isInteractionAvailable = false;
	}
	break;
	}
}

SInteractionFeedback CHighlightComponent::InteractionTick()
{
	m_isInteractionAvailable = false;

	if (m_pCarry == nullptr || m_pCarry->IsCarrying())
	{
		return m_feedback;
	}	

	m_isInteractionAvailable = true;

	return m_feedback;
}

void CHighlightComponent::SwitchMaterial(IMaterial* pDesiredMaterial)
{
	// nothing to do...
	if (!pDesiredMaterial) return;

	// check if we want something different
	const IMaterial* pCurrentMat = m_pEntity->GetSlotMaterial(m_destSlot);
	if (pCurrentMat == pDesiredMaterial) return;

	m_pEntity->SetSlotMaterial(m_destSlot, pDesiredMaterial);
}

void CHighlightComponent::Reset()
{
	m_pMeshComponent = m_pEntity->GetComponent<Cry::DefaultComponents::CStaticMeshComponent>();
	CRY_ASSERT_MESSAGE(m_pMeshComponent != nullptr, "To highlight and entity, it has to have mesh component");

	m_pCarry = m_pEntity->GetComponent<CCarryComponent>();
	
	// load highlight material
	if (!m_highlightMatFile.value.IsEmpty())
		m_pHighlightMaterial = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(m_highlightMatFile.value.c_str(), false, true);
	if (!m_defaultMatFile.value.IsEmpty())
		m_pDefaultMaterial = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(m_defaultMatFile.value.c_str(), false, true);
}