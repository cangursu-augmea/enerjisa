#include "StdAfx.h"
#include "GamePlugin.h"

#include <IGameObjectSystem.h>
#include <IGameObject.h>

#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/EnvPackage.h>
#include <CrySchematyc/Utils/SharedString.h>

// Included only once per DLL module.
#include <CryCore/Platform/platform_impl.inl>

#include "StdDefs.h"
#include "StateManager.h"
#include "SessionManager.h"

VRBasicUI CGamePlugin::s_basicUI = VRBasicUI();
TextManager CGamePlugin::s_textManager = TextManager();
StateManager::CStateManager CGamePlugin::s_stateManager = StateManager::CStateManager();
CSessionManager CGamePlugin::s_sessionManager = CSessionManager{};

int CGamePlugin::s_isInteractionDebugEnabled = false;
Vec2 CGamePlugin::s_uiScale = Vec2(1.f);
Vec2 CGamePlugin::s_uiPos = Vec2(0.5f, 0.5f);

CGamePlugin::~CGamePlugin()
{
	// Remove any registered listeners before 'this' becomes invalid
	if (gEnv->pGameFramework != nullptr)
	{
		gEnv->pGameFramework->RemoveNetworkedClientListener(*this);
	}

	gEnv->pSystem->GetISystemEventDispatcher()->RemoveListener(this);

	if (gEnv->pSchematyc)
	{
		gEnv->pSchematyc->GetEnvRegistry().DeregisterPackage(CGamePlugin::GetCID());
	}
}

bool CGamePlugin::Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams)
{
	// Register for engine system events, in our case we need ESYSTEM_EVENT_GAME_POST_INIT to load the map
	gEnv->pSystem->GetISystemEventDispatcher()->RegisterListener(this, "CGamePlugin");

	REGISTER_CVAR2("aug_interaction_debug", &CGamePlugin::s_isInteractionDebugEnabled, 0, VF_NULL, "augmea interaction debug draw");
	REGISTER_FLOAT("aug_nearPlane", 0.01f, VF_NULL, "augmea default near plane distance");
	REGISTER_CVAR2_CB("aug_ui_scale_x", &CGamePlugin::s_uiScale.x, 0.25f, VF_NULL, "Augmea ui scale X val", CGamePlugin::OnUIScaleChange);
	REGISTER_CVAR2_CB("aug_ui_scale_y", &CGamePlugin::s_uiScale.y, 0.25f, VF_NULL, "Augmea ui scale Y val", CGamePlugin::OnUIScaleChange);
	REGISTER_CVAR2_CB("aug_ui_pos_x", &CGamePlugin::s_uiPos.x, 0.75f, VF_NULL, "Augmea ui pos X val", CGamePlugin::OnUIScaleChange);
	REGISTER_CVAR2_CB("aug_ui_pos_y", &CGamePlugin::s_uiPos.y, 0.25f, VF_NULL, "Augmea ui pos Y val", CGamePlugin::OnUIScaleChange);

	//REGISTER_STRING_CB(AUG_SCENARIO_CVAR_NAME.c_str(), OPERATION_REPLACEMENT.c_str(), VF_NULL, "Aug scenario (operation)", CGamePlugin::OnScenarioChange);
	/*REGISTER_STRING_CB("aug_training_set"		, "X5"    , VF_NULL, "Aug Training Set"   , CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_operation"			, "KONTROL" , VF_NULL, "Aug Operation Type" , CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_cable_color"		, "MULTICOLOR", VF_NULL, "Aug Cable Color"    , CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_cable_condition"	, "NORMAL", VF_NULL, "Aug Cable Condition", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_electricity_pole"	, "USED", VF_NULL, "Aug Electricity Pole Is Used", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_cable_order"		, "STANDARD", VF_NULL, "Aug Cable Order", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_monitor_state"		, "CORRECT", VF_NULL, "Aug Cable Order", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_transformer_state"	, "INCORRECT", VF_NULL, "Aug Cable Order", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_transformer_order"  , "INCORRECT", VF_NULL, "Aug Cable Order", CGamePlugin::OnScenarioChange);*/

	REGISTER_STRING_CB("aug_training_set", "HIGHVOL", VF_NULL, "Aug Training Set", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_operation", "DEGISTIRME", VF_NULL, "Aug Operation Type", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_cable_color", "MULTICOLOR", VF_NULL, "Aug Cable Color", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_cable_condition", "NORMAL", VF_NULL, "Aug Cable Condition", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_electricity_pole", "USED", VF_NULL, "Aug Electricity Pole Is Used", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_cable_order", "STANDARD", VF_NULL, "Aug Cable Order", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_monitor_state", "CORRECT", VF_NULL, "Aug Cable Order", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_transformer_state", "INCORRECT", VF_NULL, "Aug Cable Order", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_transformer_order", "INCORRECT", VF_NULL, "Aug Cable Order", CGamePlugin::OnScenarioChange);
	REGISTER_STRING_CB("aug_ttr_value", "VALID", VF_NULL, "Aug TTR Phase Control", CGamePlugin::OnScenarioChange);

	return true;
}

void CallbackTest(uint32_t val)
{
	const std::string text = CGamePlugin::GetTextManager().GetText("menu_usage_yesno");
	CGamePlugin::GetBasicUI().ShowYesNo(text, nullptr);
}

void CGamePlugin::OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam)
{
	switch (event)
	{
		// Called when the game framework has initialized and we are ready for game logic to start
		case ESYSTEM_EVENT_GAME_POST_INIT:
		{
			// Listen for client connection events, in order to create the local player
			gEnv->pGameFramework->AddNetworkedClientListener(*this);

			// Don't need to load the map in editor
			if (!gEnv->IsEditor())
			{
				auto set = gEnv->pConsole->GetCVar("aug_training_set");
				
				if (strcmp(set->GetString(), "TRIFAZE") == 0)
				{
					gEnv->pConsole->ExecuteString("map trifaze", false, true);
				}
				else if (strcmp(set->GetString(), "X5") == 0)
				{
					gEnv->pConsole->ExecuteString("map x5trifaze", false, true);
				}
				else if (strcmp(set->GetString(), "HIGHVOL") == 0)
				{
					gEnv->pConsole->ExecuteString("map highvol", false, true);
				}
				else
				{
					gEnv->pConsole->ExecuteString("map trifaze", false, true);
				}
			}
		}
		break;

		case ESYSTEM_EVENT_REGISTER_SCHEMATYC_ENV:
		{
			// Register all components that belong to this plug-in
			auto staticAutoRegisterLambda = [](Schematyc::IEnvRegistrar& registrar)
			{
				// Call all static callback registered with the CRY_STATIC_AUTO_REGISTER_WITH_PARAM
				Detail::CStaticAutoRegistrar<Schematyc::IEnvRegistrar&>::InvokeStaticCallbacks(registrar);
			};

			if (gEnv->pSchematyc)
			{
				gEnv->pSchematyc->GetEnvRegistry().RegisterPackage(
					stl::make_unique<Schematyc::CEnvPackage>(
						CGamePlugin::GetCID(),
						"EntityComponents",
						"Crytek GmbH",
						"Components",
						staticAutoRegisterLambda
						)
				);
			}

			// load level configs
			m_levelConfigsRoot = gEnv->pSystem->LoadXmlFromFile(AUG_LEVEL_CONF_FILE.c_str());

		}
		break;

		case ESYSTEM_EVENT_LEVEL_LOAD_END:
		{
			const std::string currentLevelName = gEnv->pGameFramework->GetLevelName();
			if (m_levelConfigsRoot)
			{
				for (int i = 0; i < m_levelConfigsRoot->getChildCount(); ++i)
				{
					const auto levelConf = m_levelConfigsRoot->getChild(i);
					if (currentLevelName != levelConf->getAttr("name"))
						continue;

					for (int j = 0; j < levelConf->getChildCount(); ++j)
					{
						const auto command = levelConf->getChild(j);
						if (!command->haveAttr("name") || !command->haveAttr("value"))
							continue;
						const std::string executionStr = std::string(command->getAttr("name")) + std::string(" ") + std::string(command->getAttr("value"));

						gEnv->pConsole->ExecuteString(executionStr.c_str() , false, false);
					}
				}
			}

			GetStateManager().Reset();
			GetSessionManager().Reset();

			OnScenarioChange(gEnv->pSystem->GetIConsole()->GetCVar("aug_training_set")  );
			OnScenarioChange(gEnv->pSystem->GetIConsole()->GetCVar("aug_operation")     );
			OnScenarioChange(gEnv->pSystem->GetIConsole()->GetCVar("aug_cable_color"	));
			OnScenarioChange(gEnv->pSystem->GetIConsole()->GetCVar("aug_cable_condition"));
			
			auto training = GetStateManager().Query(StateManager::ECommand::SCENARIO_CHECK, ESecenarioQueryActions::TRAINING_SET);
			auto optype   = GetStateManager().Query(StateManager::ECommand::SCENARIO_CHECK, ESecenarioQueryActions::OPERATION_TYPE);
			if(training.size() && optype.size())
				CryLog("[DETECTED SCENARIO & OPERATION TYPE]: %s, %s", training[0].sValue, optype[0].sValue);

			GetTextManager().Init();

			if (!GetBasicUI().Init())
			{
				CRY_ASSERT_MESSAGE(false, "Basic UI initialization failed!");
			}

			if (!GetSessionManager().Init())
			{
				CRY_ASSERT_MESSAGE(false, "Session Manager initialization failed!");
			}
		}
		break;

		case ESYSTEM_EVENT_GAME_MODE_SWITCH_END:
			GetSessionManager().Reset();
		break;
	}
}

bool CGamePlugin::OnClientConnectionReceived(int channelId, bool bIsReset)
{
//	// Connection received from a client, create a player entity and component
//	SEntitySpawnParams spawnParams;
//	spawnParams.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();
//	spawnParams.sName = "Player";
//	spawnParams.nFlags |= ENTITY_FLAG_NEVER_NETWORK_STATIC;
//	
//	// Set local player details
//	if (m_players.size() == 0 && !gEnv->IsDedicated())
//	{
//		spawnParams.id = LOCAL_PLAYER_ENTITY_ID;
//		spawnParams.nFlags |= ENTITY_FLAG_LOCAL_PLAYER;
//	}
//
//	// Spawn the player entity
//	if (IEntity* pPlayerEntity = gEnv->pEntitySystem->SpawnEntity(spawnParams))
//	{
//		// Set the local player entity channel id, and bind it to the network so that it can support Multiplayer contexts
//		pPlayerEntity->GetNetEntity()->SetChannelId(channelId);
//		pPlayerEntity->GetNetEntity()->BindToNetwork();
//
//		// Create the player component instance
//		CPlayerComponent* pPlayer = pPlayerEntity->GetOrCreateComponentClass<CPlayerComponent>();
//
//		// Push the component into our map, with the channel id as the key
//		m_players.emplace(std::make_pair(channelId, pPlayerEntity->GetId()));
//	}

	return true;
}

bool CGamePlugin::OnClientReadyForGameplay(int channelId, bool bIsReset)
{
//	// Revive players when the network reports that the client is connected and ready for gameplay
//	auto it = m_players.find(channelId);
//	if (it != m_players.end())
//	{
//		if (IEntity* pPlayerEntity = gEnv->pEntitySystem->GetEntity(it->second))
//		{
//			if (CPlayerComponent* pPlayer = pPlayerEntity->GetComponent<CPlayerComponent>())
//			{
//				pPlayer->Revive();
//			}
//		}
//	}

	return true;
}

void CGamePlugin::OnClientDisconnected(int channelId, EDisconnectionCause cause, const char* description, bool bKeepClient)
{
	// Client disconnected, remove the entity and from map
	auto it = m_players.find(channelId);
	if (it != m_players.end())
	{
		gEnv->pEntitySystem->RemoveEntity(it->second);

		m_players.erase(it);
	}
}

CRYREGISTER_SINGLETON_CLASS(CGamePlugin)