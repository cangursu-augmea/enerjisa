// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 06:16 7.01.2019, Ali Mehmet Altundag

#include "StdAfx.h"
#include "TextManager.h"

void TextManager::Init()
{
	stack_string scanPath = "Assets/Texts/";
	stack_string search = scanPath + stack_string("/*.xml");
	stack_string sspath(scanPath);
	stack_string xmlFile;
	_finddata_t fd;
	intptr_t handle = gEnv->pCryPak->FindFirst(search.c_str(), &fd);

	if (handle <= -1)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "[TextManager]: Not able to find something there: %s", scanPath.c_str());
		return;
	}

	do
	{
		// AUGMEA TODO: add extra checks here
		xmlFile = sspath + "/" + fd.name;
		XmlNodeRef rootNode = gEnv->pSystem->LoadXmlFromFile(xmlFile.c_str());

		// AUGMEA TODO: add your warning here
		if (!rootNode)
		{
			CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_ERROR, "[TextManager]: Cannot parse given file: %s", xmlFile.c_str());
			return;
		}

		// check if it s projectile or weapon
		if (rootNode->isTag("Texts"))
			LoadTexts(rootNode);

	} while (gEnv->pCryPak->FindNext(handle, &fd) >= 0);
}

std::string TextManager::GetText(const std::string& name)
{
	auto it = m_texts.find(name);
	if (m_texts.end() != it) return it->second;

	return std::string("");
}

void TextManager::LoadTexts(const XmlNodeRef& root)
{
	for (int i = 0; i < root->getChildCount(); ++i)
	{
		auto textNode = root->getChild(i);
		if (!textNode->isTag("Text")) continue;

		if (!textNode->haveAttr("name")) continue;

		const std::string name = std::string(textNode->getAttr("name"));
		const std::string text = std::string(textNode->getContent());

		auto it = m_texts.find(name);
		if (m_texts.end() != m_texts.find(name))
			m_texts.erase(it);

		m_texts.insert(std::make_pair(name, text));
	}
}