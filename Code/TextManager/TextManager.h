// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 06:16 7.01.2019, Ali Mehmet Altundag

#pragma once

#include <string>
#include <map>

class TextManager
{
public:
	void Init();

	/* returns text if found */
	std::string GetText(const std::string& name);

private:
	void LoadTexts(const XmlNodeRef& root);

	// text-name <-> text pairs
	std::map<std::string, std::string> m_texts;


	std::string WordWrap(std::string sentence, int width);
};