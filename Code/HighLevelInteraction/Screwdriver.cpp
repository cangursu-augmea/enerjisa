// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:12 21.01.2019, Ali Mehmet Altundag

#include "StdAfx.h"
#include "Screwdriver.h"
#include "Interaction/InteractionManagerComponent.h"

void CScrewdriverComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CScrewdriverComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CScrewdriverComponent::Register)

void CScrewdriverComponent::Initialize()
{
	CInteractionManagerComponent* pInteraction = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	CRY_ASSERT_MESSAGE(pInteraction != nullptr, "CScrewdriverComponent component needs to have interaction component attached to the entity.");
}

CScrewdriverComponent::CScrewdriverComponent()
	: m_screwSearchRadius(0.05f)
{}

SInteractionFeedback CScrewdriverComponent::InteractionTick()
{
	return m_feedback;
}

void CScrewdriverComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_INIT:
	{
		SetRadius(m_screwSearchRadius);
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		auto state = GetTrackerState();
		state.SetTransform(CryTransform::CTransform(m_pEntity->GetWorldTM()));
		UpdateTrackingState(state);
	}
	break;
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{
		SetRadius(m_screwSearchRadius);
	}
	break;
	}
}