// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 4:08 PM 01/16/2019, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include "../Components/AudioController.h"



class CPhotoCameraComponent
	: public CInteractionBaseComponent

{
public:
	CPhotoCameraComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CPhotoCameraComponent>& desc)
	{
		desc.SetGUID("{73D75EDF-6DF6-4161-96ED-EF9CEE1F0327}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("PhotoCamera");
		desc.SetDescription("To Touch an object, you use this component.");
		//EDITOR MEMBERS
		desc.AddMember(&CPhotoCameraComponent::m_offSet, 'mdir', "RaycastVec", "Way to Scan(-1 ... 1)", "Way to Scan", (0.f, 1.f, 0.f));
		desc.AddMember(&CPhotoCameraComponent::m_posOffSet, 'mpof', "OffSetVec", "OffSet", "Way to Move", (0.f, 0.f, 0.f));
		desc.AddMember(&CPhotoCameraComponent::m_lengt, 'mlen', "LengtofRay", "Lengt of Ray", "LengtofRay", 1.f);
		desc.AddMember(&CPhotoCameraComponent::m_refreshRate, 'mrat', "RefreshRate", "Refresh Rate(Sec)", "RefreshRate", 1.f);
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::CAMERA; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

private:
	Vec3 m_offSet;
	Vec3 m_posOffSet;
	float m_lengt;
	bool m_operate;
	bool m_trigger;
	float m_coolDown;
	float m_refreshRate;
	std::string m_photo;

	CAudioController* m_pAudioController;
};