// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 21.01.2019, Alper �ekerci

#include "Klemens.h"
#include "CrySchematyc\CoreAPI.h"
#include "StdDefs.h"

void CKlemens::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CKlemens));
		{
		}
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&CKlemens::Register)

void CKlemens::Initialize()
{
	m_pFemale = m_pEntity->GetOrCreateComponentClass<CPlugFemaleTerminal>();
	m_pToggle = m_pEntity->GetOrCreateComponentClass<CToggleComponent>();
}

uint64 CKlemens::GetEventMask() const
{
	return ENTITY_EVENT_BIT(ENTITY_EVENT_PREPHYSICSUPDATE) | ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE) | ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_RESET) | ENTITY_EVENT_BIT(ENTITY_EVENT_PHYS_POSTSTEP) | ENTITY_EVENT_BIT(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);
}

void CKlemens::UpdateFemaleId()
{
	bool screwed = m_pToggle->GetCurrentState();
	m_pFemale->SetEnabled(!screwed);
}

void CKlemens::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		float dt = event.fParam[0];
		UpdateFemaleId();
	}
	break;
	case ENTITY_EVENT_PREPHYSICSUPDATE:
	{
		float dt = event.fParam[0];
	}
	break;
	case ENTITY_EVENT_PHYS_POSTSTEP:
	{
	}
	break;
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:	
	case ENTITY_EVENT_RESET:
	{
	}
	break;
	}
}