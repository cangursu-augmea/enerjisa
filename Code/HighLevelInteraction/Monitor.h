// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 2:48 PM 3/12/2019, Tugba Karadeniz

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"

class CMonitorComponent
	: public CInteractionBaseComponent
{
private:
	Vec3 m_meshScale{ 1,1,1 };
	Vec3 m_meshPos{ 0,0,0 };
	Vec3 m_meshRot{ 0,0,0 };

public:
	CMonitorComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CMonitorComponent>& desc)
	{
		desc.SetGUID("{4B78FCB7-92B0-4C84-A1EB-A3844F9EF8E7}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Highlevel Interaction");
		desc.SetLabel("Monitor");
		desc.SetDescription("Basic Monitor's Display functionality.");

	}

	// IEntityComponent
	virtual void Initialize() override;
	virtual void ProcessEvent(const SEntityEvent& event) override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return
			  ENTITY_EVENT_BIT(ENTITY_EVENT_LEVEL_LOADED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_RESET)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_START_GAME)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_START_LEVEL)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE);
	}
	// ~IEntitycomponent

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::MONITOR; }
	// ~CInteractionBaseComponent

private:
	void Revive();
	void Reset(const std::string meshPath);
	void SetMonitorMeshNames();

	bool m_triggerBuffer;

	const int geometrySlot = 0;
	IEntity* m_monitorDisplay;
	std::vector<std::string> m_meshNames;
	int m_index = 0;

	float m_lifeTime;
	bool m_IsActive;	
};