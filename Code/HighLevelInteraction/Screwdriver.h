// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:12 21.01.2019, Ali Mehmet Altundag

// TODO: shouldnt have its own sensor sys. queries
// It should have multiple interaction sys. hierarchy

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "Interaction/InteractionBaseComponent.h"

class CScrewdriverComponent
	: public CInteractionBaseComponent
{
public:
	CScrewdriverComponent();

	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CScrewdriverComponent>& desc)
	{
		desc.SetGUID("{A01D93CA-7AB4-41BD-BE02-3033120E548D}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetLabel("Screwdriver");
		desc.SetEditorCategory("Augmea Highlevel Interaction");
		desc.AddMember(&CScrewdriverComponent::m_screwSearchRadius, 'radi', "ScrewSearchRadius", "Screw Search Radius", "Screw Search Radius", 0.05f);
	}

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override { return
		ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_INIT)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED); }
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const { return EHighLevelInteractionIDs::SCREWDRIVER; }
	// ~CInteractionBaseComponent

private:
	float m_screwSearchRadius;
};