// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 10:12 AM 01/24/2019, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"

struct Detectable
{
	std::string Object;
	bool state;
	float Timer;
};

class CPensAmpermetreComponent
	: public CInteractionBaseComponent

{
public:
	CPensAmpermetreComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CPensAmpermetreComponent>& desc)
	{
		desc.SetGUID("{085339EE-2A8F-438B-ABDD-D8665D527D86}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("PensAmpermetre");
		desc.SetDescription("To PensAmpermetre an object, you use this component.");
		//EDITOR MEMBERS
		desc.AddMember(&CPensAmpermetreComponent::m_offSet, 'mdir', "RaycastVec", "Way to Scan(-1 ... 1)", "Way to Scan", (0.f, 1.f, 0.f));
		desc.AddMember(&CPensAmpermetreComponent::m_posOffSet, 'mpof', "OffSetVec", "OffSet", "Way to Move", (0.f, 0.f, 0.f));
		desc.AddMember(&CPensAmpermetreComponent::m_lengt, 'mlen', "LengtofRay", "Lengt of Ray", "LengtofRay", 0.025);
		desc.AddMember(&CPensAmpermetreComponent::m_stepLengt, 'mslg', "StepLengt", "StepLengt", "StepLengt", 0.01f);
		desc.AddMember(&CPensAmpermetreComponent::m_CoolDown, 'codw', "DetectCoolDown", "CoolDown time beetween detects", "DetectCoolDown", 30.f);
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::PENSAMPERMETRE; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_LEVEL_LOADED);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	int GetType() { return m_type; };
	void Reset(const std::string meshPath);

private:
	Vec3  m_offSet;
	Vec3  m_posOffSet;
	float m_lengt;
	bool  m_operate;
	float m_buffer;
	float m_stepLengt;
	std::vector<Detectable> m_OutPutList;
	float m_CoolDown;
	int m_type;
	bool m_triggerBuffer;

	const int geometrySlot = 0;
	IEntity* m_pensDisplay;
	 
	std::string NUMERICAL_GAUGE_0 = "objects/equipments/pensAmp_NumericalGauge_0.cgf"; // for wrong evaluation
	std::string NUMERICAL_GAUGE_1 =	"objects/equipments/pensAmp_NumericalGauge_1.cgf";
	std::string NUMERICAL_GAUGE_15 = "objects/equipments/pensAmp_NumericalGauge_15.cgf";

};