// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:02 aM 11/02/2019, Utku Guler

#include "StdAfx.h"
#include "ControlPen.h"
#include "Interaction/InteractionManagerComponent.h"
#include <DefaultComponents/Lights/PointLightComponent.h>
#include <sstream>
#include <string.h>


void CControlPenComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CControlPenComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CControlPenComponent::Register)

CControlPenComponent::CControlPenComponent()
	: m_OffSet(0.f, 0.f, 0.f)
	, m_Lengt(1.f)
	, m_operate(true)
	, m_PosOffSet(0.f, 0.f, 0.f)
	, m_fuseToggle()
{

}


SInteractionFeedback CControlPenComponent::InteractionTick()
{
	return SInteractionFeedback();
}
void CControlPenComponent::Initialize()
{
	m_audioController = m_pEntity->GetOrCreateComponent<CAudioController>();

	
}


void CControlPenComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	break;
	case ENTITY_EVENT_LEVEL_LOADED:
	{
		if(IEntity* fuse = gEnv->pEntitySystem->FindEntityByName("SIGORTA"))
			m_fuseToggle = fuse->GetOrCreateComponentClass<CToggleComponent>();
		m_eventFlag = true;
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{

		if (m_operate != true) return;

		Vec3 target = m_pEntity->GetWorldRotation() * m_OffSet;
		Vec3 m_Dir = target;
		Vec3 Pos = m_pEntity->GetWorldPos();
		Pos = Pos + m_pEntity->GetWorldRotation() * m_PosOffSet;



		gEnv->pAuxGeomRenderer->DrawLine(Pos, Col_Blue, Pos + ((m_Dir).normalize() * m_Lengt), Col_Blue, 2);

		const int maxHitCount = 16;
		int nHit = 0;
		ray_hit hits[maxHitCount];
		if ((nHit = gEnv->pPhysicalWorld->RayWorldIntersection(Pos, ((m_Dir).normalize() * m_Lengt), ent_static | ent_sleeping_rigid | ent_rigid, rwi_stop_at_pierceable, hits, maxHitCount, m_pEntity->GetPhysicalEntity())))
		{
			//CryLogAlways("hit");
			for (int i = 0; i < nHit; ++i)
			{
				IPhysicalEntity* pPhyEnt = hits[i].pCollider;
				if (pPhyEnt)
				{
					IEntity* pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pPhyEnt);
					if (pEnt)
					{
						std::string name(pEnt->GetName());

						if (CMeasurableComponent* Measurable = pEnt->GetComponent<CMeasurableComponent>())
						{
							//////////////////////////////////////////  ACTION  //////////////////////////////////////////
							if (Measurable->IsEnable() == false)
								return;
							
							int RedType = Measurable->GetType();
							if(RedType == 3 && m_eventFlag)
							{
								/////EVENT SEND/////
								StateManager::Event event;
								event.objects = { m_pEntity->GetName() , name };
								event.action = GetInteractionId();
								event.result.bValue = true;
								CGamePlugin::GetStateManager().Insert(event);
								/////EVENT SEND/////
								m_eventFlag = false;
							}
							else
							{
								if (m_fuseToggle->GetCurrentState() && RedType != 3 && RedType != 0)
								{
									TurnOnLight(true);
								}
								else
								{
									TurnOnLight(false);
								}
							}
							

							//////////////////////////////////////////  ACTION  //////////////////////////////////////////
						}
						else
						{
							TurnOnLight(false);
						}
					}
				}
			}
		}
		else
		{
			TurnOnLight(false);
		}

	}
	break;
	}
}

void CControlPenComponent::TurnOnLight(bool active)
{
	static bool audioFlag = true;

	auto componentLight1 = gEnv->pEntitySystem->FindEntityByName("CPEN_LIGHT-1")->GetComponent<Cry::DefaultComponents::CPointLightComponent>();
	auto componentLight2 = gEnv->pEntitySystem->FindEntityByName("CPEN_LIGHT-2")->GetComponent<Cry::DefaultComponents::CPointLightComponent>();
	auto componentLight3 = gEnv->pEntitySystem->FindEntityByName("CPEN_LIGHT-3")->GetComponent<Cry::DefaultComponents::CPointLightComponent>();
	auto componentLight4 = gEnv->pEntitySystem->FindEntityByName("CPEN_LIGHT-4")->GetComponent<Cry::DefaultComponents::CPointLightComponent>();

	componentLight1->Enable(active);
	componentLight2->Enable(active);
	componentLight3->Enable(active);
	componentLight4->Enable(active);

	if (active) { // play if active and audioFlag are true
		if (audioFlag) {
			audioFlag = false;
			m_audioController->PlayByName("beep");
		}
	}
	else { // stop if active and audioFlag are false
		audioFlag = true;
		m_audioController->StopByName("beep");
	}	

}
