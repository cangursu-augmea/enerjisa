// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 2:37 PM 15/02/2019, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"

class CMeasurableComponent
	: public IEntityComponent

{
public:
	CMeasurableComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CMeasurableComponent>& desc)
	{
		desc.SetGUID("{A730405D-8307-4B28-9470-7EF5D2009D0C}"_cry_guid);
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Measureable");
		desc.SetDescription("Objects that can be measured by Probes");
		//EDITOR MEMBERS
		desc.AddMember(&CMeasurableComponent::m_type, 'type', "TypeofMeasureable", "Type of Measurable", "Type of Measurable", 0 );
		desc.AddMember(&CMeasurableComponent::m_isInput, 'isin', "IsInputCable", "Is Input Cable", "Is Input Cable", true);
		desc.AddMember(&CMeasurableComponent::m_pairID, 'pair', "PairIDofCable", "Pair ID of Cable", "Pair ID of Cable", 0);

	}

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	int GetType() { return m_type; }
	bool GetIsInput() { return m_isInput; }
	int GetPairID() { return m_pairID; }
	bool IsEnable() { return m_enabled; }
	void SetEnabled(bool enable) { m_enabled = enable; }
	void SetType(int type) { m_type = type; }

private:
	bool m_enabled;
	int  m_type;
	bool m_isInput;
	int  m_pairID;
	
};