// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 4:08 PM 01/16/2019, Utku Guler

#include "StdAfx.h"
#include "PhotoCamera.h"
#include "Interaction/InteractionManagerComponent.h"
#include <sstream>
#include <CryRenderer/IRenderAuxGeom.h>

void CPhotoCameraComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CPhotoCameraComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CPhotoCameraComponent::Register)

CPhotoCameraComponent::CPhotoCameraComponent()
	: m_offSet(0.f, 0.f, 0.f)
	, m_lengt(1.f)
	, m_operate(true)
	, m_posOffSet(0.f, 0.f, 0.f)
	, m_trigger(false)
	, m_coolDown(1.f)
	, m_refreshRate(1.f)
	, m_photo("")
{

}


SInteractionFeedback CPhotoCameraComponent::InteractionTick()
{
	auto state = GetTrackerState();

	if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 0)
	{
		m_trigger = false;
	}
	if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 1)
	{
		m_trigger = true;
	}




	return SInteractionFeedback();
}
void CPhotoCameraComponent::Initialize()
{
	m_pAudioController = m_pEntity->GetOrCreateComponent<CAudioController>();
}

void CPhotoCameraComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (m_coolDown > 0)
		{
			float DeltaTime = gEnv->pTimer->GetFrameTime();
			m_coolDown -= DeltaTime;
			if(m_coolDown <= 0)
			{
				/////EVENT SEND/////
				StateManager::Event event;
				event.objects = { m_pEntity->GetName() , m_photo };
				event.action = GetInteractionId();
				event.result.bValue = false;
				CGamePlugin::GetStateManager().Insert(event);
				/////EVENT SEND/////
			}
			return;
		}

		if (m_operate != true) return;
		if (m_trigger != true) return;

		Vec3 target = m_pEntity->GetWorldRotation() * m_offSet;
		Vec3 m_Dir = target;
		Vec3 Pos = m_pEntity->GetWorldPos();
		Pos = Pos + m_pEntity->GetWorldRotation() * m_posOffSet;
		m_Dir.normalize();

		//#ifndef __RELASE
		//		if (gEnv->IsEditor())
		//		{
		//			IRenderAuxGeom::GetAux()->DrawLine(Pos, Col_Blue, Pos + m_Dir * m_lengt, Col_Blue);
		//		}
		//#endif

		IEntity* pEntity = gEnv->pEntitySystem->FindEntityByName("KUTU_GOVDE");
		IPhysicalEntity* pPhysicalEntity = pEntity->GetPhysicalEntity();

		// Camera check to see if camera is pointing at box
		// 1: Ray trace the entity to see if camera is pointing at box
		ray_hit hit;
		const int numHits = gEnv->pPhysicalWorld->RayTraceEntity(pPhysicalEntity, Pos, m_Dir * m_lengt, &hit);
		if (numHits != 0)
		{

			if((pEntity->GetForwardDir().Dot(m_pEntity->GetForwardDir())) < 0 )
			{
				// If there is a hit, confirm that the camera is pointed in the right direction
				m_pAudioController->PlayByName("photoCapture");

				/////EVENT SEND/////
				StateManager::Event event;
				event.objects = { m_pEntity->GetName() , "KUTU_GOVDE" };
				event.action = GetInteractionId();
				event.result.bValue = true;
				CGamePlugin::GetStateManager().Insert(event);
				/////EVENT SEND/////
				CryLogAlways("%s", "KUTU_GOVDE");
				m_coolDown = m_refreshRate;
				m_photo = "KUTU_GOVDE";
			}
		}
	}
	break;
	}
}
