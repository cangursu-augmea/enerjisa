// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:02 aM 11/02/2019, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include "HighLevelInteraction/Measurable.h"


class CBlackProbeComponent
	: public CInteractionBaseComponent

{
public:
	CBlackProbeComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CBlackProbeComponent>& desc)
	{
		desc.SetGUID("{A964B86D-1402-4DED-B462-34A3CC2D8582}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("BlackProbe");
		desc.SetDescription("For BlackProbe object, you use this component.");
		//EDITOR MEMBERS
		desc.AddMember(&CBlackProbeComponent::m_OffSet, 'mdir', "RaycastVec", "Way to Scan(-1 ... 1)", "Way to Scan", (0.f, 1.f, 0.f));
		desc.AddMember(&CBlackProbeComponent::m_PosOffSet, 'mpof', "OffSetVec", "OffSet", "Way to Move", (0.f, 0.f, 0.f));
		desc.AddMember(&CBlackProbeComponent::m_Lengt, 'mlen', "LengtofRay", "Lengt of Ray", "LengtofRay", 1.f);
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::TOUCH; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	bool IsActive() const { return m_active; }
	int getType() { return m_mesType; }
	std::string getName() { return m_mesName; }

private:
	Vec3 m_OffSet;
	Vec3 m_PosOffSet;
	float m_Lengt;
	bool m_operate;
	Quat offsetRotator;
	bool m_active;
	int m_mesType;
	std::string m_mesName;
};