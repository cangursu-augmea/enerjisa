// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:02 aM 11/02/2019, Utku Guler

#include "StdAfx.h"
#include "Staka.h"
#include "Interaction/InteractionManagerComponent.h"
#include <DefaultComponents/Lights/PointLightComponent.h>
#include <sstream>
#include <string.h>


void CstakaComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CstakaComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CstakaComponent::Register)

CstakaComponent::CstakaComponent()
	: m_OffSet(0.f, 0.f, 0.f)
	, m_Lengt(1.f)
	, m_operate(true)
	, m_PosOffSet(0.f, 0.f, 0.f)
	, m_fuseToggle()
{

}


SInteractionFeedback CstakaComponent::InteractionTick()
{
	return SInteractionFeedback();
}
void CstakaComponent::Initialize()
{
	m_audioController = m_pEntity->GetOrCreateComponent<CAudioController>();


}

void CstakaComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	break;
	case ENTITY_EVENT_LEVEL_LOADED:
	{
		//if (IEntity* fuse = gEnv->pEntitySystem->FindEntityByName("SIGORTA"))
			//m_fuseToggle = fuse->GetOrCreateComponentClass<CToggleComponent>();
		glove = gEnv->pEntitySystem->FindEntityByName(AUG_ENTITY_NAME_LEFT_HAND.c_str());

	}
	break;

	case ENTITY_EVENT_UPDATE:
	{

		if (m_operate != true) return;

		Vec3 target = m_pEntity->GetWorldRotation() * m_OffSet;
		Vec3 m_Dir = target;
		Vec3 Pos = m_pEntity->GetWorldPos();
		Pos = Pos + m_pEntity->GetWorldRotation() * m_PosOffSet;
		
		gEnv->pAuxGeomRenderer->DrawLine(Pos, Col_Blue, Pos + ((m_Dir).normalize() * m_Lengt), Col_Blue, 2);

		const int maxHitCount = 16;
		int nHit = 0;
		ray_hit hits[maxHitCount];

		if ((nHit = gEnv->pPhysicalWorld->RayWorldIntersection(Pos, ((m_Dir).normalize() * m_Lengt), ent_all, rwi_stop_at_pierceable, hits, maxHitCount, m_pEntity->GetPhysicalEntity())))
		{
			for (int i = 0; i < nHit; ++i)
			{
				IPhysicalEntity* pPhyEnt = hits[i].pCollider;
				if (pPhyEnt)
				{
					IEntity* pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pPhyEnt);
					if (pEnt)
					{
						std::string name(pEnt->GetName());

						CryLog("entity name : %s", name);

						if (CMeasurableComponent* Measurable = pEnt->GetComponent<CMeasurableComponent>())
						{
							//////////////////////////////////////////  ACTION  //////////////////////////////////////////
							if (Measurable->IsEnable() == false)
								return;

							int RedType = Measurable->GetType();
							static bool flag = true;
							if (RedType == 3 && flag)
							{
								/////EVENT SEND/////
								StateManager::Event event;
								event.objects = { m_pEntity->GetName() , name };
								event.action = GetInteractionId();
								event.result.bValue = true;
								CGamePlugin::GetStateManager().Insert(event);
								/////EVENT SEND/////
								flag = false;
							}

						}

					}
				}
			}

		}
	}
	break;
	}

}


