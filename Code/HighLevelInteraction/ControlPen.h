// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:02 aM 11/02/2019, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include "Components/Toggle.h"
#include "HighLevelInteraction/Measurable.h"
#include "../Components/AudioController.h"


class CControlPenComponent
	: public CInteractionBaseComponent

{
public:
	CControlPenComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CControlPenComponent>& desc)
	{
		desc.SetGUID("{38FF74FC-8260-49A1-8BA6-5EA7E687517F}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("ControlPen");
		desc.SetDescription("For ControlPen object, you use this component.");
		//EDITOR MEMBERS
		desc.AddMember(&CControlPenComponent::m_OffSet, 'mdir', "RaycastVec", "Way to Scan(-1 ... 1)", "Way to Scan", (0.f, 1.f, 0.f));
		desc.AddMember(&CControlPenComponent::m_PosOffSet, 'mpof', "OffSetVec", "OffSet", "Way to Move", (0.f, 0.f, 0.f));
		desc.AddMember(&CControlPenComponent::m_Lengt, 'mlen', "LengtofRay", "Lengt of Ray", "LengtofRay", 1.f);
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::TOUCH; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_LEVEL_LOADED);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	void TurnOnLight(bool active);


private:
	Vec3 m_OffSet;
	Vec3 m_PosOffSet;
	float m_Lengt;
	bool m_operate;
	Quat offsetRotator;
	CToggleComponent* m_fuseToggle;
	bool m_eventFlag = true;
	

	CAudioController* m_audioController;

};