// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 17:34 24.01.2019, Ali Mehmet Altundag

// TODO: shouldnt have its own sensor sys. queries
// It should have multiple interaction sys. hierarchy

#pragma once

#include "Interaction/InteractionBaseComponent.h"
#include "Components/Toggle.h"
#include "Components/PlugSystem/PlugMaleTerminal.h"
#include "HighLevelInteraction.h"
#include "../Components/AudioController.h"

class CScrewComponent
	: public CInteractionBaseComponent
{
public:
	CScrewComponent();

	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CScrewComponent>& desc)
	{
		desc.SetGUID("{9B240343-A289-4EBA-A265-F9B92AAFB5A0}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetLabel("Screw");
		desc.SetEditorCategory("Augmea Highlevel Interaction");
		desc.AddMember(&CScrewComponent::m_DefaultState, 'mdef', "DefaultStateofScrew", "Default State of Screw", "DefaultStateofScrew", true);
	}

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override {
		return 
			ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
		|   ENTITY_EVENT_BIT(ENTITY_EVENT_START_GAME)
		|   ENTITY_EVENT_BIT(ENTITY_EVENT_RESET)
		|	ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
		|	ENTITY_EVENT_BIT(ENTITY_EVENT_LEVEL_LOADED); }
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const { return EHighLevelInteractionIDs::SCREW; }
	// ~CInteractionBaseComponent

private:
	bool m_interactionExists;
	bool m_isAvailableForToggle;
	CToggleComponent* m_pToggle;
	bool m_DefaultState;
	IEntity* m_klemens = nullptr;
	CPlugMaleTerminal* m_klemensMaleTerminal = nullptr;
	CPlugFemaleTerminal* m_sayacFemaleTerminal = nullptr;

	CInteractionManagerComponent* m_pInteractionManager;
	CAudioController* m_pAudioController;
};