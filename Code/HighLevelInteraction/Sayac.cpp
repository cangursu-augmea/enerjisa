// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 28.01.2019, Alper �ekerci

#include "Sayac.h"
#include "CrySchematyc\CoreAPI.h"
#include "StdDefs.h"
#include "StateManager.h"
#include "AugUtils.h"
#include "Components\PlugSystem\PlugFemaleTerminal.h"

void CSayac::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CSayac));
		{
		}
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&CSayac::Register)

void CSayac::Initialize()
{
	m_pCarry = m_pEntity->GetOrCreateComponentClass<CCarryComponent>();

	m_cablecount = 8;
	std::string operationType = gEnv->pConsole->GetCVar("aug_training_set")->GetString();
	if (operationType == AUG_SET_X5 || operationType == AUG_SET_HIGHVOL)
		m_cablecount = 13;
}

void CSayac::DisableKlemensCover()
{
	if (m_pKlemensCoverCarry == nullptr)
		return;

	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	using Objs = StateManager::TStateObjects;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	auto IF_CONNECTED = ESecenarioQueryActions::IF_CONNECTED;
	auto& state = CGamePlugin::GetStateManager();

	bool sealOn = state.Query(Cmd::IS_DONE, Act::TOGGLE, Objs{ ENTITY_NAME_KLEMENS_COVER })[0].nValue == 1;		
	bool panelOpened = state.Query(Cmd::IS_DONE, Act::TURN, Objs{ ENTITY_NAME_PANEL })[0].nValue == 1;	
	bool panoOpened = state.Query(Cmd::IS_DONE, Act::TOGGLE, Objs{ ENTITY_NAME_PANO })[0].nValue == 0;
	panoOpened |= strcmp(state.Query(Cmd::SCENARIO_CHECK, TRAINING_SET)[0].sValue, AUG_SET_TRIFAZE.c_str()) == 0; // Because there is no 'pano' in TRIFAZE.

	m_pKlemensCoverCarry->SetEnabled(!sealOn && panelOpened && panoOpened);
	
	if (m_pKlemensCoverToggle == nullptr)
		return;

	m_pKlemensCoverToggle->SetEnabled(panelOpened && panoOpened);
}

void CSayac::DisablePano()
{
	if (m_pPanoCarry == nullptr)
		return;

	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	using Objs = StateManager::TStateObjects;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	auto IF_CONNECTED = ESecenarioQueryActions::IF_CONNECTED;
	auto& state = CGamePlugin::GetStateManager();

	bool panoOpened = state.Query(Cmd::IS_DONE, Act::TOGGLE, Objs{ ENTITY_NAME_PANO })[0].nValue == 1;
	bool panelOpened = state.Query(Cmd::IS_DONE, Act::TURN, Objs{ ENTITY_NAME_PANEL })[0].nValue == 1;
	m_pPanoCarry->SetEnabled(!panoOpened && panelOpened);
	m_pPanoToggle->SetEnabled(panelOpened);
}

void CSayac::DisableCables()
{
	if (cables.size() < 8)
	{
		return;
	}

	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	using Objs = StateManager::TStateObjects;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	auto IF_CONNECTED = ESecenarioQueryActions::IF_CONNECTED;
	auto& state = CGamePlugin::GetStateManager();

	if (state.Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, Objs{ m_pEntity->GetName(), ENTITY_NAME_SAYAC_FEMALE }).size() == 0)
	{
		return;
	}

	if (!m_screwsInitialized)
	{
		for (auto klemens : klemensList)
		{
			klemens->SetEnabled(true);
		}

		m_screwsInitialized = true;
	}

	bool coverOn = state.Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, Objs{ ENTITY_NAME_KLEMENS_COVER, m_pEntity->GetName() }).size() > 0;



	for (int i = 1; i <= m_cablecount; ++i)
	{
		auto plugRes = state.Query(Cmd::PLUG_SYSTEM_CHECK, IF_CONNECTED, Objs{ GetCableName(i) });
		if (plugRes.size() == 0)
			continue;

		if (coverOn)
		{
			cables[i - 1]->SetEnabled(!coverOn);
			continue;
		}		

		auto screwRes = state.Query(Cmd::IS_DONE, Act::TOGGLE, Objs{ std::string{plugRes[0].sValue} });
		if (screwRes.size() == 0) return;

		bool screwed = screwRes[0].nValue == 1;
		cables[i - 1]->SetEnabled(!screwed);
	}
}

uint64 CSayac::GetEventMask() const
{
	return ENTITY_EVENT_BIT(ENTITY_EVENT_PREPHYSICSUPDATE)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_RESET)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_PHYS_POSTSTEP)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_START_GAME)
		| ENTITY_EVENT_BIT(ENTITY_EVENT_START_LEVEL);
}

std::string CSayac::GetCableName(int id)
{
	return ENTITY_NAME_CABLEPREFIX + std::to_string(id);
}

std::string CSayac::GetKlemensName(int id)
{
	return ENTITY_NAME_KLEMENSPREFIX + std::to_string(id);
}
std::string CSayac::GetTrafoUstName(int id)
{
	return ENTITY_NAME_TRAFOUSTPREFIX + std::to_string(id);
}

std::string CSayac::GetTrafoAltName(int id)
{
	return ENTITY_NAME_TRAFOALTPREFIX + std::to_string(id);
}

void CSayac::DisableCollisions()
{
	// TODO: Unefficient due to string operations, maybe store entities directly.
	AugUtils::IgnoreCollisionBetween(m_pEntity->GetName(), ENTITY_NAME_KLEMENS_COVER.c_str());
	AugUtils::IgnoreCollisionBetween(m_pEntity->GetName(), AUG_ENTITY_NAME_LEFT_HAND.c_str());
	AugUtils::IgnoreCollisionBetween(m_pEntity->GetName(), AUG_ENTITY_NAME_RIGHT_HAND.c_str());
	AugUtils::IgnoreCollisionBetween(m_pEntity->GetName(), AUG_ENTITY_NAME_PLAYER_HEAD.c_str());

	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_KLEMENS_COVER.c_str(), AUG_ENTITY_NAME_LEFT_HAND.c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_KLEMENS_COVER.c_str(), AUG_ENTITY_NAME_RIGHT_HAND.c_str());

	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_DRILL.c_str(), m_pEntity->GetName());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_MULTIMETER.c_str(), m_pEntity->GetName());

	AugUtils::IgnoreCollisionBetween("BlackProbe_M", m_pEntity->GetName());
	AugUtils::IgnoreCollisionBetween("RedProbe_M", m_pEntity->GetName());
	AugUtils::IgnoreCollisionBetween("RedProbe_M", "BlackProbe_M");


	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_DRILL.c_str(), ENTITY_NAME_PANEL_BODY_STATIC.c_str());
	
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CAMERA.c_str(), AUG_ENTITY_NAME_LEFT_HAND.c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CAMERA.c_str(), AUG_ENTITY_NAME_RIGHT_HAND.c_str());

	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_PANEL.c_str(), AUG_ENTITY_NAME_LEFT_HAND.c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_PANEL.c_str(), AUG_ENTITY_NAME_RIGHT_HAND.c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_PANEL.c_str(), AUG_ENTITY_NAME_PLAYER_HEAD.c_str());

	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_PENSAMPERMETRE.c_str(), GetTrafoUstName(1).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_PENSAMPERMETRE.c_str(), GetTrafoUstName(2).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_PENSAMPERMETRE.c_str(), GetTrafoUstName(3).c_str());

	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_PENSAMPERMETRE.c_str(), GetTrafoAltName(1).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_PENSAMPERMETRE.c_str(), GetTrafoAltName(2).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_PENSAMPERMETRE.c_str(), GetTrafoAltName(3).c_str());

	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_RED_1.c_str(), GetTrafoUstName(1).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_RED_1.c_str(), GetTrafoUstName(2).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_RED_1.c_str(), GetTrafoUstName(3).c_str());

	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_RED_2.c_str(), GetTrafoUstName(1).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_RED_2.c_str(), GetTrafoUstName(2).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_RED_2.c_str(), GetTrafoUstName(3).c_str());

	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_BLACK_1.c_str(), GetTrafoAltName(1).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_BLACK_1.c_str(), GetTrafoAltName(2).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_BLACK_1.c_str(), GetTrafoAltName(3).c_str());

	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_BLACK_2.c_str(), GetTrafoAltName(1).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_BLACK_2.c_str(), GetTrafoAltName(2).c_str());
	AugUtils::IgnoreCollisionBetween(ENTITY_NAME_CROCODILE_BLACK_2.c_str(), GetTrafoAltName(3).c_str());


	// TODO: The below code works only for trifaze, change the cable array.

	for (int i = 1; i <= m_cablecount; ++i) // Ignore collisions between cables.
	{
		AugUtils::IgnoreCollisionBetween(m_pEntity->GetName(), GetCableName(i).c_str());
		AugUtils::IgnoreCollisionBetween(AUG_ENTITY_NAME_LEFT_HAND.c_str(), GetCableName(i).c_str());
		AugUtils::IgnoreCollisionBetween(AUG_ENTITY_NAME_RIGHT_HAND.c_str(), GetCableName(i).c_str());

		for (int b = i + 1; b <= m_cablecount; ++b)
		{
			AugUtils::IgnoreCollisionBetween(GetCableName(i).c_str(), GetCableName(b).c_str());
		}
	}
}

void CSayac::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		float dt = event.fParam[0];
		m_pCarry->SetEnabled(!ShouldDisable());
		DisableCollisions();
		DisableKlemensCover();
		DisableCables();
		DisablePano();

	}
	break;
	case ENTITY_EVENT_PREPHYSICSUPDATE:
	{
		float dt = event.fParam[0];
	}
	break;
	case ENTITY_EVENT_PHYS_POSTSTEP:
	{
	}
	break;
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	case ENTITY_EVENT_RESET:
	{
		m_screwsInitialized = false;
	}
	break;
	case ENTITY_EVENT_START_GAME:
	case ENTITY_EVENT_START_LEVEL:
	{
		cables.clear();
		for (int i = 1; i <= 13; ++i)
		{
			if (auto cable = gEnv->pEntitySystem->FindEntityByName(GetCableName(i).c_str()))
			{
				cables.push_back(cable->GetOrCreateComponentClass<CCarryComponent>());
			}			
		}

		klemensList.clear();
		for (int i = 1; i <= 13; ++i) // KLEMENS 12 and 13 are reserved for modem cables.
		{	
			if (auto klemens = gEnv->pEntitySystem->FindEntityByName(GetKlemensName(i).c_str()))
				klemensList.push_back(klemens->GetOrCreateComponentClass<CPlugFemaleTerminal>());
		}

		auto klemensCover = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_KLEMENS_COVER.c_str());
		m_pKlemensCoverCarry = klemensCover->GetComponent<CCarryComponent>();
		m_pKlemensCoverToggle = klemensCover->GetComponent<CToggleComponent>();

		if (auto Pano = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_PANO.c_str()))
		{
			m_pPanoToggle = Pano->GetComponent<CToggleComponent>();
			m_pPanoCarry = Pano->GetComponent<CCarryComponent>();
		}
	}
	break;
	}
}

bool CSayac::ShouldDisable()
{
	using Cmd = StateManager::ECommand;
	using Act = EInteractionComponenetIDs;
	using Objs = StateManager::TStateObjects;
	auto CONNECTED = ESecenarioQueryActions::CONNECTION;
	auto IF_CONNECTED = ESecenarioQueryActions::IF_CONNECTED;
	auto& state = CGamePlugin::GetStateManager();

	if (state.Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, Objs{ m_pEntity->GetName(), ENTITY_NAME_SAYAC_FEMALE }).size() == 0)
	{
		return false;
	}

	if (state.Query(Cmd::PLUG_SYSTEM_CHECK, CONNECTED, Objs{ ENTITY_NAME_KLEMENS_COVER, m_pEntity->GetName() }).size() > 0)
	{
		return true;
	}

	for (int i = 1; i <= m_cablecount; ++i)
	{
		if (state.Query(Cmd::PLUG_SYSTEM_CHECK, IF_CONNECTED, Objs{ GetCableName(i) }).size() > 0)
			return true;
	}

	return false;
}