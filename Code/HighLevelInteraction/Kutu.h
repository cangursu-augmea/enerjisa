// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:36 PM 26/02/2019, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>

class CKutuComponent
	: public IEntityComponent

{
public:
	CKutuComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CKutuComponent>& desc)
	{
		desc.SetGUID("{29913140-0DDC-4AEB-9BA4-1E5625A04159}"_cry_guid);
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Kutu");
		desc.SetDescription("For Kutu only");
		//EDITOR MEMBERS
		desc.AddMember(&CKutuComponent::m_MultiColorNormalModel, 'mcnm', "MultiColorNormalModelOfKutu", "MultiColor Normal Model Of Kutu", "MultiColor Normal Model Of Kutu", "");
		desc.AddMember(&CKutuComponent::m_MultiColorBrokenModel, 'mcbm', "MultiColorBrokenModelOfKutu", "MultiColor Broken Model Of Kutu", "MultiColor Broken Model Of Kutu", "");
		desc.AddMember(&CKutuComponent::m_MonoColorNormalModel , 'munm', "MonoColoNormalModelOfKutu"  , "MonoColo Normal Model Of Kutu"  , "MonoColo Normal Model Of Kutu"  , "");
		desc.AddMember(&CKutuComponent::m_MonoColorBrokenModel , 'mubm', "MonoColoBrokenModelOfKutu"  , "MonoColo Broken Model Of Kutu"  , "MonoColo Broken Model Of Kutu"  , "");
		desc.AddMember(&CKutuComponent::m_X5NormalModel		   , 'x5nm', "X5NormalModelOfKutu"		  , "X5 Normal Model Of Kutu"		 , "X5 Normal Model Of Kutu"		, "");
		desc.AddMember(&CKutuComponent::m_X5BrokenModel		   , 'x5bm', "X5BrokenModelOfKutu"		  , "X5 Broken Model Of Kutu"		 , "X5 Broken Model Of Kutu"		, "");

	}

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent


private:

	Schematyc::GeomFileName m_MultiColorNormalModel;
	Schematyc::GeomFileName m_MultiColorBrokenModel;
	Schematyc::GeomFileName m_MonoColorNormalModel;
	Schematyc::GeomFileName m_MonoColorBrokenModel;
	Schematyc::GeomFileName m_X5NormalModel;
	Schematyc::GeomFileName m_X5BrokenModel;

}; 
