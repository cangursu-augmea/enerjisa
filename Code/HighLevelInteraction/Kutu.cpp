// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:36 PM 26/02/2019, Utku Guler

#include "StdAfx.h"
#include "HighLevelInteraction/Kutu.h"
#include "Interaction/InteractionManagerComponent.h"
#include <sstream>
#include <CryRenderer/IRenderAuxGeom.h>

void CKutuComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CKutuComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CKutuComponent::Register)

CKutuComponent::CKutuComponent()
	: m_MultiColorNormalModel("")
	, m_MultiColorBrokenModel("")
	, m_MonoColorNormalModel("")
	, m_MonoColorBrokenModel("")
{

}

void CKutuComponent::Initialize()
{
	Cry::DefaultComponents::CStaticMeshComponent* pMeshComp;
	pMeshComp = m_pEntity->GetComponent<Cry::DefaultComponents::CStaticMeshComponent>();	
	pMeshComp->GetComponentFlags().Add(EEntityComponentFlags::UserAdded);

	string levelType = gEnv->pConsole->GetCVar("aug_training_set")->GetString();
	
	if(!strcmp(levelType, "TRIFAZE"))
	{
		string cableCondition = gEnv->pConsole->GetCVar("aug_cable_condition")->GetString();
		string colorType = gEnv->pConsole->GetCVar("aug_cable_color")->GetString();

		if (cableCondition == "NORMAL")
		{
			if (colorType == "MULTICOLOR")
				pMeshComp->SetFilePath(m_MultiColorNormalModel.value.c_str());
			else
				pMeshComp->SetFilePath(m_MonoColorNormalModel.value.c_str());
		}
		else if (cableCondition == "BROKEN")
		{
			if (colorType == "MULTICOLOR")
				pMeshComp->SetFilePath(m_MultiColorBrokenModel.value.c_str());
			else
				pMeshComp->SetFilePath(m_MonoColorBrokenModel.value.c_str());
		}
	}
	else if (!strcmp(levelType, "X5"))
	{
		string cableOrder = gEnv->pConsole->GetCVar("aug_transformer_order")->GetString();

			if (cableOrder == "CORRECT")
				pMeshComp->SetFilePath(m_X5NormalModel.value.c_str());
			else
				pMeshComp->SetFilePath(m_X5BrokenModel.value.c_str());
	}
	

	pMeshComp->LoadFromDisk();
	pMeshComp->ResetObject();
}

void CKutuComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	break;
	case ENTITY_EVENT_UPDATE:
	{

	}
	break;
	}
}
