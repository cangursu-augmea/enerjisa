// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:12 21.01.2019, Ali Mehmet Altundag

#include "StdAfx.h"
#include "Screw.h"
#include "Screwdriver.h"
#include "Interaction/InteractionManagerComponent.h"
#include "StdDefs.h"

void CScrewComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CScrewComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CScrewComponent::Register)

CScrewComponent::CScrewComponent()
	: m_pInteractionManager(nullptr)
	, m_pToggle(nullptr)
	, m_isAvailableForToggle(true)
{}

void CScrewComponent::Initialize()
{
	CInteractionManagerComponent* pInteraction = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	CRY_ASSERT_MESSAGE(pInteraction != nullptr, "CScrewComponent component needs to have interaction component attached to the entity.");

	m_pToggle = m_pEntity->GetOrCreateComponent<CToggleComponent>();
	m_pToggle->SetEnabled(false);
	CRY_ASSERT_MESSAGE(m_pToggle != nullptr, "CScrewComponent component needs to have CToggleComponent attached to the entity.");

	m_pInteractionManager = m_pEntity->GetComponent<CInteractionManagerComponent>();
	m_pAudioController = m_pEntity->GetOrCreateComponent<CAudioController>();

	if(m_klemens = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_KLEMENS_COVER.c_str()))
		m_klemensMaleTerminal = m_klemens->GetComponent<CPlugMaleTerminal>();
	CRY_ASSERT_MESSAGE(m_klemensMaleTerminal != nullptr, "CKlemensComponent component needs to have CPlugMaleTerminal attached to the entity.");

	if (auto pSayacFemale = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_SAYAC_FEMALE.c_str()))
	{
		m_sayacFemaleTerminal = pSayacFemale->GetComponent<CPlugFemaleTerminal>();
	}
	else
	{
		CryLogAlways("!NO SAYAC FEMALE FOUND!");
	}
}

void CScrewComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_START_GAME:
	case ENTITY_EVENT_RESET:
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	case ENTITY_EVENT_LEVEL_LOADED:
	{
		m_pToggle->SetState(m_DefaultState);
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (!m_interactionExists )
			m_isAvailableForToggle = true;

		if(m_klemensMaleTerminal)
		{
			bool coverConnectedOnWall = m_klemensMaleTerminal->IsConnected() && m_sayacFemaleTerminal->IsConnected() &&
				m_klemensMaleTerminal->GetConnectedFemale()->GetEntity() == m_sayacFemaleTerminal->GetConnectedMaleTerminal()->GetEntity();
		}
		

		if (m_interactionExists && m_pToggle && m_isAvailableForToggle )
		{
			m_pAudioController->StopByName("screw"); // stop audio
			
			m_pToggle->Toggle();
			m_isAvailableForToggle = false;
			m_pAudioController->PlayByName("screw"); // play audio
		}
		m_interactionExists = false;
	}
	break;
	}
}

SInteractionFeedback CScrewComponent::InteractionTick()
{
	if (m_pInteractionManager)
	{
		auto pSource = m_pInteractionManager->GetInteractionSource();
		if (pSource->GetInteractionId() == EHighLevelInteractionIDs::SCREWDRIVER)
		{
			m_interactionExists = true;
		}
	}

	return m_feedback;
}