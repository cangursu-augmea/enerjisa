﻿// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:02 aM 11/02/2019, Utku Guler

#include "StdAfx.h"
#include "MultiMeterRedProbe.h"
#include "StdDefs.h"
#include "Interaction/InteractionManagerComponent.h"
#include "Components/PlugSystem/PlugMaleTerminal.h"
#include <DefaultComponents/Lights/PointLightComponent.h>
#include <sstream>
#include <string.h>


void CMultiMeterRedProbeComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CMultiMeterRedProbeComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CMultiMeterRedProbeComponent::Register)

CMultiMeterRedProbeComponent::CMultiMeterRedProbeComponent()
	: m_OffSet(0.f, 0.f, 0.f)
	, m_Lengt(1.f)
	, m_operate(true)
	, m_PosOffSet(0.f, 0.f, 0.f)
	, m_fuseToggle()
	, m_blackProbe(false)
	, m_CoolDown(30)
{

}


SInteractionFeedback CMultiMeterRedProbeComponent::InteractionTick()
{
	return SInteractionFeedback();
}
void CMultiMeterRedProbeComponent::Initialize()
{
	m_audioController = m_pEntity->GetOrCreateComponent<CAudioController>();

	MultiMeterOutPut buffer;

	for (int i = 1; i <= 13; i++)
	{
		buffer.Object = std::string("FAZ_KABLO_" + std::to_string(i));
		buffer.state = false;
		buffer.Timer = 0;
		m_OutPutList.push_back(buffer);
	}

	for (int i = 1; i <= 3; i++)
	{
		buffer.Object = std::string("SALTER_" + std::to_string(i));
		buffer.state = false;
		buffer.Timer = 0;
		m_OutPutList.push_back(buffer);
	}
}


void CMultiMeterRedProbeComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	break;
	case ENTITY_EVENT_LEVEL_LOADED:
	{
		if (IEntity* fuse = gEnv->pEntitySystem->FindEntityByName("SIGORTA"))
			m_fuseToggle = fuse->GetOrCreateComponentClass<CToggleComponent>();
		if (IEntity* BlackProbe = gEnv->pEntitySystem->FindEntityByName("BlackProbe_M"))
			m_blackProbe = BlackProbe->GetOrCreateComponentClass<CBlackProbeComponent>();
		if (IEntity* Pensampermetre = gEnv->pEntitySystem->FindEntityByName("PENSAMPERMETRE"))
			m_pensampermetre = Pensampermetre->GetOrCreateComponentClass<CPensAmpermetreComponent>();

		m_pensDisplay = gEnv->pEntitySystem->FindEntityByName("PENS_DISPLAY");
		m_pensDisplay->Hide(true);
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{

		if (m_operate != true) return;
		//CToggleComponent* ToggleCom = m_pEntity->GetOrCreateComponent<CToggleComponent>();
		//if (ToggleCom->GetCurrentState() == false)return;
		Vec3 target = m_pEntity->GetWorldRotation() * m_OffSet;
		Vec3 m_Dir = target;
		Vec3 Pos = m_pEntity->GetWorldPos();
		Pos = Pos + m_pEntity->GetWorldRotation() * m_PosOffSet;

		float DeltaTime = gEnv->pTimer->GetFrameTime();
		for (int i = 0; i<m_OutPutList.size(); i++)
		{
			if (m_OutPutList[i].Timer > 0)
			{
				m_OutPutList[i].Timer -= DeltaTime;
			}
			if (m_OutPutList[i].Timer <= 0 && m_OutPutList[i].state == true)
			{
				/////EVENT SEND/////
				StateManager::Event event;
				event.objects = { m_OutPutList[i].Object };
				event.action = GetInteractionId();
				event.result.bValue = false;
				CGamePlugin::GetStateManager().Insert(event);
				/////EVENT SEND/////

				m_OutPutList[i].state = false;
			}
		}

		gEnv->pAuxGeomRenderer->DrawLine(Pos, Col_Blue, Pos + ((m_Dir).normalize() * m_Lengt), Col_Blue, 2);

		const int maxHitCount = 128;
		int nHit = 0;
		ray_hit hits[maxHitCount];
		if ((nHit = gEnv->pPhysicalWorld->RayWorldIntersection(Pos, ((m_Dir).normalize() * m_Lengt), ent_static | ent_sleeping_rigid | ent_rigid, rwi_stop_at_pierceable, hits, maxHitCount, m_pEntity->GetPhysicalEntity())))
		{
			//CryLogAlways("hit");
			for (int i = 0; i < nHit; ++i)
			{
				IPhysicalEntity* pPhyEnt = hits[i].pCollider;
				if (pPhyEnt)
				{
					IEntity* pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pPhyEnt);
					if (pEnt)
					{
						std::string name(pEnt->GetName());

						if (CMeasurableComponent* Measurable = pEnt->GetComponent<CMeasurableComponent>())
						{
							//////////////////////////////////////////  ACTION  //////////////////////////////////////////
							if (Measurable->IsEnable() == false)
								return;
							int RedType = Measurable->GetType();
							int MultiMod = m_pensampermetre->GetType();
							if (m_blackProbe->IsActive())
							{

								switch (MultiMod)
								{
								case 1:
								{

								}
								break;
								case 2:
								{
									int BlackType = m_blackProbe->getType();
									std::string BlackName = m_blackProbe->getName();
									if (RedType == 0 && BlackType == 0) { //VOLT
										Reset(NUMERICAL_GAUGE_0V);
									}
									else
									{
										if (m_fuseToggle->GetCurrentState())
										{
											//if ((RedType == 1 || RedType == 5) && BlackType == 1) { // FAZ -- FAZ
											//	CryLogAlways("50 VOLT");
											//}
											//if ((RedType == 1 || RedType == 5) && BlackType == 2) { // FAZ -- Cikis
											//	CryLogAlways("50 VOLT");
											//}
											//if (RedType == 2 && BlackType == 1) { // ÇIKIŞ -- FAZ
											//	CryLogAlways("50 VOLT");
											//}
											std::string eventName = "";
											if ((RedType == 2 && BlackType == 3) || (RedType == 3 && BlackType == 2)) { // Cikis -- Notr
												if (!strcmp(name.c_str(), "FAZ_KABLO_2") || !strcmp(BlackName.c_str(), "FAZ_KABLO_2")) {
													CryLogAlways("220 VOLT");
													Reset(NUMERICAL_GAUGE_220V);
													eventName = "FAZ_KABLO_2";
												}
												if (!strcmp(name.c_str(), "FAZ_KABLO_5") || !strcmp(BlackName.c_str(), "FAZ_KABLO_5")) {
													CryLogAlways("218 VOLT");
													Reset(NUMERICAL_GAUGE_218V);
													eventName = "FAZ_KABLO_5";
												}
												if (!strcmp(name.c_str(), "FAZ_KABLO_8") || !strcmp(BlackName.c_str(), "FAZ_KABLO_8")) {
													CryLogAlways("222 VOLT");
													Reset(NUMERICAL_GAUGE_222V);
													eventName = "FAZ_KABLO_8";
												}
												//else{
												//	CryLogAlways("220 VOLT");
												//	Reset(NUMERICAL_GAUGE_220V);
												//}
											}
											else {
												CryLogAlways("0 VOLT");
												Reset(NUMERICAL_GAUGE_0V);
											}
											//if ((RedType == 4 && BlackType == 2) || (RedType == 2 && BlackType == 4)) { //BOZUK KABLO -- ÇIKIÞ
											//	CryLogAlways("50 VOLT");
											//}

											for (int i = 0; i < m_OutPutList.size(); i++)
											{
												if (m_OutPutList[i].Object == eventName && m_OutPutList[i].state == false)
												{
													m_OutPutList[i].state = true;
													m_OutPutList[i].Timer = m_CoolDown;
													/////EVENT SEND/////
													StateManager::Event event;
													event.objects = { eventName };
													event.action = GetInteractionId();
													event.result.bValue = true;
													CGamePlugin::GetStateManager().Insert(event);
													/////EVENT SEND/////
													CryLogAlways("AMPERMETRE------------------------------ %s", eventName);
												}
											}
										}
									}
								}
								break;
								case 3:
								{
									int BlackType = m_blackProbe->getType();
									if (RedType == 0 && BlackType == 0) { //VOLT
										Reset(NUMERICAL_GAUGE_0V);
									}

								}
								break;
								default:
									m_pensDisplay->Hide(true);
									break;
								}
								
									
				

							}
						}
						else
						{
							m_pensDisplay->Hide(true);
						}
					}
				}
			}
		}
	}
	break;
	}
}

void CMultiMeterRedProbeComponent::Reset(const std::string meshPath)
{
	// unhide entity
	m_pensDisplay->Hide(false);
	m_pensDisplay->LoadGeometry(geometrySlot, meshPath.c_str());
}

//bool CMultiMeterRedProbeComponent::IsPairPlugged(int pairID)
//{
//	std::vector<int> default;
//
//	auto set = gEnv->pConsole->GetCVar("aug_training_set");
//	if (set->GetString() == "X5")
//		default = { 1, 4, 7, 10, 12 };
//	else
//		default = { 1, 3, 5, 7 };
//
//
//	for (const auto i : default) {
//		if (auto pPhaseCable = gEnv->pEntitySystem->FindEntityByName(Cable(i).c_str()))
//		{
//			if (CMeasurableComponent* Measurable = pPhaseCable->GetComponent<CMeasurableComponent>())
//			{
//				if (Measurable->GetPairID() == pairID)
//				{
//					CPlugMaleTerminal* MaleTerminal = pPhaseCable->GetComponent<CPlugMaleTerminal>();
//					return MaleTerminal->IsConnected();
//				}
//			}
//		}
//	}
//
//
//	return false;
//}
