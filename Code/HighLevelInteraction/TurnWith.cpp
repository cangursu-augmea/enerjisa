﻿// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 9:25 AM 01/07/2019, Utku Guler
// Edited by Ali Mehmet Altundag on 11:54 19.01.2019

#include "StdAfx.h"
#include "TurnWith.h"
#include "Interaction/InteractionManagerComponent.h"

void CTurnWithComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CTurnWithComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CTurnWithComponent::Register)

CTurnWithComponent::CTurnWithComponent() :
	m_dir(Vec3(0.f, 0.f, 0.f)), isTurned(false), kesiciSoundState(false), ayiriciLoadSoundState(true), ayiriciOutSoundState(true)
{

}

SInteractionFeedback CTurnWithComponent::InteractionTick()
{
	return SInteractionFeedback();
}

void CTurnWithComponent::Initialize()
{
	if (m_pStaka = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_KRIKO.c_str()))
		m_pKrikoTurnComponent = m_pStaka->GetComponent<CTurnComponent>();

	if (m_pLoadToprak = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_AYIRICI_LOAD_CELL_KRIKO_TOPRAK_DELIGI.c_str()))
		m_pLoadToprakFemale = m_pLoadToprak->GetComponent<CPlugFemaleTerminal>();

	if (m_pOutToprak = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_AYIRICI_OUT_CELL_KRIKO_TOPRAK_DELIGI.c_str()))
		m_pOutToprakFemale = m_pOutToprak->GetComponent<CPlugFemaleTerminal>();
	////////////
	if (m_pLoadAyirici = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_AYIRICI_LOAD_CELL_KRIKO_DELIGI.c_str()))
		m_pLoadAyiriciFemale = m_pLoadAyirici->GetComponent<CPlugFemaleTerminal>();

	if (m_pOutAyirici = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_AYIRICI_OUT_CELL_KRIKO_DELIGI.c_str()))
		m_pOutAyiriciFemale = m_pOutAyirici->GetComponent<CPlugFemaleTerminal>();
		//////////////
	if (m_pAnahtar = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_ANAHTAR.c_str()))
		m_pAnahtarTurnComponent = m_pAnahtar->GetComponent<	CTurnComponent>();

	if (m_pKesiciAnahtarDeligi = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_CUTTING_KEY.c_str()))
		m_pKesiciAnahtarDeligiPlugComponent = m_pKesiciAnahtarDeligi->GetComponent<	CPlugFemaleTerminal>();

	if (m_pKesiciButton = gEnv->pEntitySystem->FindEntityByName(ENTITY_NAME_CUTTING_BUTTON.c_str()))
		m_pKesiciButtonToggleComponent = m_pKesiciButton->GetComponent<CToggleComponent>();

	m_pAudioController = m_pEntity->GetOrCreateComponent<CAudioController>();
}

void CTurnWithComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	case ENTITY_EVENT_UPDATE:
	{
		//kriko olmasi gereken yere takili ve dondurulduyse topraklama okunu gereken yone cevir.
		//topraklama_oku
		if (m_pKrikoTurnComponent->GetTurnActionStarted() && m_pLoadAyiriciFemale->IsConnected())
		{
			if (ayiriciLoadSoundState)
			{
				m_pAudioController->PlayByName("manevraSound");
				ayiriciLoadSoundState = false;
			}
			m_dir = (Vec3(0.f, 1.f, 0.f));
			Matrix34 TM = gEnv->pEntitySystem->FindEntityByName("TOPRAKLAMA_LOAD_OK")->GetWorldTM();
			Matrix33 RotTM = Matrix33::CreateRotationVDir(m_dir, 0.88f* gEnv->pTimer->GetFrameTime());
			TM = TM * RotTM;
			gEnv->pEntitySystem->FindEntityByName("TOPRAKLAMA_LOAD_OK")->SetWorldTM(TM);
		}

		if (m_pKrikoTurnComponent->GetTurnActionStarted() && m_pOutAyiriciFemale->IsConnected())
		{
			if (ayiriciOutSoundState)
			{
				m_pAudioController->PlayByName("manevraSound");
				ayiriciOutSoundState = false;
			}
			m_dir = (Vec3(0.f, 1.f, 0.f));
			Matrix34 TM = gEnv->pEntitySystem->FindEntityByName("TOPRAKLAMA_OUT_OK")->GetWorldTM();
			Matrix33 RotTM = Matrix33::CreateRotationVDir(m_dir, 0.88f* gEnv->pTimer->GetFrameTime());
			TM = TM * RotTM;
			gEnv->pEntitySystem->FindEntityByName("TOPRAKLAMA_OUT_OK")->SetWorldTM(TM);
		}

		if (m_pKrikoTurnComponent->GetTurnActionStarted() && m_pLoadToprakFemale->IsConnected())
		{
			if (!ayiriciLoadSoundState)
			{
				m_pAudioController->PlayByName("manevraSound");
				ayiriciLoadSoundState = true;
			}
			m_dir = (Vec3(0.f, 1.f, 0.f));
			Matrix34 TM = gEnv->pEntitySystem->FindEntityByName("TOPRAKLAMA_LOAD_OK")->GetWorldTM();
			Matrix33 RotTM = Matrix33::CreateRotationVDir(m_dir, 0.88f* gEnv->pTimer->GetFrameTime());
			TM = TM * RotTM;
			gEnv->pEntitySystem->FindEntityByName("TOPRAKLAMA_LOAD_OK")->SetWorldTM(TM);
		}
		if (m_pKrikoTurnComponent->GetTurnActionStarted() && m_pOutToprakFemale->IsConnected())
		{
			if (!ayiriciOutSoundState)
			{
				m_pAudioController->PlayByName("manevraSound");
				ayiriciOutSoundState = true;
			}
			m_dir = (Vec3(0.f, 1.f, 0.f));
			Matrix34 TM = gEnv->pEntitySystem->FindEntityByName("TOPRAKLAMA_OUT_OK")->GetWorldTM();
			Matrix33 RotTM = Matrix33::CreateRotationVDir(m_dir, 0.88f* gEnv->pTimer->GetFrameTime());
			TM = TM * RotTM;
			gEnv->pEntitySystem->FindEntityByName("TOPRAKLAMA_OUT_OK")->SetWorldTM(TM);
		}
		//~topraklama_oku

		//kesici anahtari bagli olmadan kesici dugmesi deactive
		//kesici dugmesi enable?disable
		if (!m_pKesiciAnahtarDeligiPlugComponent->IsConnected())
			m_pKesiciButtonToggleComponent->SetEnabled(false);

		else if (m_pAnahtarTurnComponent->GetTurnActionStarted())
			m_pKesiciButtonToggleComponent->SetEnabled(true);

		if (!m_pKesiciButtonToggleComponent->GetCurrentState() && !kesiciSoundState)
		{
			m_pAudioController->PlayByName("kesiciButtonOff");
			kesiciSoundState = true;
		}
		else if (m_pKesiciButtonToggleComponent->GetCurrentState() && kesiciSoundState)
		{
			m_pAudioController->PlayByName("kesiciButtonOn");
			kesiciSoundState = false;
		}
		//~kesici dugmesi enable?disable

	}
	break;
	}
}




