// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:02 aM 11/02/2019, Utku Guler

#include "StdAfx.h"
#include "RedProbe.h"
#include "StdDefs.h"
#include "Interaction/InteractionManagerComponent.h"
#include "Components/PlugSystem/PlugMaleTerminal.h"
#include <DefaultComponents/Lights/PointLightComponent.h>
#include <sstream>
#include <string.h>


void CRedProbeComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CRedProbeComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CRedProbeComponent::Register)

CRedProbeComponent::CRedProbeComponent()
	: m_OffSet(0.f, 0.f, 0.f)
	, m_Lengt(1.f)
	, m_operate(true)
	, m_PosOffSet(0.f, 0.f, 0.f)
	, m_fuseToggle()
	, m_blackProbe(false)
	, m_CoolDown(30)
{

}


SInteractionFeedback CRedProbeComponent::InteractionTick()
{
	return SInteractionFeedback();
}
void CRedProbeComponent::Initialize()
{
	m_audioController = m_pEntity->GetOrCreateComponent<CAudioController>();

	OutPut buffer;

	for(int i = 1 ; i<=13 ; i++)
	{
		buffer.Object = std::string("FAZ_KABLO_" + std::to_string(i));
		buffer.state = false;
		buffer.Timer = 0;
		m_OutPutList.push_back(buffer);
	}

	for (int i = 1; i <= 3; i++)
	{
		buffer.Object = std::string("SALTER_" + std::to_string(i));
		buffer.state = false;
		buffer.Timer = 0;
		m_OutPutList.push_back(buffer);
	}
}


void CRedProbeComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{
		
	}
	break;
	case ENTITY_EVENT_LEVEL_LOADED:
	{
		if(IEntity* fuse = gEnv->pEntitySystem->FindEntityByName("SIGORTA"))
			m_fuseToggle = fuse->GetOrCreateComponentClass<CToggleComponent>();
		if(IEntity* BlackProbe = gEnv->pEntitySystem->FindEntityByName("BlackProbe"))
			m_blackProbe = BlackProbe->GetOrCreateComponentClass<CBlackProbeComponent>();
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{

		if (m_operate != true) return;
		//CToggleComponent* ToggleCom = m_pEntity->GetOrCreateComponent<CToggleComponent>();
		//if (ToggleCom->GetCurrentState() == false)return;
		Vec3 target = m_pEntity->GetWorldRotation() * m_OffSet;
		Vec3 m_Dir = target;
		Vec3 Pos = m_pEntity->GetWorldPos();
		Pos = Pos + m_pEntity->GetWorldRotation() * m_PosOffSet;

		float DeltaTime = gEnv->pTimer->GetFrameTime();
		for (int i = 0; i<m_OutPutList.size(); i++)
		{
			if (m_OutPutList[i].Timer > 0)
			{
				m_OutPutList[i].Timer -= DeltaTime;
			}
			if(m_OutPutList[i].Timer <= 0 && m_OutPutList[i].state == true)
			{
				/////EVENT SEND/////
				StateManager::Event event;
				event.objects = { m_OutPutList[i].Object };
				event.action = GetInteractionId();
				event.result.bValue = false;
				CGamePlugin::GetStateManager().Insert(event);
				/////EVENT SEND/////

				m_OutPutList[i].state = false;
			}
		}

		gEnv->pAuxGeomRenderer->DrawLine(Pos, Col_Blue, Pos + ((m_Dir).normalize() * m_Lengt), Col_Blue, 2);

		const int maxHitCount = 128;
		int nHit = 0;
		ray_hit hits[maxHitCount];
		if ((nHit = gEnv->pPhysicalWorld->RayWorldIntersection(Pos, ((m_Dir).normalize() * m_Lengt), ent_static | ent_sleeping_rigid | ent_rigid, rwi_stop_at_pierceable, hits, maxHitCount, m_pEntity->GetPhysicalEntity())))
		{
			//CryLogAlways("hit");
			for (int i = 0; i < nHit; ++i)
			{
				IPhysicalEntity* pPhyEnt = hits[i].pCollider;
				if (pPhyEnt)
				{
					IEntity* pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pPhyEnt);
					if (pEnt)
					{
						std::string name(pEnt->GetName());
						
						if(CMeasurableComponent* Measurable = pEnt->GetComponent<CMeasurableComponent>())
						{
							//////////////////////////////////////////  ACTION  //////////////////////////////////////////
							if (Measurable->IsEnable()==false)
								return;
							int RedType = Measurable->GetType();
							if(m_blackProbe->IsActive())
							{
								int BlackType = m_blackProbe->getType();
								if (RedType == 0 && BlackType == 0) {
									TurnOnLight("PROBE_LIGHT", true);
									TurnOnLight("380", false);
									TurnOnLight("220", false);
								}
								else
								{
									if (m_fuseToggle->GetCurrentState())
									{
										if ((RedType == 1 || RedType == 5) && BlackType == 1) {
											TurnOnLight("380", true);
											TurnOnLight("PROBE_LIGHT", true);//FAZ FAZ
											TurnOnLight("220", false);
										}										
										if ((RedType == 1 || RedType == 5) && BlackType == 2) {
											TurnOnLight("220", true);
											TurnOnLight("PROBE_LIGHT", true);
											TurnOnLight("380", false);
										}	
										if (RedType == 2 && (RedType == 1 || RedType == 5)) {
											TurnOnLight("220", true);
											TurnOnLight("PROBE_LIGHT", true);
											TurnOnLight("380", false);
										}
										if ((RedType == 4 && BlackType == 2) || (RedType == 2 && BlackType == 4)) {
											TurnOnLight("220", true);
											TurnOnLight("PROBE_LIGHT", true); //BOZUK KABLO
											TurnOnLight("380", false);
										}
											
										for (int i = 0; i < m_OutPutList.size(); i++)
										{
											if (m_OutPutList[i].Object == name && m_OutPutList[i].state == false)
											{
												m_OutPutList[i].state = true;
												m_OutPutList[i].Timer = m_CoolDown;
												/////EVENT SEND/////
												StateManager::Event event;
												event.objects = { name };
												event.action = GetInteractionId();
												event.result.bValue = true;
												CGamePlugin::GetStateManager().Insert(event);
												/////EVENT SEND/////
												CryLogAlways("MULTIMETRE------------------------------ %s", name);
											}
										}

									}
								}
							}
							else
							{
								static bool flag = true;
								if (RedType == 3 )
								{
									if(flag)
									{
									/////EVENT SEND/////
									StateManager::Event event;
									event.objects = { ENTITY_NAME_CONTROL_PROBE , name };
									event.action = EInteractionComponenetIDs::TOUCH;
									event.result.bValue = true;
									CGamePlugin::GetStateManager().Insert(event);
									/////EVENT SEND/////
									flag = false;
									}
									return;
								}

								if(Measurable->GetIsInput() == false)
								{
									if(m_fuseToggle->GetCurrentState())
									{
										CPlugMaleTerminal* MaleTerminal = pEnt->GetComponent<CPlugMaleTerminal>();
										if(MaleTerminal->IsConnected())
											if(IsPairPlugged(Measurable->GetPairID()))
											{
												TurnOnLight("PROBE_LIGHT", true);
												TurnOnLight("380", false);
												TurnOnLight("220", false);
											}
										for (int i = 0; i < m_OutPutList.size(); i++)
										{
											if (m_OutPutList[i].Object == name && m_OutPutList[i].state == false)
											{
												m_OutPutList[i].state = true;
												m_OutPutList[i].Timer = m_CoolDown;
												/////EVENT SEND/////
												StateManager::Event event;
												event.objects = { name };
												event.action = GetInteractionId();
												event.result.bValue = true;
												CGamePlugin::GetStateManager().Insert(event);
												/////EVENT SEND/////
												CryLogAlways("MULTIMETRE------------------------------ %s", name);
											}
										}
									}

									
								}
								else
								{
									if (m_fuseToggle->GetCurrentState())
									{
										TurnOnLight("PROBE_LIGHT", true);
										TurnOnLight("380", false);
										TurnOnLight("220", false);
										for (int i = 0; i < m_OutPutList.size(); i++)
										{
											if (m_OutPutList[i].Object == name && m_OutPutList[i].state == false)
											{
												m_OutPutList[i].state = true;
												m_OutPutList[i].Timer = m_CoolDown;
												/////EVENT SEND/////
												StateManager::Event event;
												event.objects = { name };
												event.action = GetInteractionId();
												event.result.bValue = true;
												CGamePlugin::GetStateManager().Insert(event);
												/////EVENT SEND/////
												CryLogAlways("MULTIMETRE------------------------------ %s", name);
											}
										}
									}
									else
									{
										TurnOnLight("PROBE_LIGHT", false);
										TurnOnLight("380", false);
										TurnOnLight("220", false);
									}
								}	
							}
							//////////////////////////////////////////  ACTION  //////////////////////////////////////////
						}
						else
						{
							TurnOnLight("PROBE_LIGHT", false);
							TurnOnLight("380", false);
							TurnOnLight("220", false);
						}
					}
				}
			}
		}
		else
		{
			TurnOnLight("PROBE_LIGHT", false);
			TurnOnLight("380", false);
			TurnOnLight("220", false);
		}

	}
	break;
	}
}


void CRedProbeComponent::TurnOnLight(const char* lightName, bool active)
{
	static bool audioFlag = true;

	auto entityLight = gEnv->pEntitySystem->FindEntityByName(lightName);

	if (entityLight) {
		auto componentLight = entityLight->GetComponent<Cry::DefaultComponents::CPointLightComponent>();
		componentLight->Enable(active);
		
		if (!strcmp(lightName, "PROBE_LIGHT")) {
			if (active) { // play if active and audioFlag are true
				if (audioFlag) {
					audioFlag = false;
					m_audioController->PlayByName("beep");
				}
			}
			else { // stop if active and audioFlag are false
				audioFlag = true;
				m_audioController->StopByName("beep");
			}
			
		}
		

	}
	else {
		CryLogAlways("%s named entity not found...", lightName);
	}
}

bool CRedProbeComponent::IsPairPlugged(int pairID)
{
	std::vector<int> default;

	auto set = gEnv->pConsole->GetCVar("aug_training_set");
	if (set->GetString() == "X5")
		default = { 1, 4, 7, 10, 12 };
	else
		default = { 1, 3, 5, 7 };


	for (const auto i : default) {
		if (IEntity* pPhaseCable = gEnv->pEntitySystem->FindEntityByName(Cable(i).c_str()))
		{
			if (CMeasurableComponent* Measurable = pPhaseCable->GetComponent<CMeasurableComponent>())
			{
				if(Measurable->GetPairID() == pairID )
				{
					CPlugMaleTerminal* MaleTerminal = pPhaseCable->GetComponent<CPlugMaleTerminal>();
					return MaleTerminal->IsConnected();
				}	
			}
		}
	}


	return false;
}
