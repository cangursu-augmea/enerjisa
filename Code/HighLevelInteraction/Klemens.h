// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 21.01.2019, Alper �ekerci

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include "Components\PlugSystem\PlugFemaleTerminal.h"
#include "Components\Toggle.h"

class CKlemens final : public IEntityComponent
{
private:
	CPlugFemaleTerminal * m_pFemale;
	CToggleComponent * m_pToggle;
	void UpdateFemaleId();

public:
	CKlemens() = default;
	~CKlemens() = default;

	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(const SEntityEvent& event) override;
	
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CKlemens>& desc)
	{
		desc.SetGUID("{314F016B-687D-4B57-B9CC-8ADA84FA5BEC}"_cry_guid);
		desc.SetEditorCategory("AUGMEA");
		desc.SetLabel("Klemens Component");
	}
};