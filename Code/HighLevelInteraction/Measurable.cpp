// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 2:37 PM 15/02/2019, Utku Guler

#include "StdAfx.h"
#include "HighLevelInteraction/Measurable.h"
#include "Interaction/InteractionManagerComponent.h"
#include <sstream>
#include <CryRenderer/IRenderAuxGeom.h>

void CMeasurableComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CMeasurableComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CMeasurableComponent::Register)

CMeasurableComponent::CMeasurableComponent()
	: m_enabled(true)
	, m_type(0)
	, m_isInput(true)
	, m_pairID(0)
	
{

}

void CMeasurableComponent::Initialize()
{

}

void CMeasurableComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		
	}
	break;
	}
}
