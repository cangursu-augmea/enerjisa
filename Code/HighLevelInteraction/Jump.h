// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 9:25 AM 01/07/2019, Utku Guler
// Edited by Ali Mehmet Altundag on 11:54 19.01.2019

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntity.h>
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>
#include "Components/PlugSystem/PlugFemaleTerminal.h"
#include "Components/PlugSystem/PlugMaleTerminal.h"

class CJumpComponent
	: public CInteractionBaseComponent
{
public:
	CJumpComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CJumpComponent>& desc)
	{
		desc.SetGUID("{62EDFD36-53C3-4E34-9A7C-5963A9A47836}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("Jump");
		desc.SetDescription("Basic Jump functionality.");
		//desc.AddMember(&CJumpComponent::m_turnAxis, 'axis', "TurnAxis", "Turn Axis", "Turn Axis", FORWARD_DIRECTION);
		//desc.AddMember(&CJumpComponent::m_turnAxisOffset, 'axof', "TurnAxisOffset", "Turn Axis Offset", "Turn Axis Offset", ZERO);
		//desc.AddMember(&CJumpComponent::m_turnAmount, 'tamo', "TurnAmount", "Turn Amount", "Turn Amount", 90.f);
		//desc.AddMember(&CJumpComponent::m_turnSpeed, 'tspd', "TurnSpeed", "Turn Speed", "Turn Speed", 10.f);
		//desc.AddMember(&CJumpComponent::m_turnMeshComponent, 'tmsc', "TurnMeshComponent", "Turn Mesh Component", "Turn Mesh Component", false);
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::TURN; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual void ProcessEvent(const SEntityEvent& event) override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_START_LEVEL);
	}
	// ~IEntitycomponent

private:
	int jumpCount = 0;
	float axisY = 0.f;
	std::map<string, int> crocadileJumps {
	{ ENTITY_NAME_CROCODILE_FIRST.c_str(), 0 },
	{ ENTITY_NAME_CROCODILE_SECOND.c_str(), 0 },
	{ ENTITY_NAME_CROCODILE_THIRD.c_str(), 0 },
	{ ENTITY_NAME_CROCODILE_RED.c_str(), 0 } };

	std::map<string, int>::iterator mapIt;

};
