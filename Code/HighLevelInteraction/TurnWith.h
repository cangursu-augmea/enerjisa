// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 9:25 AM 01/07/2019, Utku Guler
// Edited by Ali Mehmet Altundag on 11:54 19.01.2019

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntity.h>
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>
#include "Components/Turn.h"
#include "Components/PlugSystem/PlugFemaleTerminal.h"
#include "../Components/AudioController.h"

#define		X_DIRECTION (Vec3(1.f, 0.f, 0.f))
#define		Y_DIRECTION (Vec3(0.f, 1.f, 0.f))
#define		Z_DIRECTION (Vec3(0.f, 0.f, 1.f))

class CTurnWithComponent
	: public CInteractionBaseComponent
{
public:
	CTurnWithComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CTurnWithComponent>& desc)
	{
		desc.SetGUID("{0146F8C9-BD1A-419F-848C-488752EF37B9}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("TurnWith");
		desc.SetDescription("Basic TurnWith functionality.");
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::TURN; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual void ProcessEvent(const SEntityEvent& event) override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{


		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_DONE)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_LAST);
	}
	// ~IEntitycomponent

private:

	IEntity *m_pStaka = nullptr, *m_pLoadToprak = nullptr, *m_pOutToprak = nullptr, *m_pLoadAyirici = nullptr, *m_pOutAyirici = nullptr;
	IEntity *m_pAnahtar = nullptr, *m_pKesiciAnahtarDeligi = nullptr, *m_pKesiciButton = nullptr;
	CTurnComponent *m_pKrikoTurnComponent = nullptr, *m_pAnahtarTurnComponent = nullptr;
	CPlugFemaleTerminal *m_pLoadToprakFemale = nullptr, *m_pKesiciAnahtarDeligiPlugComponent = nullptr, *m_pOutToprakFemale = nullptr; 
	CPlugFemaleTerminal *m_pLoadAyiriciFemale = nullptr, *m_pOutAyiriciFemale = nullptr;
	CPlugMaleTerminal *m_pAnahtarPlugComponent = nullptr;
	CToggleComponent *m_pKesiciButtonToggleComponent = nullptr;
	bool isTurned;
	Vec3 m_dir;
	CAudioController* m_pAudioController;
	bool kesiciSoundState;
	bool ayiriciLoadSoundState;
	bool ayiriciOutSoundState;
};