// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 9:25 AM 01/07/2019, Utku Guler
// Edited by Ali Mehmet Altundag on 11:54 19.01.2019

#include "StdAfx.h"
#include "Jump.h"
#include "Interaction/InteractionManagerComponent.h"

void CJumpComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CJumpComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CJumpComponent::Register)

CJumpComponent::CJumpComponent()
{
	
}

void CJumpComponent::Initialize()
{	

}

SInteractionFeedback CJumpComponent::InteractionTick()
{
	auto state = GetTrackerState();

	if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 1.f)
	{	
		mapIt = crocadileJumps.find(m_pEntity->GetName());
		mapIt->second++;
		
		if (mapIt->second > 2)
		{
			mapIt->second = 0;
			axisY = -0.66f;
		}
		else
		{
			CryLog("elsecheck"); 
			axisY = 0.33f;
		}
		
		Vec3 newOffSet = m_pEntity->GetPos() + Vec3(0.f, axisY, 0.f);
		m_pEntity->SetPos(newOffSet);
	}
	return m_feedback;
}

void CJumpComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_START_LEVEL:
	{
		
	}
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		

	}
	break;
	}
}
