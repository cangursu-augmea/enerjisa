// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 10:12 AM 01/24/2019, Utku Guler

#include "StdAfx.h"
#include "PensAmpermetre.h"
#include "Interaction/InteractionManagerComponent.h"
#include "HighLevelInteraction/Measurable.h"

#include <DefaultComponents/Geometry/StaticMeshComponent.h>
#include <sstream>
#include <string.h>

void CPensAmpermetreComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CPensAmpermetreComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CPensAmpermetreComponent::Register)


CPensAmpermetreComponent::CPensAmpermetreComponent()
	: m_offSet(0.f, 1.f, 0.f)
	, m_posOffSet(0.f, 0.f, 0.f)
	, m_lengt(0.025)
	, m_operate(true)
	, m_buffer(0)
	, m_stepLengt(0.01f)
	, m_CoolDown(30.f)
	, m_type(1)
	, m_triggerBuffer(false)
{

}

SInteractionFeedback CPensAmpermetreComponent::InteractionTick()
{
	auto state = GetTrackerState();

	if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 0)
	{
		m_triggerBuffer = false;
	}

	if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 1.f && m_triggerBuffer == false)
	{
		m_triggerBuffer = true;
		if (m_type < 3)
			m_type++;
		else
			m_type = 1;

		if(m_type == 1)
			m_pEntity->LoadGeometry(geometrySlot, "objects/equipments/pensampermetre_A.cgf");
		else if(m_type == 2)
			m_pEntity->LoadGeometry(geometrySlot, "objects/equipments/pensampermetre_V.cgf");
		else if (m_type == 3)
			m_pEntity->LoadGeometry(geometrySlot, "objects/equipments/pensampermetre_G.cgf");
	}

	return SInteractionFeedback();
}

void CPensAmpermetreComponent::Initialize()
{
	Detectable buffer;
	for (int i = 1; i <= 3; i++)
	{
		buffer.Object = std::string("TRAFO_U" + std::to_string(i));
		buffer.state = false;
		buffer.Timer = 0;
		m_OutPutList.push_back(buffer);
	}

	for (int i = 1; i <= 3; i++)
	{
		buffer.Object = std::string("TRAFO_D" + std::to_string(i));
		buffer.state = false;
		buffer.Timer = 0;
		m_OutPutList.push_back(buffer);
	}
	
}

void CPensAmpermetreComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	break;
	case ENTITY_EVENT_LEVEL_LOADED:
	{
		m_pensDisplay = gEnv->pEntitySystem->FindEntityByName("PENS_DISPLAY");
		m_pensDisplay->Hide(true);
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (m_operate != true) return;

		Vec3 m_Dir = m_pEntity->GetWorldRotation() * m_offSet;
		Vec3 Pos = m_pEntity->GetWorldPos();
		Pos = Pos + m_pEntity->GetWorldRotation() * m_posOffSet;
		
		if (m_buffer > 6.28 || m_buffer < -6.28)
			m_buffer = 0;
		m_buffer += m_stepLengt;
		Quat foo = foo.CreateRotationZ(m_buffer);
		Vec3 Final = (m_pEntity->GetWorldRotation() * foo)*m_offSet ;

		gEnv->pAuxGeomRenderer->DrawLine(Pos, Col_Red, Pos + ((Final).normalize() * m_lengt), Col_Red, 2);
		gEnv->pAuxGeomRenderer->DrawLine(Pos, Col_Blue, Pos + ((m_Dir).normalize() * m_lengt), Col_Blue, 2);
	

		const int maxHitCount = 16;
		int nHit = 0;
		ray_hit hits[maxHitCount];
		if ((nHit = gEnv->pPhysicalWorld->RayWorldIntersection(Pos, ((m_Dir).normalize() * m_lengt), ent_static | ent_sleeping_rigid | ent_rigid, rwi_stop_at_pierceable | rwi_any_hit, hits, maxHitCount, m_pEntity->GetPhysicalEntity())))
		{
			for (int i = 0; i < nHit; ++i)
			{
				IPhysicalEntity* pPhyEnt = hits[i].pCollider;
				if (pPhyEnt)
				{
					IEntity* pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pPhyEnt);
					if (pEnt)
					{
						std::string name(pEnt->GetName());
						
						IEntity* foundEntity = gEnv->pEntitySystem->FindEntityByName(name.c_str());
						if(CMeasurableComponent* p_measurable = foundEntity->GetComponent<CMeasurableComponent>() )
						{
							if (!(m_type == 1))
								return;
							int type = p_measurable->GetType();

							switch (type)
							{
							case 1:
							{
								Reset(NUMERICAL_GAUGE_1);
							}
								break;
							case 2:
							{
								Reset(NUMERICAL_GAUGE_15);
							}
							break;
							case 5:
							{
								Reset(NUMERICAL_GAUGE_0);
							}
							break;
							default:
								m_pensDisplay->Hide(true);
								break;
							}

							for (int i = 0; i < m_OutPutList.size(); i++)
							{
								if (m_OutPutList[i].Object == name && m_OutPutList[i].state == false)
								{
									m_OutPutList[i].state = true;
									m_OutPutList[i].Timer = m_CoolDown;
									/////EVENT SEND/////
									StateManager::Event event;
									event.objects = { name };
									event.action = GetInteractionId();
									event.result.bValue = true;
									CGamePlugin::GetStateManager().Insert(event);
									/////EVENT SEND/////
									CryLogAlways("PENSAMPERMETRE------------------------------ %s", name);
								}
							}
						}
						else
						{
							m_pensDisplay->Hide(true);
						}
					}
				}
			}
		}
	}
	break;
	}
}

void CPensAmpermetreComponent::Reset(const std::string meshPath)
{
	// unhide entity
	m_pensDisplay->Hide(false);

	m_pensDisplay->LoadGeometry(geometrySlot, meshPath.c_str());

}