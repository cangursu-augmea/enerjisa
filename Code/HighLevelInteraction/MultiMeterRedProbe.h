// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:02 aM 11/02/2019, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include "HighLevelInteraction/PensAmpermetre.h"
#include "Components/Toggle.h"
#include "BlackProbe.h"
#include "../Components/AudioController.h"

struct MultiMeterOutPut
{
	std::string Object;
	bool state;
	float Timer;
};

class CMultiMeterRedProbeComponent
	: public CInteractionBaseComponent

{
public:
	CMultiMeterRedProbeComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CMultiMeterRedProbeComponent>& desc)
	{
		desc.SetGUID("{FFE0C30D-1512-462E-82AF-6BE55A1C8A25}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("MultiMeterRedProbe");
		desc.SetDescription("For MultiMeterRedProbe object, you use this component.");
		//EDITOR MEMBERS
		desc.AddMember(&CMultiMeterRedProbeComponent::m_OffSet, 'mdir', "RaycastVec", "Way to Scan(-1 ... 1)", "Way to Scan", (0.f, 1.f, 0.f));
		desc.AddMember(&CMultiMeterRedProbeComponent::m_PosOffSet, 'mpof', "OffSetVec", "OffSet", "Way to Move", (0.f, 0.f, 0.f));
		desc.AddMember(&CMultiMeterRedProbeComponent::m_Lengt, 'mlen', "LengtofRay", "Lengt of Ray", "LengtofRay", 1.f);
		desc.AddMember(&CMultiMeterRedProbeComponent::m_CoolDown, 'cool', "DetectCoolDown", "CoolDown time beetween detects", "DetectCoolDown", 30.f);
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::AMPERMETRE; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_LEVEL_LOADED);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	void Reset(const std::string meshPath);

	//bool IsPairPlugged(int pairID);
	/*static std::string Cable(int i)
	{
		return ENTITY_NAME_CABLEPREFIX + std::to_string(i);
	}*/

private:
	Vec3 m_OffSet;
	Vec3 m_PosOffSet;
	float m_Lengt;
	bool m_operate;
	Quat offsetRotator;
	CToggleComponent* m_fuseToggle;
	CBlackProbeComponent* m_blackProbe;
	CPensAmpermetreComponent* m_pensampermetre;
	std::vector<MultiMeterOutPut> m_OutPutList;
	float m_CoolDown;

	CAudioController* m_audioController;

	const int geometrySlot = 0;
	IEntity* m_pensDisplay;

	std::string NUMERICAL_GAUGE_0V   = "objects/equipments/pensAmp_NumericalGauge_zero.cgf"; // for wrong evaluation
	std::string NUMERICAL_GAUGE_218V = "objects/equipments/pensAmp_NumericalGauge_218.cgf";
	std::string NUMERICAL_GAUGE_220V = "objects/equipments/pensAmp_NumericalGauge_220.cgf";
	std::string NUMERICAL_GAUGE_222V = "objects/equipments/pensAmp_NumericalGauge_222.cgf";
};