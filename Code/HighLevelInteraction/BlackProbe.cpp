// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:02 aM 11/02/2019, Utku Guler

#include "StdAfx.h"
#include "BlackProbe.h"
#include "Interaction/InteractionManagerComponent.h"
#include <sstream>

void CBlackProbeComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CBlackProbeComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CBlackProbeComponent::Register)

CBlackProbeComponent::CBlackProbeComponent()
	: m_OffSet(0.f, 0.f, 0.f)
	, m_Lengt(1.f)
	, m_operate(true)
	, m_PosOffSet(0.f, 0.f, 0.f)
	, m_active(false)
	, m_mesType(0)
	, m_mesName("")
{

}


SInteractionFeedback CBlackProbeComponent::InteractionTick()
{
	return SInteractionFeedback();
}
void CBlackProbeComponent::Initialize()
{

}

void CBlackProbeComponent::ProcessEvent(const SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_EDITOR_PROPERTY_CHANGED:
	{

	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (m_operate != true) return;
		//CToggleComponent* ToggleCom = m_pEntity->GetOrCreateComponent<CToggleComponent>();
		//if (ToggleCom->GetCurrentState() == false)return;
		Vec3 target = m_pEntity->GetWorldRotation() * m_OffSet;
		Vec3 m_Dir = target;
		Vec3 Pos = m_pEntity->GetWorldPos();
		Pos = Pos + m_pEntity->GetWorldRotation() * m_PosOffSet;

		gEnv->pAuxGeomRenderer->DrawLine(Pos, Col_Blue, Pos + ((m_Dir).normalize() * m_Lengt), Col_Blue, 2);

		//m_active mechanic = if BlackProbe touching a listed entity it s true if not touching or touching a non listed entity it s false   


		const int maxHitCount = 16;
		int nHit = 0;
		ray_hit hits[maxHitCount];
		if ((nHit = gEnv->pPhysicalWorld->RayWorldIntersection(Pos, ((m_Dir).normalize() * m_Lengt), ent_static | ent_sleeping_rigid | ent_rigid, rwi_stop_at_pierceable | rwi_any_hit, hits, maxHitCount, m_pEntity->GetPhysicalEntity())))
		{
			//CryLogAlways("hit");
			for (int i = 0; i < nHit; ++i)
			{
				IPhysicalEntity* pPhyEnt = hits[i].pCollider;
				if (pPhyEnt)
				{
					IEntity* pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pPhyEnt);
					if (pEnt)
					{
						std::string name(pEnt->GetName());
						
							if (CMeasurableComponent* Measurable = pEnt->GetComponent<CMeasurableComponent>())
							{
								if (Measurable->IsEnable() == false)
								{
									m_active = false;
									return;
								}
									
								m_active = true;
								m_mesType = Measurable->GetType();
								m_mesName = name;
							}
							else
								m_active = false;
						
					}
				}
			}
		}
		else
			m_active = false;
	}
	break;
	}
}
