// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 11:02 aM 11/02/2019, Utku Guler

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "CryEntitySystem/IEntity.h"
#include "Interaction/Interactor.h"
#include "Interaction/InteractionBaseComponent.h"
#include "Components/Toggle.h"
#include "BlackProbe.h"
#include "../Components/AudioController.h"

struct OutPut
{
	std::string Object;
	bool state;
	float Timer;
};

class CRedProbeComponent
	: public CInteractionBaseComponent

{
public:
	CRedProbeComponent();
	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CRedProbeComponent>& desc)
	{
		desc.SetGUID("{60DCC12C-D3FA-4217-96A5-03429C4F8F7E}"_cry_guid);
		desc.AddBase<CInteractionBaseComponent>();
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("RedProbe");
		desc.SetDescription("For RedProbe object, you use this component.");
		//EDITOR MEMBERS
		desc.AddMember(&CRedProbeComponent::m_OffSet, 'mdir', "RaycastVec", "Way to Scan(-1 ... 1)", "Way to Scan", (0.f, 1.f, 0.f));
		desc.AddMember(&CRedProbeComponent::m_PosOffSet, 'mpof', "OffSetVec", "OffSet", "Way to Move", (0.f, 0.f, 0.f));
		desc.AddMember(&CRedProbeComponent::m_Lengt, 'mlen', "LengtofRay", "Lengt of Ray", "LengtofRay", 1.f);
		desc.AddMember(&CRedProbeComponent::m_CoolDown, 'cool', "DetectCoolDown", "CoolDown time beetween detects", "DetectCoolDown", 30.f);
	}

	// CInteractionBaseComponent
	virtual SInteractionFeedback InteractionTick() override;
	virtual TInteractionComponentId GetInteractionId() const override { return EInteractionComponenetIDs::MULTIMETER; }
	// ~CInteractionBaseComponent

	// IEntityComponent
	virtual void Initialize() override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const override
	{
		return ENTITY_EVENT_BIT(ENTITY_EVENT_EDITOR_PROPERTY_CHANGED)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE)
			| ENTITY_EVENT_BIT(ENTITY_EVENT_LEVEL_LOADED);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	void TurnOnLight(const char* lightName, bool active);
	bool IsPairPlugged(int pairID);
	static std::string Cable(int i)
	{
		return ENTITY_NAME_CABLEPREFIX + std::to_string(i);
	}

private:
	Vec3 m_OffSet;
	Vec3 m_PosOffSet;
	float m_Lengt;
	bool m_operate;
	Quat offsetRotator;
	CToggleComponent* m_fuseToggle;
	CBlackProbeComponent* m_blackProbe;
	std::vector<OutPut> m_OutPutList;
	float m_CoolDown;
	
	CAudioController* m_audioController;

};