// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 28.01.2019, Alper �ekerci

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include "Components\Carry.h"
#include "HighLevelInteraction/Measurable.h"
#include "Components\PlugSystem\PlugFemaleTerminal.h"
#include "Components\Toggle.h"

class CSayac final : public IEntityComponent
{
private:
	CCarryComponent * m_pCarry = nullptr;
	std::vector<CCarryComponent*> cables = {};
	std::vector<CPlugFemaleTerminal*> klemensList = {};
	bool m_screwsInitialized = false;
	int m_cablecount;
	CCarryComponent* m_pKlemensCoverCarry = nullptr;
	CToggleComponent* m_pKlemensCoverToggle = nullptr;
	CToggleComponent* m_pPanoToggle = nullptr;
	CCarryComponent* m_pPanoCarry = nullptr;

	bool ShouldDisable();
	std::string GetCableName(int id);
	std::string GetKlemensName(int id);
	std::string GetTrafoUstName(int id);
	std::string GetTrafoAltName(int id);
	void DisableCollisions();
	void DisableCables();
	void DisableKlemensCover();
	void DisablePano();


public:
	CSayac() = default;
	~CSayac() = default;

	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(const SEntityEvent& event) override;

	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CSayac>& desc)
	{
		desc.SetGUID("{A04EA6E1-FD2E-4306-B677-9A525032010F}"_cry_guid);
		desc.SetEditorCategory("AUGMEA");
		desc.SetLabel("Sayac Component");
	}
};