#pragma once

#include "Interaction/Interaction.h"

enum EHighLevelInteractionIDs : TInteractionComponentId
{
	SCREW = EInteractionComponenetIDs::END_OF_DEFAULT_INT_COMP + 1,
	SCREWDRIVER,

	END_OF_DEFAULT_HIGH_LEVEL_INT_COMP
};