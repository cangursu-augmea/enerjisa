// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 2:48 PM 3/12/2019, Tugba Karadeniz

#include "StdAfx.h"
#include "Monitor.h"
#include "Interaction/InteractionManagerComponent.h"
#include "Components/PlugSystem/PlugMaleTerminal.h"
#include <DefaultComponents/Geometry/StaticMeshComponent.h>
#include <sstream>

void CMonitorComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CMonitorComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CMonitorComponent::Register)

CMonitorComponent::CMonitorComponent()
	: m_triggerBuffer(false)
	, m_lifeTime(0)
	, m_IsActive(false)
{

}

void CMonitorComponent::Initialize()
{
	CInteractionManagerComponent* pInteraction = CInteractionManagerComponent::AddOnEntity(m_pEntity);
	CRY_ASSERT_MESSAGE(pInteraction != nullptr, "Sayac Toggle component needs to have interaction component attached to the entity.");

}

SInteractionFeedback CMonitorComponent::InteractionTick()
{

	CPlugMaleTerminal* maleTerminal = m_pEntity->GetComponent<CPlugMaleTerminal>();
	if (maleTerminal->IsConnected() == false)
	{
		m_monitorDisplay->Hide(true);
	}
	else
	{
		auto state = GetTrackerState();
		if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 0)
		{
			m_triggerBuffer = false;
		}

		if (state.GetButtonState(eKI_Motion_OpenVR_TouchPadBtn) == 1.f && m_triggerBuffer == false)
		{
			if (!strcmp(m_pEntity->GetName(), "YENI_SAYAC")) {
				m_monitorDisplay = gEnv->pEntitySystem->FindEntityByName("MONITOR_DISPLAY_NEW");
			}
			m_triggerBuffer = true;
			Revive();
		}
	}

	return SInteractionFeedback();
}

void CMonitorComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_LEVEL_LOADED:
	{
		if (m_monitorDisplay = gEnv->pEntitySystem->FindEntityByName("MONITOR_DISPLAY"))
			m_monitorDisplay->Hide(true);
		else
			CryLogAlways("NOT FINDING MONITOR_DISPLAY ENTITY........");
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
	
		float deltaTime = gEnv->pTimer->GetFrameTime();
		
		if (m_lifeTime > 0)
			m_lifeTime -= deltaTime;

		if (m_lifeTime <= 0 && m_IsActive == true)
		{
			/////EVENT SEND/////
			StateManager::Event event;
			event.objects = { m_pEntity->GetName() };
			event.action = GetInteractionId();
			event.result.bValue = false;
			CGamePlugin::GetStateManager().Insert(event);
			/////EVENT SEND/////
			m_IsActive = false;
			m_lifeTime = 0;
		}

	}
	break;
	case ENTITY_EVENT_RESET:
	case ENTITY_EVENT_START_GAME:
	case ENTITY_EVENT_START_LEVEL:
	{
		SetMonitorMeshNames();
	
	}
	break;
	}
}

void CMonitorComponent::Revive()
{	
	if (!strcmp(gEnv->pConsole->GetCVar("aug_monitor_state")->GetString(), "INCORRECT"))
	{
		static int count = 0;
		if (count == 0) {
			Reset("objects/equipments/powerMeter_numGauge_wrong_V.cgf");
		}
		else {
			Reset("objects/equipments/powerMeter_numGauge_wrong_I.cgf");
		}
		++count;

		if (count > 1) {
			count = 0;
			/////EVENT SEND/////
			StateManager::Event event;
			event.objects = { m_pEntity->GetName() };
			event.action = EInteractionComponenetIDs::MONITOR_LAST;
			event.result.bValue = true;
			CGamePlugin::GetStateManager().Insert(event);
			/////EVENT SEND/////
			/////EVENT SEND for cable check process
			event.action = GetInteractionId();
			CGamePlugin::GetStateManager().Insert(event);
			/////EVENT SEND/////
		}

		return;
	}

	if (m_meshNames.size() > 0) {
		
		Reset(m_meshNames[m_index]);
		++m_index;
		
		/////EVENT SEND/////
		StateManager::Event event;
		event.objects = { m_pEntity->GetName()};
		event.action = GetInteractionId();
		event.result.bValue = true;
		CGamePlugin::GetStateManager().Insert(event);
		/////EVENT SEND/////

		m_IsActive = true;
		m_lifeTime = 1;

		if (m_index == m_meshNames.size())
		{
			m_index = 0;
			/////EVENT SEND/////
			StateManager::Event event;
			event.objects = { m_pEntity->GetName() };
			event.action = EInteractionComponenetIDs::MONITOR_LAST;
			event.result.bValue = true;
			CGamePlugin::GetStateManager().Insert(event);
			/////EVENT SEND/////
		}
			
	}
	else {
		CryLogAlways("NOT FINDING ANY MESHES FOR DISPLAY ON MONITOR...");
	}
	
}

void CMonitorComponent::Reset(const std::string meshPath)
{
	// unhide entity
	m_monitorDisplay->Hide(false);
	m_monitorDisplay->LoadGeometry(geometrySlot, meshPath.c_str());
}

void CMonitorComponent::SetMonitorMeshNames()
{
	m_meshNames.clear();
	m_monitorDisplay->Hide(true);
	m_index = 0;

	m_meshNames.push_back("objects/equipments/powerMeter_numGauge_220.cgf"); // V1
	m_meshNames.push_back("objects/equipments/powerMeter_numGauge_218.cgf"); // V2
	m_meshNames.push_back("objects/equipments/powerMeter_numGauge_220v3.cgf"); //V3

	m_meshNames.push_back("objects/equipments/powerMeter_numGauge_1_16.cgf"); // I1
	m_meshNames.push_back("objects/equipments/powerMeter_numGauge_1_21.cgf"); // I2
	m_meshNames.push_back("objects/equipments/powerMeter_numGauge_1_18.cgf "); // I3
}
