// Copyright 2019 Augmea Inc. All rights reserved.
// Created: 12:08 28.01.2019, Ali Mehmet Altundag
// & Alper Sekerci

#pragma once

namespace AugUtils
{
	/* ignores collisions between given two entities.
	NOTE: user is responsible for when to call this func!
	There might not be gEnv set!!!*/
	void IgnoreCollisionBetween(const char* sEntA, const char* sEntB);
}