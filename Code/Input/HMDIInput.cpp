// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 16.02.2018, Orhan Aksoy
#include "StdAfx.h"
#include "Input\HMDIInput.h"
#include "HMDIInput.h"

IHmdController * CHMDIInput::getHmdController()
{
	return m_pHMDController;
}

IHmdDevice * CHMDIInput::getHmdDevice()
{
	return m_pHMDDevice;
}

CHMDIInput::CHMDIInput() {
	m_pHMDManager = gEnv->pSystem->GetHmdManager();
	CRY_ASSERT(m_pHMDManager);
	// and get hmd device
	if (m_pHMDManager && !m_pHMDDevice) 
		m_pHMDDevice = m_pHMDManager->GetHmdDevice();
	CRY_ASSERT(m_pHMDDevice);

	// ..and controllers...
	if (m_pHMDManager && m_pHMDDevice) m_pHMDController = const_cast<IHmdController*>(m_pHMDDevice->GetController());
	CRY_ASSERT(m_pHMDController);

}