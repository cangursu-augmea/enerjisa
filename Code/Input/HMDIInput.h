// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 16.02.2018, Orhan Aksoy

#pragma once

#include <CrySystem/VR/IHMDManager.h>

class CHMDIInput {
public:
	static CHMDIInput &CHMDIInput::getInstance() {
		static CHMDIInput instance;
		return instance;
	}
	IHmdController *getHmdController();
	IHmdDevice *getHmdDevice();

private:
	CHMDIInput();

	IHmdManager* m_pHMDManager = nullptr;
	IHmdDevice* m_pHMDDevice = nullptr;
	IHmdController* m_pHMDController = nullptr;
};