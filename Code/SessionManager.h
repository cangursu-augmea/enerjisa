// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 26.12.2018, Alper �ekerci

#pragma once

#include "GamePlugin.h"

class FlowState
{
	using FlowFunc = std::function<bool()>;
	using FlowAction = std::function<void()>;
	using FuncStatePair = std::pair<FlowFunc, FlowState*>;
	using FlowList = std::list<FuncStatePair>;

private:
	FlowList flows;
	FlowAction action;
	int m_id;

public:
	static int count;

	FlowState() : flows(FlowList{}), action(nullptr), m_id(-1) { }
	~FlowState() = default;

	FlowState& flow()
	{
		for (auto& pair : flows)
		{
			if (pair.first())
			{
				(*pair.second).act();
				// return (*pair.second).flow(); // Flow as long as it can.
				return (*pair.second); // Flow 1 state per frame.
			}
		}

		return *this;
	}

	FlowState& next(FlowFunc func)
	{
		auto state = new FlowState{};
		flows.push_back(FuncStatePair{ func, state });
		return *(flows.back().second);
	}

	FlowState& next() // Immediately flows to the next state.
	{
		return next([]() { return true; });
	}

	FlowState& next(FlowFunc func, FlowState* state_pointer)
	{
		flows.push_back(FuncStatePair{ func, state_pointer });
		return *(flows.back().second);
	}

	FlowState& next(FlowState* state_pointer)
	{
		return next([]() { return true; }, state_pointer);
	}

	FlowState& act(FlowAction action) // What happens when transiting into this state.
	{
		this->action = action;
		return *this;
	}

	void act() // Perform the action.
	{
		if (action)
			action();
	}

	FlowState& id(int id)
	{
		m_id = id;
		return *this;
	}

	int id()
	{
		return m_id;
	}

	static FlowState& create()
	{
		auto state = new FlowState{};
		return *state;
	}
};

struct FlowStartEnd
{
	FlowState* start;
	FlowState* end;
};

class CSessionManager : public IGameFrameworkListener
{
private:
	FlowState * m_pState;
	FlowState* m_pStart;

	bool ok = false;
	bool yes = false;
	bool no = false;

	bool failed = false;
	bool m_explodeFlag = false;
	Vec3 m_explodePos;
	bool m_fazOnNotr;
	bool m_IsTherePole;
	bool m_transformerState;
	bool m_monitorState;
	bool m_transformerOrder;

public:
	CSessionManager();
	virtual ~CSessionManager() = default;

	bool Init();
	void Reset();

	void SetOK(bool ok) { this->ok = ok; }
	bool IsOK() { return ok; }

	void SetYes(bool yes) { this->yes = yes; }
	bool IsYes() { return yes; }

	void SetNo(bool no) { this->no = no; }
	bool IsNo() { return no; }

	// Flow Chart Pieces
	FlowStartEnd CreateTutorial();
	FlowStartEnd CreateTrifazeUntilOpDecision(); // Kesme? Acma? Degistirme?
	FlowStartEnd CreateSayacDegisim();
	FlowStartEnd CreateSayacAcma();
	FlowStartEnd CreateSayacKesme();

	FlowStartEnd CreateCableTest();

	FlowStartEnd CreateX5UntilOpDecision();
	FlowStartEnd CreateX5SayacDegisim();
	FlowStartEnd CreateX5SayacAcma();
	FlowStartEnd CreateX5SayacKesme();
	FlowStartEnd CreateX5Kontrol(bool forOpenOperation);
	
	FlowStartEnd CreateHighVolUntilOpDecision();
	FlowStartEnd HighVolDegistirme();
	FlowStartEnd HighVolKesme();
	FlowStartEnd HighVolAcma();
	FlowStartEnd HighVolTTRKontrol();
	// ~Flow Chart Pieces

	bool CheckFailure();

	auto SetFailed(bool state)
	{
		return [=]()
		{
			// set failed
			this->m_explodeFlag = state;
		};
	}

	void Tick() // Updates the state machine.
	{
		if (!CheckFailure())
			m_pState = &(*m_pState).flow();
	}

	FlowState& GetState() { return *m_pState; }

	// IGameFrameworkListener
	virtual void OnPostUpdate(float fDeltaTime) override { Tick(); };
	virtual void OnSaveGame(ISaveGame* pSaveGame) override {  };
	virtual void OnLoadGame(ILoadGame* pLoadGame) override {  };
	virtual void OnLevelEnd(const char* nextLevel) override { };
	virtual void OnActionEvent(const SActionEvent& event) override {  };
	virtual void OnPreRender() { Tick(); }
	// ~IGameFrameworkListener
};