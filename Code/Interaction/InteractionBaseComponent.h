// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 15:18 26.10.2018, Ali Mehmet Altundag
// Designed with Orhan Aksoy

#pragma once

#include "Interaction.h"
#include <CryEntitySystem/IEntityComponent.h>
#include "Interactor.h"

struct CInteractionBaseComponent
	: public IEntityComponent
	, public CInteractor
{
	static void ReflectType(Schematyc::CTypeDesc<CInteractionBaseComponent>& desc)
	{
		desc.SetGUID(CInteractionBaseComponent::sInteractionCompGUID);
	}

	/* nothing more than an agreement between interaction
	componenets and interaction manager. must be called
	by the manager */
	virtual SInteractionFeedback InteractionTick() = 0;

	static CryGUID& GetInteractionBaseComponentGUID()
	{
		return CInteractionBaseComponent::sInteractionCompGUID;
	}

private:
	static CryGUID sInteractionCompGUID;
};