// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 15:18 26.10.2018, Ali Mehmet Altundag
// Designed with Orhan Aksoy

#include "StdAfx.h"
#include "InteractionBaseComponent.h"

CryGUID CInteractionBaseComponent::sInteractionCompGUID = "{D177E305-E0AD-4767-820D-3E8CF38F48FF}"_cry_guid;