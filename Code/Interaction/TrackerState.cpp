// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 13:33 19.09.2018, Ali Mehmet Altundag
// Designed by Orhan Aksoy

#include "StdAfx.h"
#include "TrackerState.h"

bool CTrackerState::UpdateFromController(const EHmdController controller, const Vec3 &playerOrigin)
{
	auto *pController = CHMDIInput::getInstance().getHmdController();
	if (!pController) return false;

	auto controllerOffset = pController->GetLocalTrackingState(controller).pose.position;

	m_transform.SetTranslation({ playerOrigin.x + controllerOffset.x, playerOrigin.y + controllerOffset.y, playerOrigin.z + controllerOffset.z });
	m_transform.SetRotation(CryTransform::CRotation(pController->GetLocalTrackingState(controller).pose.orientation));

	m_linearVelocity = pController->GetLocalTrackingState(controller).pose.linearVelocity;
	m_angularVelocity = pController->GetLocalTrackingState(controller).pose.angularVelocity;
	m_thumbStickValue = pController->GetThumbStickValue(controller, eKI_Motion_OpenVR_TouchPad_X);

	m_buttonStates[eKI_Motion_OpenVR_Grip] = pController->IsButtonPressed(controller, eKI_Motion_OpenVR_Grip);
	m_buttonStates[eKI_Motion_OpenVR_TriggerBtn] = pController->IsButtonPressed(controller, eKI_Motion_OpenVR_TriggerBtn);
	m_buttonStates[eKI_Motion_OpenVR_Trigger] = pController->IsButtonPressed(controller, eKI_Motion_OpenVR_Trigger);
	m_buttonStates[eKI_Motion_OpenVR_TouchPadBtn] = pController->IsButtonPressed(controller, eKI_Motion_OpenVR_TouchPadBtn);
		
	return true;
}

float CTrackerState::GetButtonState(EKeyId button) const
{
	const auto it = m_buttonStates.find(button);
	if (m_buttonStates.end() != it) return it->second;
	return 0.f;
}