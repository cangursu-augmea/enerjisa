// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 15.02.2018, Ali Mehmet Altundag, Orhan Aksoy

#include "StdAfx.h"
#include "InteractionManagerComponent.h"
#include "Player/PlayerHand.h"
#include "Interaction/InteractionBaseComponent.h"


void CInteractionManagerComponent::Register(Schematyc::IEnvRegistrar & registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CInteractionManagerComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&CInteractionManagerComponent::Register)

CInteractionManagerComponent::CInteractionManagerComponent()
	: m_pInteractionSource(nullptr)
	, m_isInteractionLocked(false)
	, m_isAllInteractionEnd(false)
{
}

CInteractionManagerComponent::~CInteractionManagerComponent()
{
}

CInteractionManagerComponent *CInteractionManagerComponent::AddOnEntity(IEntity * pEntity)
{
	CInteractionManagerComponent *pComponent = nullptr;
	if ((pComponent = pEntity->GetOrCreateComponent<CInteractionManagerComponent>()) == nullptr) {
		CryWarning(VALIDATOR_MODULE_ASSETS, VALIDATOR_WARNING, "CCInteractionComponent::InstantiateOnEntity: Cannot create CInteractionComponent");
	}
	return pComponent;
}

void CInteractionManagerComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
		Tick();
	default: break;
	}
}

bool CInteractionManagerComponent::SetInteractionSource(IInteractionSource* pSource)
{
	// we already have a source!...
	if (pSource && m_pInteractionSource) return false;

	// TODO: send last signal here!
	m_pInteractionSource = pSource;
	return true;
}

void CInteractionManagerComponent::Tick()
{
	// nothing to do...
	if (!m_pInteractionSource) return;

	// clear map
	m_feedbackMap.clear();

	m_isInteractionLocked = false;
	m_isAllInteractionEnd = true;
	DynArray<IEntityComponent*> componenets;
	m_pEntity->GetComponents(componenets);
	for (int i = 0; i < componenets.size(); ++i)
	{
		IEntityComponent* pComp = componenets[i];
		auto bases = pComp->GetClassDesc().GetBases();

		bool isInteractionComp = false;
		for (int j = 0; j < bases.size(); ++j)
		{
			if (CInteractionBaseComponent::GetInteractionBaseComponentGUID() == bases.at(j).GetTypeDesc().GetGUID())
			{
				// ok, it has InteractionBaseComponent, so we found it
				isInteractionComp = true;
				break;
			}
		}
		// it s not interaction comp, so continue...
		if (!isInteractionComp) continue;

		CInteractionBaseComponent* pIntComp = reinterpret_cast<CInteractionBaseComponent*>(pComp);

		if (!pIntComp->IsEnabled()) continue;

		// TODO: I dont like the following line; think about it
		if(m_pInteractionSource) pIntComp->UpdateTrackingState(m_pInteractionSource->GetTrackerState());
		auto feedback = pIntComp->InteractionTick();
		m_feedbackMap[pIntComp->GetInteractionId()] = feedback;

		if (EInteractionFeedback::INTERACTION_LOCK == feedback.feedback) m_isInteractionLocked = true;
		if (EInteractionPhase::ONGOING == feedback.phase) m_isAllInteractionEnd = false;
	}
}