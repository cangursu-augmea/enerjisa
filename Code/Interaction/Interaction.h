// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 14:36 3.09.2018, Ali Mehmet Altundag
// Designed with Orhan Aksoy

#pragma once


#include <string>
#include <map>
#include "TrackerState.h"
#include <CrySensorSystem/Interface/ISensorMap.h>

//////////////// INTERACTION PHASE ////////////////
enum class EInteractionPhase
{
	// interaction ignition event
	FIRST,

	// sent during the interaction
	ONGOING,

	// sent when interaction ends
	LAST,
};

//////////////// INTERACTION FEEDBACK ////////////////
// register your interaction componenet feedback if required
enum class EInteractionFeedback
{
	// common "nothing to do feedback"
	NONE,

	// a positive meaning feedback whatever done by the IC
	SUCCESS,

	// lock the interaction so that the source cannot interact
	// with another dest.
	INTERACTION_LOCK
};

// must be unique for each int. comp. type...
typedef uint32_t TInteractionComponentId;

//////////////// DEFAULT INTERACTION COMPONENT IDs ////////////////
enum EInteractionComponenetIDs : TInteractionComponentId
{
	UNKNOWN,
	CARRY,
	TOGGLE,
	TURN,
	WEAR,
	TOUCH,
	HIGHLIGHT,
	PLUG,
	CAMERA,
	MULTIMETER,
	MONITOR,
	MONITOR_LAST,
	PENSAMPERMETRE,
	AMPERMETRE,

	END_OF_DEFAULT_INT_COMP
};

struct SInteractionFeedback
{
	SInteractionFeedback()
		: feedback(EInteractionFeedback::NONE)
		, phase(EInteractionPhase::LAST)
	{}

	EInteractionPhase phase;
	EInteractionFeedback feedback;
};

// TInteractionComponentId <-> SInteractionFeedback map
typedef std::map<TInteractionComponentId, SInteractionFeedback> TInteractionFeedbackMap;