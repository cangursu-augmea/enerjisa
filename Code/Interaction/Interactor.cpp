// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 13:33 19.09.2018, Ali Mehmet Altundag
// Designed with Orhan Aksoy

#include "StdAfx.h"
#include "Interactor.h"
#include "InteractionManagerComponent.h"
#include <limits>
#include <CrySensorSystem/Interface/ICrySensorSystemPlugin.h>
#include <CryRenderer/IRenderAuxGeom.h>

using namespace Cry::SensorSystem;

CInteractor::CInteractor()
	: m_radius(0.f)
	, m_relativePos(ZERO)
	, m_pSensorMap(nullptr)
	, m_pSensorSystem(nullptr)
	, m_pCurrentInteractionEntity(nullptr)
	, m_isEnabled(true)
	, m_isHidden(false)
{
	Init();
}

void CInteractor::Init()
{
	// sensor sys related init
	m_sensorVolumeQueryResult.reserve(128);
	auto* pPluginManager = GetISystem()->GetIPluginManager();
	auto *pSensorSystemPlugin = pPluginManager->QueryPlugin<ICrySensorSystemPlugin>();
	CRY_ASSERT_MESSAGE(pSensorSystemPlugin != nullptr, "CPlayerHand::CPlayerHand: sensor system plugin not found.");
	m_pSensorSystem = &(pSensorSystemPlugin->GetSensorSystem());
	m_pSensorMap = &(m_pSensorSystem->GetMap());
}

void CInteractor::SetRadius(const float r)
{
	m_radius = r;

}

void CInteractor::SetRelativePos(const Vec3& pos)
{
	m_relativePos = pos;
}

void CInteractor::UpdateTrackingState(const CTrackerState& state)
{
	m_trackerState = state;

	if (m_pCurrentInteractionEntity && m_pCurrentInteractionEntity->IsInteractionLocked())
		return;
	
	if (m_pCurrentInteractionEntity && m_pCurrentInteractionEntity->IsAllInteractionEnd())
	{
		m_pCurrentInteractionEntity->SetInteractionSource(nullptr);
		m_pCurrentInteractionEntity = nullptr;
	}
	
	IEntity* pEnt = GetInteractionEntity();
	if (!pEnt) return;

	// if we already have an interaction object, inform him...
	if (m_pCurrentInteractionEntity) m_pCurrentInteractionEntity->SetInteractionSource(nullptr);

	m_pCurrentInteractionEntity = pEnt->GetComponent<CInteractionManagerComponent>();
	m_pCurrentInteractionEntity->SetInteractionSource(this);
}

void CInteractor::UpdateIntersectionVector()
{
	//if (CGamePlugin::IsInteractionDebugEnabled())
	//{
	//	SSensorVolumeParams sensorHitParams;
	//	const ::SensorTags tag = m_pSensorSystem->GetTagLibrary().GetTag("Trigger");
	//	Cry::SensorSystem::SensorVolumeIdSet qResSet;
	//	m_pSensorMap->Query(qResSet, CSensorBounds(Sphere(Vec3(ZERO), 1000000.0f)), tag);
	//	for (auto it : qResSet)
	//	{
	//		sensorHitParams = m_pSensorMap->GetVolumeParams(it);
	//		//IEntity* pEnt = gEnv->pEntitySystem->GetEntity(sensorHitParams.entityId);
	//		if (Cry::SensorSystem::ESensorShape::AABB == sensorHitParams.bounds.GetShape())
	//		{
	//			const AABB aabb = sensorHitParams.bounds.AsAABB();
	//			IRenderAuxGeom::GetAux()->DrawAABB(aabb, true, Col_Red, EBoundingBoxDrawStyle::eBBD_Faceted);
	//			//gEnv->pRenderer->GetIRenderAuxGeom()->DrawAABB(aabb, true, Col_Red, EBoundingBoxDrawStyle::eBBD_Faceted);
	//		}
	//		else if (Cry::SensorSystem::ESensorShape::OBB == sensorHitParams.bounds.GetShape())
	//		{
	//			const OBB obb = sensorHitParams.bounds.AsOBB();
	//			IRenderAuxGeom::GetAux()->DrawOBB(obb, obb.m33, true, Col_Red, EBoundingBoxDrawStyle::eBBD_Faceted);
	//			//gEnv->pRenderer->GetIRenderAuxGeom()->DrawOBB(obb, pEnt->GetWorldTM(), true, Col_Red, EBoundingBoxDrawStyle::eBBD_Faceted);
	//		}
	//	}
	//}

	m_intersectedObjects.clear();
	const Vec3 sourceWorldPos = GetWorldPosition();
	const float sourceRadius = m_radius;
	const ::SensorTags triggerTag = m_pSensorSystem->GetTagLibrary().GetTag("Trigger");
	m_sensorVolumeQueryResult.clear();
	m_pSensorMap->Query(m_sensorVolumeQueryResult, CSensorBounds(Sphere(sourceWorldPos, sourceRadius)), triggerTag);

	static SSensorVolumeParams hitParams;
	for (auto it : m_sensorVolumeQueryResult)
	{
		hitParams = m_pSensorMap->GetVolumeParams(it);
		IEntity* pEntity = gEnv->pEntitySystem->GetEntity(hitParams.entityId);

		Vec3 epos = pEntity->GetWorldPos();
		float distance = epos.GetDistance(sourceWorldPos);

		// TODO: implement hysteresis
		m_intersectedObjects.push_back(pEntity);
	}
}

IEntity* CInteractor::GetInteractionEntity()
{
	if (m_pCurrentInteractionEntity) return m_pCurrentInteractionEntity->GetEntity();

	if (CGamePlugin::IsInteractionDebugEnabled())
	{
		//gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(GetWorldPosition(), GetRadius(), Col_Blue);
		IRenderAuxGeom::GetAux()->DrawSphere(GetWorldPosition(), GetRadius(), Col_Blue);
	}

	UpdateIntersectionVector();

	// find closest entity to interact
	float closestDist = std::numeric_limits<float>::max();
	IEntity* pEnt = nullptr;
	for (const auto it : m_intersectedObjects)
	{
		float dist = it->GetWorldPos().GetDistance(GetWorldPosition());
		if (dist < closestDist)
		{
			closestDist = dist;
			pEnt = it;
		}
	}

	return pEnt;
}

bool CInteractor::DoWeIntersect(const IEntity* pEnt)
{
	if (pEnt == m_pCurrentInteractionEntity->GetEntity()) return true;

	UpdateIntersectionVector();
	auto it = std::find(m_intersectedObjects.begin(), m_intersectedObjects.end(), pEnt);
	if (m_intersectedObjects.end() == it) return false;
	return true;
}