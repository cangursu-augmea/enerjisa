// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 15.02.2018, Ali Mehmet Altundag and Orhan Aksoy

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include "Player/PlayerHand.h"
#include "Interaction.h"

class CInteractionManagerComponent
	: public IEntityComponent
{
public:
	CInteractionManagerComponent();
	~CInteractionManagerComponent();
	static CInteractionManagerComponent *AddOnEntity(IEntity *pEntity);

	static void Register(Schematyc::IEnvRegistrar& registrar);
	static void ReflectType(Schematyc::CTypeDesc<CInteractionManagerComponent>& desc)
	{
		desc.SetGUID("{CE0180A8-C27C-4F17-996C-9F04C724A1FC}"_cry_guid);
		desc.SetEditorCategory("Augmea Interaction");
		desc.SetLabel("InteractionComponent");
		desc.SetDescription("Interaction manager component.");
	}

	virtual void ProcessEvent(const SEntityEvent& event) override;
	virtual Cry::Entity::EntityEventMask GetEventMask() const { return ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE); }

	/* returns true if IM says OK for this interaction request */
	bool SetInteractionSource(IInteractionSource* pSource);

	/* returns interaction source */
	IInteractionSource* GetInteractionSource() { return m_pInteractionSource; }

	const TInteractionFeedbackMap& GetInteractionFeedbackMap() const { return m_feedbackMap; }

	bool IsInteractionLocked() const { return m_isInteractionLocked; }

	bool IsAllInteractionEnd() const { return m_isAllInteractionEnd; }

private:
	void Tick();

	bool m_isInteractionLocked;
	bool m_isAllInteractionEnd;
	IInteractionSource* m_pInteractionSource{ nullptr };
	TInteractionFeedbackMap m_feedbackMap;
};