// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 14:56 22.10.2018, Ali Mehmet Altundag

#pragma once

#include "Interaction.h"

////////////////   ////////////////
struct IInteractionSource
{
	/* returns tracker's state */
	virtual const CTrackerState& GetTrackerState() const = 0;

	/* returns relative world position depending on parent */
	virtual const Vec3 GetWorldPosition() const = 0;

	/* returns true if asking entity is interacting with its parent */
	virtual bool DoWeIntersect(const IEntity* pEnt) = 0;

	/* returns interaction radius */
	virtual float GetRadius() const = 0;

	/* breakes interaction state */
	//virtual void ReleaseInteraction() = 0;

	/* returns most current interaction feedback */
	virtual const SInteractionFeedback& GetInteractionFeedBack() const = 0;

	/* returns interaction component type id so that interaction man. can map feedbacks*/
	virtual TInteractionComponentId GetInteractionId() const { return EInteractionComponenetIDs::UNKNOWN; }

	/* hides interaction source entity if possible */
	virtual void Hide(bool isHidden) = 0;

	/* returns hide flag */
	virtual bool IsHidden() = 0;
};