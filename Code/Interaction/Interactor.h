// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 13:33 19.09.2018, Ali Mehmet Altundag
// Designed with Orhan Aksoy

#pragma once

/*
TODO: implement hysteresis.
*/

#include "StdAfx.h"
#include "InteractionSource.h"
#include <CrySensorSystem/Interface/ISensorMap.h>

class CInteractionManagerComponent;

class CInteractor
	: public IInteractionSource
{
public:
	CInteractor();

	/* initializes interaction-object finding systems; sensor sys. */
	void Init();

	/* sets radius of search sphere*/
	void SetRadius(const float r);

	/* sets relative position */
	void SetRelativePos(const Vec3& pos);

	/* sets controllers state */
	void UpdateTrackingState(const CTrackerState& state);

	// IInteractionSource
	virtual const CTrackerState& GetTrackerState() const override { return m_trackerState; }
	virtual const Vec3 GetWorldPosition() const override { return m_trackerState.GetPosition() + m_trackerState.GetRotation() * m_relativePos; }
	virtual bool DoWeIntersect(const IEntity* pEnt) override;
	virtual float GetRadius() const override { return m_radius; }
	virtual const SInteractionFeedback& GetInteractionFeedBack() const { return m_feedback; }
	// ~IInteractionSource

	void SetEnabled(bool enable = true) { m_isEnabled = enable; };

	bool IsEnabled() const { return m_isEnabled; }

	void Hide(bool isHidden) { m_isHidden = isHidden; }
	bool IsHidden() { return m_isHidden; }

protected:
	/* returns objects to interact (for internal use) */
	void UpdateIntersectionVector();

	/* returns best entity to interact. WARN: DO NOT
	CALL THIS IN THE UpdateIntersectionVector func.
	reason: recursion*/
	IEntity* GetInteractionEntity();

	SInteractionFeedback m_feedback;

private:
	float m_radius;
	Vec3 m_relativePos;

	bool m_isEnabled;
	bool m_isHidden;

	CInteractionManagerComponent* m_pCurrentInteractionEntity;

	Cry::SensorSystem::SensorVolumeIdSet m_sensorVolumeQueryResult;
	Cry::SensorSystem::ISensorMap* m_pSensorMap;
	Cry::SensorSystem::ISensorSystem* m_pSensorSystem;
	CTrackerState m_trackerState;
	std::vector<IEntity*> m_intersectedObjects;
};