// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 13:33 19.09.2018, Ali Mehmet Altundag
// Designed by Orhan Aksoy

#pragma once

#include "Interaction/Interaction.h"
#include "Input/HMDIInput.h"

class CTrackerState {
public:
	CTrackerState() {};
	bool UpdateFromController(const EHmdController controller, const Vec3 &playerOrigin);
	void SetTransform(const CryTransform::CTransform &transform) { m_transform = transform; };
	Vec3 GetPosition() const { return m_transform.GetTranslation(); };
	Quat GetRotation() const { return m_transform.GetRotation().ToQuat(); };
	const Vec3 &GetVelocity() const { return m_linearVelocity; };
	const Vec3 &GetAngularVelocity() const { return m_angularVelocity; };
	const CryTransform::CTransform &GetTransform() const { return m_transform; };
	void SetHandId(EntityId id) { m_handId = id; }
	EntityId GetHandId() const { return m_handId; }
	// TODO: memet is not happy with that..
	float GetButtonState(EKeyId button) const;

private:
	EntityId m_handId{ INVALID_ENTITYID };
	CryTransform::CTransform m_transform;
	Vec3 m_linearVelocity{ 0, 0, 0 };
	Vec3 m_angularVelocity{ 0, 0, 0 };
	Vec2 m_thumbStickValue{ 0, 0 };
	std::map<EKeyId, float> m_buttonStates;
};