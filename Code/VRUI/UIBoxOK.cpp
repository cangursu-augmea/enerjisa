// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 10:44 4.01.2019, Ali Mehmet Altundag
// Modified by Alper Sekerci

#include "StdAfx.h"
#include "UIBoxOK.h"

UIBoxOK::UIBoxOK()
	: m_pOk(nullptr)
	, m_pOkHighligted(nullptr)
{
	m_pCurrentBg = m_pOk = gEnv->pRenderer->EF_LoadTexture("Assets/UIAssets/TextureUIBGOK.tif", FT_DONT_RELEASE | FT_DONT_STREAM | FT_NOMIPS);
	m_pOkHighligted = gEnv->pRenderer->EF_LoadTexture("Assets/UIAssets/TextureUIBGOK_OK.tif", FT_DONT_RELEASE | FT_DONT_STREAM | FT_NOMIPS);
}

bool UIBoxOK::Tick()
{
	UIBox::Tick();

	float x, y;
	x = screenWidth * pos.x;	
	y = posY + 2 * boxHalfHeight - m_messageFontSize * 10 - 45 * scale.y;
	IRenderAuxText::Draw2dLabel(x, y,  m_messageFontSize, Col_White, true,  "TAMAM");

	// check controllers

	
	// for each controller
	for (int i = EHmdController::eHmdController_OpenVR_1; i < EHmdController::eHmdController_OpenVR_MaxNumOpenVRControllers; ++i)
	{
		auto pHMDController = GetController();
		if (!pHMDController) return false;

		const Vec2 val = pHMDController->GetThumbStickValue(static_cast<EHmdController>(i), eKI_Motion_OpenVR_TouchPad_X);
		const float buttonVal = pHMDController->IsButtonPressed(static_cast<EHmdController>(i), eKI_Motion_OpenVR_TouchPadBtn);

		if (buttonVal == 1.f)
		{
			m_value = 1;
			return true;
		}

		if (val.x != 0.f || val.y != 0.f)
		{
			m_pCurrentBg = m_pOkHighligted;
			// doesnt matter which hand... we got it...
			return false;
		}

		m_pCurrentBg = m_pOk;
	}

	return false;
}
