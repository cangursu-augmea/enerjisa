// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 10:44 4.01.2019, Ali Mehmet Altundag

#pragma once
#include <functional>
#include <string>
#include <vector>
#include <CryRenderer/ITexture.h>
#include <CryRenderer/IRenderAuxGeom.h>
#include <CrySystem/VR/IHMDManager.h>

typedef std::function<void(uint32_t value)> UIICallbackFunc;

struct UIBox
{
	UIBox();

	/* updates UI bos state */
	virtual bool Tick();

	/* creates text on the box */
	virtual void CreateText(const std::string& msg, float fontSize, ColorF color);

	/* returns value that callback func will take as arg */
	virtual uint32_t GetValue() const { return m_value; }

protected:
	uint32_t m_value;
	ITexture* m_pCurrentBg;
	std::vector<std::string> m_lines;
	IRenderAuxText::AColor m_messageColor;
	float m_messageFontSize;
	bool m_controllerBlockEnabled;

	const IHmdController* UIBox::GetController();

	std::string WordWrap(std::string sentence, int width);

	// UI POS & SCALE //
	float screenWidth;
	float screenHeight;
	Vec2 scale;
	Vec2 pos;
	float boxHalfWidth;
	float boxHalfHeight;
	float posX;
	float posY;
	float lineHeight;
	// ~~ UI POS & SCALE //
};