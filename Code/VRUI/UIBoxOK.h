// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 10:44 4.01.2019, Ali Mehmet Altundag

#pragma once

#include "UIBox.h"

class UIBoxOK
	: public UIBox
{
public:
	UIBoxOK();

	// UIInput
	virtual bool Tick() override;
	// ~UIInput

private:
	ITexture* m_pOk;
	ITexture* m_pOkHighligted;
};