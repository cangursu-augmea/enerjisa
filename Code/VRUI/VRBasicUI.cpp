// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 10:46 27.12.2018, Ali Mehmet Altundag

#include "StdAfx.h"
#include "VRBasicUI.h"
#include "StdDefs.h"
#include "UIBoxOK.h"
#include "UIBoxYesNo.h"

VRBasicUI::VRBasicUI()
	: m_timer(m_isTimerSet)
	, m_isTimerSet(false)
	, m_pBox(nullptr)
{
}

bool VRBasicUI::Init()
{
	Clear();

	gEnv->pGameFramework->RegisterListener(this, "VRBasicUI", FRAMEWORKLISTENERPRIORITY_HUD);
	m_fontSize = gEnv->pRenderer->GetHeight() * TEXT_HEIGHT_RATIO;
	
	mBoxTick = gEnv->pRenderer->EF_LoadTexture("Assets/UIAssets/box_tick.tif", FT_DONT_RELEASE | FT_DONT_STREAM | FT_NOMIPS);
	mBoxEmpty = gEnv->pRenderer->EF_LoadTexture("Assets/UIAssets/box_empty.tif", FT_DONT_RELEASE | FT_DONT_STREAM | FT_NOMIPS);

	m_msgLog.clear();

	return true;
}

void VRBasicUI::Update()
{
	// calculate font size depending on height for each 
	// update (user might change the screen res)
	m_fontSize = gEnv->pRenderer->GetHeight() * TEXT_HEIGHT_RATIO;

	if (m_pBox)
	{
		bool isClearNeeded = m_pBox->Tick();
		if (isClearNeeded)
		{
			auto func = m_callback;
			auto val = m_pBox->GetValue();
			Clear();
			if (func) func(val);
		}
	}
	else
	{
		for (int i = 0; i < m_msgLog.size(); ++i)
		{
			bool isLast = i == m_msgLog.size() - 1;
			auto color = isLast ? Col_White : Col_Gray;
			auto* tex = isLast ? mBoxEmpty : mBoxTick;

			float offset = 10;
			float boxSpace = 10;
			float lineSpace = 30;
			float boxSize = 26;

			float boxLeft = offset;
			float boxRight = boxLeft + boxSize;
			float boxTop = lineSpace * i + offset;

			float boxTextOffset = 5;

			IRenderAuxImage::Draw2dImage(boxLeft, boxTop, boxSize, boxSize, tex->GetTextureID());
			IRenderAuxText::Draw2dLabel(boxRight + boxSpace, boxTop + boxTextOffset, m_fontSize, color, false, m_msgLog[i].c_str());
		}
	}	

	// timer check...
	if (m_isTimerSet)
	{
		m_timer = m_timer - gEnv->pTimer->GetFrameTime();
		if (m_timer < 0.f) Clear();
	}
}

void VRBasicUI::Clear()
{
	SAFE_DELETE(m_pBox);
	m_isTimerSet = false;
	m_callback = nullptr;
}

void inline VRBasicUI::PushMsg(std::string msg)
{
	if (m_msgLog.size() >= LOG_CAPACITY)
	{
		m_msgLog.pop_front();
	}

	m_msgLog.push_back(msg);
}

void VRBasicUI::ShowMessage(const std::string& msg, float timer, bool showCounter)
{
	Clear();
	m_isTimerSet = true;
	m_timer = timer;
	m_pBox = new UIBox();
	m_pBox->CreateText(msg, m_fontSize, Col_White);

	PushMsg(msg);
}

/* shows given message */
void VRBasicUI::ShowMessage(const std::string& msg)
{
	Clear();
	m_pBox = new UIBox();
	m_pBox->CreateText(msg, m_fontSize, Col_White);

	PushMsg(msg);
}

/* yes - no question box. callbeck is called on user input */
void VRBasicUI::ShowYesNo(const std::string& msg, UIICallbackFunc func)
{
	Clear();
	m_callback = func;
	m_pBox = new UIBoxYesNo();
	m_pBox->CreateText(msg, m_fontSize, Col_White);

	PushMsg(msg);
}

/* yes - no question box. callbeck is called on user input */
void VRBasicUI::ShowOk(const std::string& msg, UIICallbackFunc func)
{
	Clear();
	m_callback = func;
	m_pBox = new UIBoxOK();
	m_pBox->CreateText(msg, m_fontSize, Col_White);

	PushMsg(msg);
}