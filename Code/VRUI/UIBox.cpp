// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 10:44 4.01.2019, Ali Mehmet Altundag
// Modified by Alper Sekerci

#include "StdAfx.h"
#include "UIBox.h"
#include <sstream>

UIBox::UIBox()
	: m_messageColor(Col_White)
	, m_value(0)
	, m_controllerBlockEnabled(true)
{
	m_pCurrentBg = gEnv->pRenderer->EF_LoadTexture("Assets/UIAssets/TextureUIBG.tif", FT_DONT_RELEASE | FT_DONT_STREAM | FT_NOMIPS);
}

bool UIBox::Tick()
{
	screenWidth = (float)gEnv->pRenderer->GetWidth();
	screenHeight = (float)gEnv->pRenderer->GetHeight();

	scale = CGamePlugin::GetUIScale();
	pos = CGamePlugin::GetUIPos();

	boxHalfWidth = screenWidth * scale.x;
	boxHalfHeight = screenHeight * scale.y;

	posX = screenWidth * pos.x - boxHalfWidth;
	posY = screenHeight * pos.y - boxHalfHeight;

	if (m_pCurrentBg)
		IRenderAuxImage::Draw2dImage(posX, posY, 2 * boxHalfWidth, 2 * boxHalfHeight, m_pCurrentBg->GetTextureID());

	lineHeight = m_messageFontSize * 10; // notice factor
	float yOffset = -lineHeight * m_lines.size() * 0.5f;
	float x, y;
	for (auto it : m_lines)
	{
		// x = gEnv->pRenderer->GetWidth() * 0.5f;
		// y = gEnv->pRenderer->GetHeight() * 0.5f + yOffset;
		// yOffset = yOffset + lineHeight;

		x = screenWidth * pos.x;
		y = screenHeight * pos.y + yOffset;
		yOffset += lineHeight;

		IRenderAuxText::Draw2dLabel(x, y, m_messageFontSize, m_messageColor, true, it.c_str());
	}

	return false;
}

void UIBox::CreateText(const std::string& msg, float fontSize, ColorF color)
{
	m_messageFontSize = fontSize;
	m_messageColor = IRenderAuxText::AColor(color);

	std::string wrapped = WordWrap(msg, 32);
	std::istringstream strStream(wrapped);
	std::string line;
	float yOffset = 0.f;
	while (std::getline(strStream, line))
	{
		m_lines.push_back(line);
	}
}

const IHmdController* UIBox::GetController()
{
	IHmdManager* pHMDManager = gEnv->pSystem->GetHmdManager();
	if (!pHMDManager) return nullptr;

	IHmdDevice* pHMDDevice = pHMDManager->GetHmdDevice();
	if (!pHMDDevice) return nullptr;

	const IHmdController* pHMDController = pHMDDevice->GetController();
	if (!pHMDController) return nullptr;

	// we dont want controllers affect another box with the junk
	// values from old session... so, first wait for all values sattle
	if (m_controllerBlockEnabled)
	{
		for (int i = EHmdController::eHmdController_OpenVR_1; i < EHmdController::eHmdController_OpenVR_MaxNumOpenVRControllers; ++i)
		{
			// these re the inputs that are used by BoxOk and BoxYesNo
			const Vec2 val = pHMDController->GetThumbStickValue(static_cast<EHmdController>(i), eKI_Motion_OpenVR_TouchPad_X);
			const float buttonVal = pHMDController->IsButtonPressed(static_cast<EHmdController>(i), eKI_Motion_OpenVR_TouchPadBtn);

			if (val.x != 0.f || val.y != 0.f) return nullptr;
			if (buttonVal != .0f) return nullptr;
		}

		m_controllerBlockEnabled = false;
	}

	return pHMDController;
}

// source: https://stackoverflow.com/questions/1083531/wordwrap-function/1083551#1083551
std::string UIBox::WordWrap(std::string sentence, int width)
{
	//this iterator is used to optimize code; could use array indice 
	//iterates through sentence till end 
	std::string::iterator it = sentence.begin();
	//this iterator = it when you reach a space; will place a newline here
	//if you reach width; also kind of hackish (used instead of comparing to NULL)
	std::string::iterator lastSpace = sentence.begin();

	int distanceToWidth = 0;

	//used in rare instance that there is a space
	//at the end of a line
	bool endOfLine = false;

	while (it != sentence.end())
	{
		//TODO: possible to stop recomparing against .end()?
		while (it != sentence.end() && distanceToWidth <= width)
		{
			const char c = *it;
			distanceToWidth = distanceToWidth + static_cast<int>((c & 0xc0) != 0x80);

			if (*it == ' ')
			{
				lastSpace = it;

				//happens if there is a space after the last character
				if (width == distanceToWidth)
				{
					*lastSpace = '\n';
				}
			}

			++it;
		}

		//happens when lastSpace did encounter a space
	   //otherwise
		if (lastSpace != sentence.begin())
		{
			*lastSpace = '\n';
		}

		lastSpace = sentence.begin();
		distanceToWidth = 0;
	}

	return sentence;
}