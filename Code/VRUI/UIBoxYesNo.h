#pragma once

#include "UIBox.h"

class UIBoxYesNo
	: public UIBox
{
public:
	UIBoxYesNo();

	// UIBox
	virtual bool Tick() override;
	// ~UIBox

private:
	ITexture* m_pDefault;
	ITexture* m_pYes;
	ITexture* m_pNo;
};