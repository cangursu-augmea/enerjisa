// Copyright 2018 Augmea Inc. All rights reserved.
// Created: 10:46 27.12.2018, Ali Mehmet Altundag

#pragma once

#include "UIBox.h"

// A singleton object that will manage the basic
// info and yes-no popups in a VR oriented HCI
// This system will be acessible on game plugin

class VRBasicUI
	: public IGameFrameworkListener
{
public:
	VRBasicUI();

	bool Init();

	void Update();

	void Clear();

	/* shows message for untill timeout */
	void ShowMessage(const std::string& msg, float timer, bool showCounter = true);

	/* shows given message */
	void ShowMessage(const std::string& msg);

	/* yes - no question box. callbeck is called on user input */
	void ShowYesNo(const std::string& msg, UIICallbackFunc func);

	void ShowOk(const std::string& msg, UIICallbackFunc func);

private:
	float m_fontSize;
	const float TEXT_HEIGHT_RATIO = 0.004f;

	UIBox* m_pBox;
	UIICallbackFunc m_callback;

	float m_timer;
	bool m_isTimerSet;

	std::deque<std::string> m_msgLog{};
	void inline PushMsg(std::string msg);
	const int LOG_CAPACITY = 3;
	ITexture* mBoxTick;
	ITexture* mBoxEmpty;

	// IGameFrameworkListener
	virtual void OnPostUpdate(float fDeltaTime) override { Update(); };
	virtual void OnSaveGame(ISaveGame* pSaveGame) override {  };
	virtual void OnLoadGame(ILoadGame* pLoadGame) override {  };
	virtual void OnLevelEnd(const char* nextLevel) override {  };
	virtual void OnActionEvent(const SActionEvent& event) override {  };
	virtual void OnPreRender() { Update(); }
	// ~IGameFrameworkListener
};