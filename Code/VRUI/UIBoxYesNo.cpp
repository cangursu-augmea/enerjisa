#include "StdAfx.h"
#include "UIBoxYesNo.h"
#include <CrySystem/VR/IHMDManager.h>

UIBoxYesNo::UIBoxYesNo()
{
	m_pCurrentBg = m_pDefault = gEnv->pRenderer->EF_LoadTexture("Assets/UIAssets/TextureUIBGRG.tif", FT_DONT_RELEASE | FT_DONT_STREAM | FT_NOMIPS);
	m_pYes = gEnv->pRenderer->EF_LoadTexture("Assets/UIAssets/TextureUIBGRG_YES.tif", FT_DONT_RELEASE | FT_DONT_STREAM | FT_NOMIPS);
	m_pNo = gEnv->pRenderer->EF_LoadTexture("Assets/UIAssets/TextureUIBGRG_NO.tif", FT_DONT_RELEASE | FT_DONT_STREAM | FT_NOMIPS);
}

bool UIBoxYesNo::Tick()
{
	UIBox::Tick();

	// render YES - NO text
	float x, y;
	// x = (gEnv->pRenderer->GetWidth() / 4.f) * 3;
	// y = gEnv->pRenderer->GetHeight() * 0.9f;
	x = screenWidth * pos.x + 0.5f * boxHalfWidth;
	y = posY + 2 * boxHalfHeight - m_messageFontSize * 10 - 45 * scale.y;

	IRenderAuxText::Draw2dLabel(x, y, m_messageFontSize, Col_White, true, "EVET");

	x = screenWidth * pos.x - 0.5f * boxHalfWidth;
	IRenderAuxText::Draw2dLabel(x, y, m_messageFontSize, Col_White, true, "HAYIR");

	auto pHMDController = GetController();
	if (!pHMDController) return false;

	// if we have a yes-no callback
	for (int i = EHmdController::eHmdController_OpenVR_1; i < EHmdController::eHmdController_OpenVR_MaxNumOpenVRControllers; ++i)
	{
		const Vec2 val = pHMDController->GetThumbStickValue(static_cast<EHmdController>(i), eKI_Motion_OpenVR_TouchPad_X);
		const float buttonVal = pHMDController->IsButtonPressed(static_cast<EHmdController>(i), eKI_Motion_OpenVR_TouchPadBtn);

		if (val.x < -0.2f)
		{
			if (buttonVal != 1.f)
			{
				m_pCurrentBg = m_pNo;
				return false;
			}
			else
			{
				m_value = 0;
				return true;
			}
		}
		else if (val.x > 0.2f)
		{
			if (buttonVal != 1.f)
			{
				// just highlight
				m_pCurrentBg = m_pYes;
				return false;
			}
			else
			{
				m_value = 1;
				return true;
			}
		}

		m_pCurrentBg = m_pDefault;
	}

	return false;
}