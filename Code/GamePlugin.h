#pragma once

#include <CrySystem/ICryPlugin.h>
#include <CryGame/IGameFramework.h>
#include <CryEntitySystem/IEntityClass.h>
#include <CryNetwork/INetwork.h>

#include "VRUI/VRBasicUI.h"
#include "TextManager/TextManager.h"
#include "SessionManager.h"
#include "StateManager.h"
#include "StdDefs.h"

class CPlayerComponent;

// The entry-point of the application
// An instance of CGamePlugin is automatically created when the library is loaded
// We then construct the local player entity and CPlayerComponent instance when OnClientConnectionReceived is first called.
class CGamePlugin 
	: public Cry::IEnginePlugin
	, public ISystemEventListener
	, public INetworkedClientListener
{
public:
	CRYINTERFACE_SIMPLE(Cry::IEnginePlugin)
	CRYGENERATE_SINGLETONCLASS_GUID(CGamePlugin, "Blank", "f01244b0-a4e7-4dc6-91e1-0ed18906fe7c"_cry_guid)

	virtual ~CGamePlugin();
	
	// Cry::IEnginePlugin
	virtual const char* GetCategory() const override { return "Game"; }
	virtual bool Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams) override;
	// ~Cry::IEnginePlugin

	// ISystemEventListener
	virtual void OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam) override;
	// ~ISystemEventListener

	// INetworkedClientListener
	// Sent to the local client on disconnect
	virtual void OnLocalClientDisconnected(EDisconnectionCause cause, const char* description) override {}

	// Sent to the server when a new client has started connecting
	// Return false to disallow the connection
	virtual bool OnClientConnectionReceived(int channelId, bool bIsReset) override;
	// Sent to the server when a new client has finished connecting and is ready for gameplay
	// Return false to disallow the connection and kick the player
	virtual bool OnClientReadyForGameplay(int channelId, bool bIsReset) override;
	// Sent to the server when a client is disconnected
	virtual void OnClientDisconnected(int channelId, EDisconnectionCause cause, const char* description, bool bKeepClient) override;
	// Sent to the server when a client is timing out (no packets for X seconds)
	// Return true to allow disconnection, otherwise false to keep client.
	virtual bool OnClientTimingOut(int channelId, EDisconnectionCause cause, const char* description) override { return true; }
	// ~INetworkedClientListener

	static int IsInteractionDebugEnabled() { return s_isInteractionDebugEnabled; }

	static VRBasicUI& GetBasicUI() { return s_basicUI; }

	static TextManager& GetTextManager() { return s_textManager; }

	static CSessionManager& GetSessionManager() { return s_sessionManager; }

	static StateManager::CStateManager& GetStateManager() { return s_stateManager; }

	static void OnUIScaleChange(ICVar* uiCVAR)
	{
		//GetBasicUI().SetScale(Vec2(CGamePlugin::s_uiScale));
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "NOT IMPLEMENTED YET!");
	}

	static Vec2 GetUIScale() { return s_uiScale; }
	static Vec2 GetUIPos() { return s_uiPos; }

	static void OnScenarioChange(ICVar* pOpType)
	{
		// inser training set scenario
		{
			StateManager::Event event;

			CryLogAlways("OnScenarioChange pOpType->GetName() = %s, pOpType->GetString() = %s", pOpType->GetName(), pOpType->GetString() );

			if (strcmp(pOpType->GetName(), "aug_training_set") == 0)
			{	
				event.action = ESecenarioQueryActions::TRAINING_SET;
			}
			if (strcmp(pOpType->GetName(), "aug_operation") == 0)
			{
				event.action = ESecenarioQueryActions::OPERATION_TYPE;
			}

			const char* value = pOpType->GetString();
			std::memcpy(event.result.sValue, value, std::strlen(value) + 1);
			GetStateManager().Insert(event);
		}
	}

protected:
	// Map containing player components, key is the channel id received in OnClientConnectionReceived
	std::unordered_map<int, EntityId> m_players;
	static int s_isInteractionDebugEnabled;
	static VRBasicUI s_basicUI;
	static TextManager s_textManager;
	static StateManager::CStateManager s_stateManager;
	static Vec2 s_uiScale;
	static Vec2 s_uiPos;
	XmlNodeRef m_levelConfigsRoot;
	static string s_scenario;

	static CSessionManager s_sessionManager;
};