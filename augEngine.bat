@ECHO OFF
:: Change enginepath according to your local engine path.
SET enginepath=../cryengine_551/
SET enginecryrun="%enginepath%Tools/CryVersionSelector/cryrun.exe"
SET enginerc="%enginepath%Tools/rc/rc.exe"

:parse
IF "%~1"=="" GOTO endparse
IF "%~1"=="open" (
	SET cryruncommand="%~1"
	SET cryrunproject="%~2"
)
IF "%~1"=="metagen" (
	SET cryruncommand="%~1"
	SET cryrunproject="%~2"
)
IF "%~1"=="edit" (
	SET cryruncommand="%~1"
	SET cryrunproject="%~2"
)
IF "%~1"=="projgen" (
	SET cryruncommand="%~1"
	SET cryrunproject="%~2"
)
IF "%~1"=="package" (
	SET cryruncommand="%~1"
	SET cryrunproject="%~2"
)
IF "%~1"=="monodev" (
	SET cryruncommand="%~1"
	SET cryrunproject="%~2"
)
IF "%~1"=="build" (
	SET cryruncommand="%~1"
	SET cryrunproject="%~2"
)
IF "%~1"=="rc" (
	SET cryruncommand="%~1"
	SET cryrunproject="%~2"	
)
SHIFT
GOTO parse

:endparse


if "%cryruncommand%"==""rc"" ( 
	ECHO %enginerc%  %cryrunproject%
	%enginerc%   %cryrunproject%
	GOTO endrc 
)


IF NOT "%cryruncommand%"=="" (
	pushd "%enginepath%"
		!setupEngine
	popd
	ECHO %enginecryrun% %cryruncommand% %cryrunproject%
	%enginecryrun% %cryruncommand% %cryrunproject%
	
) 
ELSE (
	ECHO usage: augEngine open^|edit^|projgen^|monodev^|build projectfile
)

:endrc